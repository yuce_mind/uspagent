# USP and Ambiorix symbiosis

The ambiorix framework is used to implement modern libraries, applications, plug-ins, etc. inside the middleware of the gateway. The USP standard is used for external controllers to access data models on the gateway.

In many situations, it is important that Ambiorix and USP are compatible with each other for an efficient cooperation. This document will try to describe a few situations where USP and Ambiorix need to be aligned to function properly.

## Data model Events

A USP controller is able to subscribe to data model events such as `Device.Boot!`, `Device.SoftwareModules.DUStateChange!`, etc. In USP terms, these events are known as `Events`.

Some `Events` can contain parameters that need to be transmitted to the subscribing controller. When an ambiorix event is received by the USP agent and it contains parameters, the parameters must be in a separate htable to make sure the USP agent knows which part of the event variant contains the parameters. In the current implementation, the parameters must be in a sub table called `data`.

### Example

This `DUStateChange` event contains some parameters:

```
{
    CurrentState = "Installed",
    ExecutionEnvRef = "Device.SoftwareModules.DeploymentUnit.2",
    Fault.FaultCode = 0,
    Fault.FaultString = "",
    OperationPerformed = "Install",
    Resolved = 1,
    UUID = "snake667",
    Version = "latest"
    notification = "DUStateChange!",
    object = "SoftwareModules.DeploymentUnit.cpe-snake667.",
    path = "SoftwareModules.DeploymentUnit.2.",
}

```

In order for the USP agent to figure out what the parameters are, the variant should look like this:

```
{
    data = {
        CurrentState = "Installed",
        ExecutionEnvRef = "Device.SoftwareModules.DeploymentUnit.2",
        Fault.FaultCode = 0,
        Fault.FaultString = "",
        OperationPerformed = "Install",
        Resolved = 1,
        UUID = "snake667",
        Version = "latest"
    }
    notification = "DUStateChange!",
    object = "SoftwareModules.DeploymentUnit.cpe-snake667.",
    path = "SoftwareModules.DeploymentUnit.2."
}
```

## Synchronous operations, asynchronous operations and OperationComplete events

A USP controller is able to invoke RPCs such as [Device.SoftwareModules.InstallDU()](https://usp-data-models.broadband-forum.org/tr-181-2-14-0-usp.html#D.Device:2.Device.SoftwareModules.InstallDU()) using USP Operate messages. The TR-181 data model will always indicate whether it is a synchronous or asychronous command with the `[ASYNC]` tag in the command description. This information must be added to the odl file by setting an async userflag in the function definition. The USP agent will check the data model to see if this userflag is set and use `amxb_call` or `amxb_async_call` to invoke the command. An example of such a user flag can be found in one of the [test odl files](../test/common/phonebook_definition.odl).

When using `amxb_call`, the USP agent will wait (blocking) for the result of the RPC and send an OperateResponse with possible output arguments back to the USP controller. When using `amxb_async_call`, the USP agent will not wait for a response, but will create a `LocalAgent.Request.` instance to track the status of the operation. It will reply to the USP controller with an OperateResponse message that contains the path to the newly created `LocalAgent.Request.` instance. Once the RPC is done, this will trigger the callback function provided to `amxb_async_call` and the agent will send a USP Notify message of type `OperationComplete` back to the USP controller.

In case of an asynchronous operation, the RPC implementation will sometimes need to be implemented asynchronously as well to prevent the application from blocking. In Ambiorix this can be done by implementing it as a [deferred function](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd/-/blob/v3.3.0/include/amxd/amxd_function.h#L522). The function should return with `amxd_status_defer` and provide the result later on with `amxd_function_deferred_done`. The final `amxd_status_t` value can be provided with `amxd_function_deferred_done` and will be used by the USP agent to determine whether the operation succeeded. An example of a deferred function implementation can be found in the [greeter plugin](https://gitlab.com/prpl-foundation/components/ambiorix/examples/datamodel/greeter_plugin/-/blob/v0.4.2/src/dm_greeter_methods.c#L191).

When the service implementing the RPC returns with `amxd_function_returned_done`, the USP agent will send an `OperationComplete` notification to all USP controllers that are subscribed to this event. A USP controller should add a subscription to the `LocalAgent.Subscription.` data model before invoking the RPC to make sure it does not miss the `OperationComplete` notification. This notification can indicate success or failure and as mentioned above, the USP agent will look at the value of the `amxd_status_t` to send back the possible output arguments in case of success or command failure otherwise.

In case a method has output arguments, these arguments must be set as `%out` arguments in the odl file. It is important that the service implementing the RPC also adds the output arguments to the `args` variant in its function implementation. If they are not added to the `args` variant, they cannot be sent to the USP controller in the response.

If the method also has a return value, it will be added to the output arguments with `_retval` as the argument name.

Example:

Assume the following custom RPC method:

```
bool MyCustomMethod(%out string info, %out uint32 number);
```

Calling this method could for example give the following return variant
```
[
    true,
    {
        info = "Some crucial information",
        number = 123
    }

]
```

We can see the return variant at index 0 and the output arguments at index 1 of the list.

This would be translated to an Operate response message with the following output arguments:
```
output_args {
    key: "info",
    value: "Some crucial information"
}
output_args {
    key: "number",
    value: "123"
}
output_args {
    key: "_retval",
    value: "true"
}
```

In most situations the process that implements the RPC has more knowledge of what could have gone wrong during the function invocation. Therefore it can also add an `err_code` (uin32t_t) and `err_msg` (string) to the `args` variant when something goes wrong. The USP agent will retrieve these values and provided them in the OperateResponse or Notification to the USP controller.

Take a look at the [tutorial regarding rpc-methods for more information](https://gitlab.com/prpl-foundation/components/ambiorix/tutorials/datamodels/server/rpc-methods/#methods-with-output-arguments).

Example of the updated `args` variant when error information is added:
```
{
    err_code = 7005,
    err_msg = "Resources exceeded",
}
```

Example of the updated `args` variant when an output argument named `Foo` is added.
```
{
    Foo = "Bar",
}
```

In case of a successful operation without output arguments, the `args` variant does not need to be updated.
