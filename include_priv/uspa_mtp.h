/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPA_MTP_H__)
#define __USPA_MTP_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspa.h"

/**
   @brief
   Subscribe to events for LocalAgent.MTP.{i}. and act accordingly

   If an MTP is enabled or an enabled MTP is added, check the enable parameters in the
   LocalAgent.Controller. data model and setup an IMTP connection if they are all enabled.

   If an MTP is disabled or deleted (TODO), close the IMTP connection.

   @param ctx a bus context
   @return 0 when subscribed successfully, -1 in case of failure
 */
int uspa_mtp_subscribe(amxb_bus_ctx_t* ctx);

int uspa_mtp_init(amxb_bus_ctx_t* ctx);

void uspa_mtp_clean(void);

int uspa_mtp_new(uspa_mtp_t** mtp, uint32_t type_id, const char* la_mtp);
void uspa_mtp_delete(uspa_mtp_t** mtp);
amxc_htable_t* uspa_mtp_get_instances(void);
int uspa_mtp_instance_new(uspa_mtp_instance_t** inst);
void uspa_mtp_instance_delete(uspa_mtp_instance_t** inst);
void uspa_mtp_instance_hit_clean(const char* key, amxc_htable_it_t* hit);
int uspa_mtp_instance_con_add(const char* la_mtp, uspi_con_t* con);
int uspa_mtp_instance_con_rm(const char* la_mtp);
void uspa_mtp_controller_add(uspa_mtp_t* mtp, const char* contr_mtp);
void uspa_mtp_controller_remove(const char* contr_mtp);

#ifdef __cplusplus
}
#endif

#endif // __USPA_MTP_H__
