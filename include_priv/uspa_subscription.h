/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPA_SUBSCRIPTION_H__)
#define __USPA_SUBSCRIPTION_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspa.h"

/**
   @brief
   Subscribe to data model events for changes in the LocalAgent.Subscription. data model

   Creates bus subscriptions when Subscription instances are added or enabled.
   Removes bus subscriptions when Subscription instances are removed or disabled.

   @param ctx a bus context
   @return 0 in case of success, -1 in case of error
 */
int uspa_subscription_subscribe(amxb_bus_ctx_t* ctx);

/**
   @brief
   Loop over all enabled LocalAgent.Subscription. instances in the data model on startup.
   Subscribe to data model event if needed.

   @param ctx a bust context
   @return 0 in case of success, -1 in case of error
 */
int uspa_subscription_init(amxb_bus_ctx_t* ctx);

/**
   @brief
   Clean up the list of subscriptions to avoid memory leaks

 */
void uspa_subscription_clean(void);

/**
   @brief
   Find a uspa_sub_t struct based on the Subscription ID

   @param id Subscription ID
   @return Pointer to a uspa_sub_t struct if found, NULL otherwise
 */
uspa_sub_t* uspa_sub_find_from_id(const char* id);

/**
   @brief
   Find a uspa_sub_retry_t struct based on the corresponding uspa_sub_t object and the message id
   from the received Notify response.

   @param sub pointer to a uspa_sub_t struct related to this notification
   @param msg_id message id of the recevied Notify response
   @return pointer to a uspa_sub_retry_t struct if found, NULL otherwise
 */
uspa_sub_retry_t* uspa_sub_retry_find_from_msg_id(uspa_sub_t* sub, const char* msg_id);

/**
   @brief
   Free memory allocated for the uspa_sub_retry_t struct

   @param sub_retry pointer to a pointer of a uspa_sub_retry_t struct
 */
void uspa_sub_retry_delete(uspa_sub_retry_t** sub_retry);

/**
   @brief
   Forwards a notification to the subscribed controller.

   The controller that needs to be contacted is found based on the provided subscription ID. The
   notification variant that is provided is a variant obtained by calling uspl_notify_extract.

   @param sub_id subscription ID
   @param notification variant containing the USP notification data
   @return 0 in case the controller can be notified, any other value indicates an error
 */
int uspa_sub_notify_controller(const char* sub_id, amxc_var_t* notification);

/**
   @brief
   Checks whether it is allowed to subscribe to the parameters mentioned in the ref_list

   @param acls acls with search paths resolved
   @param ref_list LocalAgent.Subscription.{i}.ReferenceList parameter
   @param notif_type LocalAgent.Subscription.{i}.NotifType parameter
   @return true if the controller is allowed to subscribe to each element in the ref_list, false otherwise
 */
bool uspa_subscription_validate(amxc_var_t* acls, const char* ref_list, const char* notif_type);

#ifdef __cplusplus
}
#endif

#endif // __USPA_SUBSCRIPTION_H__
