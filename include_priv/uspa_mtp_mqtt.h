/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPA_MTP_MQTT_H__)
#define __USPA_MTP_MQTT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspa.h"

/**
   @brief
   Get the instance path of the enabled LocalAgent.MTP.{i} instance based on the provided MQTT
   Client reference path

   @param reference
   @return the MTP instance path if an enabled MTP is found for the reference, NULL otherwise.
 */
char* uspa_mtp_mqtt_get_enabled_mtp(const char* reference);

/**
   @brief
   Get the LocalAgent.MTP.{i}.MQTT.Reference parameter based on the provided MTP instance.

   @note
   The memory must be freed.

   @param mtp an MTP instance path
   @return a char pointer containing the reference parameter or NULL
 */
char* uspa_mtp_mqtt_get_reference(const char* mtp);

/**
   @brief
   Get the uspi_con_t that belongs to the provided reference client

   @param reference MQTT.Client.{i}. path
   @return pointer to a uspi_con_t struct if found, NULL otherwise
 */
uspi_con_t* uspa_mtp_mqtt_get_connection(const char* reference);

/**
   @brief
   Enable the provided MQTT.Client.{i}.

   @param reference an MQTT Client instance path
   @return int
 */
int uspa_mtp_mqtt_enable_reference(const char* reference);

/**
   @brief
   Adds User properties for a MQTT.Client.{i}. instance

   @param reference path to an MQTT.Client.{i}. instance
   @return 0 in case of success, any other value indicates failure
 */
int uspa_mtp_mqtt_add_user_props(const char* reference);

/**
   @brief
   Configuration function that can be called when a LocalAgent.MTP.{i}. instance with Protocol MQTT
   is enabled or when a new enabled instance is added.

   Setup the IMTP connection to MQTT Reference client, add the needed UserProperty and Subscription
   instances.

   @param la_mtp instance path to an enabled LocalAgent.MTP. instance
   @return 0 in case the IMTP connection with the MQTT client was established and the client was
           enabled, -1 in all other situations
 */
int uspa_mtp_mqtt_configure(const char* la_mtp);

/**
   @brief
   Disables the IMTP connection to the MTP

   Function will be called when LocalAgent.MTP.{i}. gets disabled

   @param mtp instance path to a LocalAgent.MTP.{i}. instance
 */
void uspa_mtp_mqtt_disable(const char* mtp);

/**
   @brief
   Subscribe to changes in MQTT.Client. Enable and Status parameters

   @param ctx a valid bus context
   @return 0 in case of successful subscriptions, any other value in case of error
 */
int uspa_mtp_mqtt_subscribe(amxb_bus_ctx_t* ctx);

int uspa_mtp_mqtt_new(uspi_con_t* con,
                      const char* reference,
                      const char* la_mtp);

void uspa_mtp_mqtt_delete(uspi_con_t* con);

int uspa_mtp_mqtt_init(amxb_bus_ctx_t* bus_ctx);

void uspa_mtp_mqtt_clean(void);

#ifdef __cplusplus
}
#endif

#endif // __USPA_MTP_MQTT_H__
