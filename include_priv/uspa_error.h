/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPA_ERROR_H__)
#define __USPA_ERROR_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspa.h"

/**
   @brief
   Builds the top level error variant that will be used to build a USP Error message. No parameter
   error information will be added by this function.

   The provided error code must be a USP specific error code. The provided error message can be
   NULL. If it is NULL, a generic USP error message will be built based on the provided error code.

   @param error initialized variant that will be appended with the provided error information
   @param err_code USP specific error code
   @param err_msg optional error message
   @return 0 in case of success, -1 in case of error
 */
int uspa_error_top_level(amxc_var_t* error, uint32_t err_code, const char* err_msg);

/**
   @brief
   Appends parameter error information to the top level error variant. The provided error variant
   must first be populated with top level error inforation using @ref uspa_error_top_level.

   The obj_path must contain the path to the object where the error occured. The param_path must
   contain the parameter name that failed to be updated. The full parameter path will be built by
   concatenating the obj_path and param_path. For added flexibility, it is also allowed to provide
   the full parameter path with the obj_path argument. In this case, the param_path must be NULL.

   The provided error code must be a USP specific error code for the parameter error. The provided
   error message can be NULL. If it is NULL, a generic USP error message will be built based on the
   provided error code.

   @param error error variant that will be appended with the provided parameter error information
   @param obj_path path to the object where the error occured
   @param param_path path to the parameter that failed to update
   @param err_code USP specific error code
   @param err_msg optional error message
   @return 0 in case of success, -1 in case of error
 */
int uspa_error_append_param(amxc_var_t* error,
                            const char* obj_path,
                            const char* param_path,
                            uint32_t err_code,
                            const char* err_msg);

/**
   @brief
   Appends forbidden parameters to an error message.

   The provided variant 'requests' contains a list of update requests. For each parameter, there
   will be a boolean 'allowed' to indicate whether it is allowed to update the parameter. Forbidden
   parameters will be added to the error variant

   ```
    requests = [
        {
            object_path = <string>,
            parameters = [
                {
                    param = <string>,
                    value = <any>,
                    required = <bool>
                },
                ...
            ]
        },
        ...
    ]
   ```

   Also see @ref uspa_error_append_param

   @param error error variant that will be appended
   @param requests variant with requests that contain forbidden parameters
 */
void uspa_error_append_forbidden(amxc_var_t* error, amxc_var_t* requests);

#ifdef __cplusplus
}
#endif

#endif // __USPA_ERROR_H__
