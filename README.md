# USP Agent

[[_TOC_]]

## Introduction

This repo provides an implementation for the USP Agent as defined by the USP BBF community.
https://www.broadband-forum.org/download/TR-369.pdf

## Building, installing, running and testing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

1. Install docker

    Docker must be installed on your system.

    If you have no clue how to do this here are some links that could help you:

    - [Get Docker Engine - Community for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)<br /><br />
    
    Make sure you user id is added to the docker group:

    ```
    sudo usermod -aG docker $USER
    ```

1. Fetch the container image

    To get access to the pre-configured environment, all you need to do is pull the image and launch a container.

    Pull the image:

    ```bash
    docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    Before launching the container, you should create a directory which will be shared between your local machine and the container.

    ```bash
    mkdir -p ~/workspace/amx/usp/applications
    ```

    Launch the container:

    ```bash
    docker run -ti -d --name oss-dbg --restart always --cap-add=SYS_PTRACE --sysctl net.ipv6.conf.all.disable_ipv6=1 -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/workspace/:/home/$USER/workspace/ registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    The `-v` option bind mounts the local directory for the ambiorix project in the container, at the exact same place.
    The `-e` options create environment variables in the container. These variables are used to create a user name with exactly the same user id and group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

    ```bash
    docker exec -ti --user $USER oss-dbg /bin/bash
    ```

### Building

#### Prerequisites

- [libamxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc)
- [libamxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp)
- [libamxj](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxj)
- [libamxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd)
- [libamxb](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxb)
- [libamxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo)
- [libamxa](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxa)
- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp)
- [libsahtrace](https://gitlab.com/soft.at.home/network/libsahtrace)
- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp)

#### Building uspagent

1. Clone the git repository

    To be able to build it, you need the source code. So open the desired target directory and clone the application.

    ```bash
    mkdir ~/workspace/amx/usp/applications
    cd ~/workspace/amx/usp/applications
    git clone git@gitlab.com:soft.at.home/usp/applications/uspagent.git
    ``` 

1. Install dependencies

    Although the container will contain all tools needed for building, it does not contain the libraries needed for building the `uspagent`. To be able to build the `uspagent` you need `libamxc`, `libamxp`, `libamxj`, `libamxd`, `libamxb`, `libamxo`, `libamxa`, `libimpt`, `libusp` and `libsahtrace`. These libraries can be installed in the container by executing the following commands. 

    ```bash
    sudo apt update
    sudo apt install libamxc libamxp libamxj libamxd libamxb libamxo libamxa libimtp sah-lib-sahtrace-dev libusp
    ```

    Note that you do not need to install all components explicitly. Some components will be installed automatically because other components depend on them.

1. Build it

    ```bash
    cd ~/workspace/amx/usp/applications/uspagent
    make
    ```

### Installing

#### Using make target install


You can use the install target in the makefile to install the application

```bash
cd ~/workspace/amx/usp/applications/uspagent
sudo -E make install
```

#### Using package

From within the container you can create packages.

```bash
cd ~/workspace/amx/usp/applications/uspagent
make package
```

The packages generated are:

```
~/workspace/amx/usp/applications/uspagent/uspagent-<VERSION>.tar.gz
~/workspace/amx/usp/applications/uspagent/uspagent-<VERSION>.deb
```

You can copy these packages and extract/install them.

For ubuntu or debian distributions use dpkg:

```bash
sudo dpkg -i ~/workspace/amx/usp/applications/uspagent/uspagent-<VERSION>.deb
```

### Running

#### Run time prerequisites

- [amxrt](https://gitlab.com/prpl-foundation/components/ambiorix/applications/amxrt)
- [mod-dmext](https://gitlab.com/prpl-foundation/components/core/modules/mod_dmext)
- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp)
- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt)
- [mod-sahtrace](https://gitlab.com/prpl-foundation/components/core/modules/mod_sahtrace) (optional)

This plugin runs with `amxrt` like most ambiorix plugins. It also uses `mod-dmext` to check the validity of a few data model parameters. To publish messages with the MQTT protocol, it uses the `USP backend` to communicate with `tr181-mqtt` client. For logging it loads the `sahtrace` module at run time if it is available. When it is not available, it will not be loaded.

These tools can be installed from debian packages.

```bash
sudo apt install amxrt mod-dmext mod-amxb-usp tr181-mqtt mod-sahtrace
```

#### Running the USP agent

During installation of the USP agent a symbolic link is created to amxrt:

```text
/usr/bin/uspagent -> /usr/bin/amxrt
```

This allows you to run the agent using the `uspagent` command. `amxrt` will find the relevant odl files in `/etc/amx/uspagent`.

With the current version of the USP agent you can only communicate over MQTT. Other MTPs can be added later on. Before starting the USP agent, you must launch `tr181-mqtt` first. To start both applications run:

```bash
tr181-mqtt
uspagent
```

You may need to run them with sudo privileges depending on your setup.

### Testing

#### Run tests

1. Install dependencies

    No extra dependencies are needed for testing the `uspagent`.

1. Run tests

    You can run the tests by executing the following command.
    
    ```bash
    cd ~/workspace/amx/usp/applications/uspagent/test
    make
    ```
    
    Or this command if you also want the coverage tests to run:
    
    ```bash
    cd ~/workspace/amx/usp/applications/uspagent/test
    make run coverage
    ```
    
    Or from the root directory of this repository,
    
    ```bash
    cd ~/workspace/amx/usp/applications/uspagent
    make test
    ```
    
    This last will run the unit-tests and generate the test coverage reports in one go.

#### Coverage reports

The coverage target will generate coverage reports using [gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and [gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each file (*.c file) is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are available in the output directory of the compiler used.
Example: using native gcc
When the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML coverage reports can be found at `~/workspace/amx/usp/applications/uspagent/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser.
In the container start a python3 http server in background.

```bash
cd ~/workspace/
python3 -m http.server 8080 &
```

Use the following url to access the reports `http://<IP ADDRESS OF YOUR CONTAINER>:8080/amx/usp/applications/uspagent/output/<MACHINE>/coverage/report`
You can find the ip address of your container by using the `ip` command in the container.

Example:

```bash
USER@<CID>:~/amx/usp/applications/uspagent$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
173: eth0@if174: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:07 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.7/16 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 2001:db8:1::242:ac11:7/64 scope global nodad 
       valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:7/64 scope link 
       valid_lft forever preferred_lft forever
```

In this case the IP address of the container is `172.17.0.7`.
So the uri you should use is: `http://172.17.0.7:8080/amx/usp/applications/uspagent/output/x86_64-linux-gnu/coverage/report/`

## Extra information

For more information about configuring the USP agent, refer to the [configuration guidelines](doc/agent_configuration.md).

To get a better understanding of how USP works together with ambiorix, check out the [USP and Ambiorix symbiosis guidelines](doc/usp_and_ambiorix_symbiosis.md).

To get a practical explanation of how USP subscriptions work in distributed systems, check out the [USP subscription guidelines](doc/usp_subscriptions.md).