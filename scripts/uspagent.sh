#!/bin/sh

case $1 in
    start|boot)
        if [ ! -d /var/run/imtp ]; then
            mkdir -p /var/run/imtp/
        fi
        if [ ! -d /var/run/mqtt ]; then
            mkdir -p /var/run/mqtt/
        fi
        if [ ! -d /etc/config/uspagent ]; then
            mkdir -p /etc/config/uspagent/odl
        fi
        uspagent -D /etc/amx/uspagent/uspagent.odl
            ;;
    stop)
        if [ -f /var/run/uspagent.pid ]; then
            kill `cat /var/run/uspagent.pid`
        fi
        ;;
    debuginfo)
        ubus-cli "LocalAgent.?"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    fail)
        /etc/init.d/tr181-localagent stop
        /etc/init.d/tr181-mqtt stop
        /etc/init.d/tr181-mqtt start
        /etc/init.d/tr181-localagent start
        /etc/init.d/uspagent start
        ;;
    log)
        echo "TODO log USP Agent"
        ;;
        *)
        echo "Usage : $0 [start|boot|stop|debuginfo|log]"
        ;;
esac
