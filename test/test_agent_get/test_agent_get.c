/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include <usp/uspl.h>

#include "uspa.h"
#include "uspa_msghandler.h"
#include "test_agent_get.h"
#include "dummy_be.h"
#include "common.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxo_parser_t parser;

static const char* odl_config = "../common/test_config.odl";
static const char* la_definition = "../common/tr181-localagent_definition.odl";
static const char* la_defaults = "../common/tr181-localagent_mtp_disabled.odl";
static const char* la_permissions = "tr181-localagent_permissions.odl";
static const char* mqtt_definition = "../common/tr181-mqtt_definition.odl";
static const char* mqtt_defaults = "../common/tr181-mqtt_defaults.odl";
static const char* phonebook_definition = "../common/phonebook_definition.odl";
static const char* phonebook_defaults = "../common/phonebook_defaults.odl";

static const char* acl_dir = NULL;
static const char* role = "root";
static amxc_string_t acl_file;

static void uspa_initialize(void) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_permissions, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_defaults, root_obj), 0);
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);

    amxc_var_clean(&ret);
}

static void acl_initialize(void) {
    amxc_var_t* root_acls = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);

    // Mimic the acl-manager functionality
    acl_dir = GET_CHAR(&parser.config, "acl_dir");
    amxc_string_setf(&path, "%s/merged", acl_dir);
    mkdir(amxc_string_get(&path, 0), 0700);

    amxc_string_setf(&path, "%s/%s/", acl_dir, role);
    root_acls = amxa_parse_files(amxc_string_get(&path, 0));
    assert_non_null(root_acls);

    amxc_string_setf(&path, "%s/merged/%s.json", acl_dir, role);
    assert_int_equal(amxa_merge_rules(root_acls, amxc_string_get(&path, 0)), 0);

    // Init acl file to use in each of the real test functions
    amxc_string_init(&acl_file, 0);
    amxc_string_setf(&acl_file, "%s/merged/%s.json", acl_dir, role);

    amxc_string_clean(&path);
    amxc_var_delete(&root_acls);
}

static void acl_clean(void) {
    amxc_string_t path;

    amxc_string_init(&path, 0);

    remove(amxc_string_get(&acl_file, 0));

    amxc_string_setf(&path, "%s/merged", acl_dir);
    rmdir(amxc_string_get(&path, 0));

    amxc_string_clean(&acl_file);
    amxc_string_clean(&path);
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "CreateListenSocket", AMXO_FUNC(_Client_CreateListenSocket));

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    // Register data model
    amxb_register(bus_ctx, &dm);

    // Load USP back end
    assert_int_equal(amxb_be_load("/usr/bin/mods/amxb/mod-amxb-usp.so"), 0);

    handle_events();

    uspa_initialize();
    acl_initialize();
    handle_events();

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);
    acl_clean();
    assert_int_equal(amxb_be_remove("usp"), 0);
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_can_get_contact_firstname(UNUSED void** state) {
    uspl_tx_t* usp_get_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t request;
    amxc_var_t* response = NULL;
    amxc_var_t* paths = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&request);

    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, "Device.Phonebook.Contact.1.FirstName");

    assert_int_equal(uspl_tx_new(&usp_get_msg, "one", "two"), 0);
    assert_int_equal(uspl_get_new(usp_get_msg, &request), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_get_msg->pbuf, usp_get_msg->pbuf_len);

    assert_int_equal(uspa_handle_get(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    assert_non_null(usp_tx);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_get_resp_extract(usp_response, &resp_list), 0);
    response = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(response);

    amxc_var_dump(response, STDOUT_FILENO);
    assert_non_null(GETP_ARG(response, "'Device.Phonebook.Contact.1.'.FirstName"));

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_response);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_get_msg);
    amxc_var_clean(&request);
}


/**
 * Relevant requirements:
 * R-GET.0 -  If requested_path contains a Path Name that does not match any Object or Parameter in
 *            the Agent’s Supported Data Model, the Agent MUST use the 7026 - Invalid Path error in
 *            this RequestedPathResult.
 * R-GET.1 -  If the Controller making the Request does not have Read permission on an Object or
 *            Parameter matched through the requested_path field, the Object or Parameter MUST be
 *            treated as if it is not present in the Agent’s Supported data model.
 *
 * These 2 requirements imply that a get request for a specific parameter that the controller does
 * not have access to, should be answered with a get response that contains error code 7026.
 *
 * R-GET.4 -  If the Controller does not have Read permission on any of the parameters specified in
 *            result_params, these parameters MUST NOT be returned in this field. This MAY result in
 *            this field being empty.
 *
 * This requirement states that the result_params field may be empty. This does not impose any
 * further constraints after R-GET.0 and R-GET.1 have been processed. It just states that it isn't
 * an issue if all parameters are filtered out in the end
 */
void test_cannot_get_contact_lastname(UNUSED void** state) {
    uspl_tx_t* usp_get_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t request;
    amxc_var_t* response = NULL;
    amxc_var_t* paths = NULL;
    const char* requested_path = "Device.Phonebook.Contact.1.LastName";
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, requested_path);

    assert_int_equal(uspl_tx_new(&usp_get_msg, "one", "two"), 0);
    assert_int_equal(uspl_get_new(usp_get_msg, &request), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_get_msg->pbuf, usp_get_msg->pbuf_len);

    assert_int_equal(uspa_handle_get(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    assert_non_null(usp_tx);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_get_resp_extract(usp_response, &resp_list), 0);
    response = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(response);

    amxc_var_dump(response, STDOUT_FILENO);
    assert_int_equal(GETP_UINT32(response, "result.err_code"), USP_ERR_INVALID_PATH);
    assert_string_equal(GETP_CHAR(response, "result.requested_path"), requested_path);

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_response);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_get_msg);
    amxc_var_clean(&request);
}

void test_cannot_get_contact_number(UNUSED void** state) {
    uspl_tx_t* usp_get_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t request;
    amxc_var_t* response = NULL;
    amxc_var_t* paths = NULL;
    const char* requested_path = "Device.Phonebook.Contact.1.Phonenumber.";
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, requested_path);

    assert_int_equal(uspl_tx_new(&usp_get_msg, "one", "two"), 0);
    assert_int_equal(uspl_get_new(usp_get_msg, &request), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_get_msg->pbuf, usp_get_msg->pbuf_len);

    assert_int_equal(uspa_handle_get(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    assert_non_null(usp_tx);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_get_resp_extract(usp_response, &resp_list), 0);
    response = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(response);

    amxc_var_dump(response, STDOUT_FILENO);
    assert_int_equal(GETP_UINT32(response, "result.err_code"), USP_ERR_INVALID_PATH);
    assert_string_equal(GETP_CHAR(response, "result.requested_path"), requested_path);

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_response);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_get_msg);
    amxc_var_clean(&request);
}

void test_can_filter_object_and_param(UNUSED void** state) {
    uspl_tx_t* usp_get_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t request;
    amxc_var_t* response = NULL;
    amxc_var_t* paths = NULL;
    amxc_var_t* contact_1 = NULL;
    const char* requested_path = "Device.Phonebook.Contact.1.";
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, requested_path);

    assert_int_equal(uspl_tx_new(&usp_get_msg, "one", "two"), 0);
    assert_int_equal(uspl_get_new(usp_get_msg, &request), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_get_msg->pbuf, usp_get_msg->pbuf_len);

    assert_int_equal(uspa_handle_get(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    assert_non_null(usp_tx);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_get_resp_extract(usp_response, &resp_list), 0);
    response = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(response);

    amxc_var_dump(response, STDOUT_FILENO);
    assert_int_equal(GETP_UINT32(response, "result.err_code"), USP_ERR_OK);
    assert_string_equal(GETP_CHAR(response, "result.requested_path"), requested_path);
    assert_null(GETP_ARG(response, "'Device.Phonebook.Contact.1.PhoneNumber.'"));
    contact_1 = GETP_ARG(response, "'Device.Phonebook.Contact.1.'");
    assert_non_null(contact_1);
    assert_non_null(GET_ARG(contact_1, "Alias"));
    assert_non_null(GET_ARG(contact_1, "FirstName"));
    assert_null(GET_ARG(contact_1, "LastName"));

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_response);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_get_msg);
    amxc_var_clean(&request);
}

void test_can_get_la(UNUSED void** state) {
    uspl_tx_t* usp_get_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t request;
    amxc_var_t* response = NULL;
    amxc_var_t* paths = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, "LocalAgent.");

    assert_int_equal(uspl_tx_new(&usp_get_msg, "one", "two"), 0);
    assert_int_equal(uspl_get_new(usp_get_msg, &request), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_get_msg->pbuf, usp_get_msg->pbuf_len);

    assert_int_equal(uspa_handle_get(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    assert_non_null(usp_tx);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_get_resp_extract(usp_response, &resp_list), 0);
    response = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(response);

    amxc_var_dump(response, STDOUT_FILENO);
    assert_non_null(GETP_ARG(response, "'LocalAgent.'"));
    assert_non_null(GETP_ARG(response, "'LocalAgent.Controller.1.'"));
    assert_non_null(GETP_ARG(response, "'LocalAgent.ControllerTrust.'"));
    assert_non_null(GETP_ARG(response, "'LocalAgent.ControllerTrust.Role.1.'"));
    assert_non_null(GETP_ARG(response, "'LocalAgent.ControllerTrust.Role.1.Permission.1.'"));
    assert_non_null(GETP_ARG(response, "'LocalAgent.ControllerTrust.Role.1.Permission.2.'"));
    assert_non_null(GETP_ARG(response, "'LocalAgent.ControllerTrust.Role.1.Permission.3.'"));

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_response);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_get_msg);
    amxc_var_clean(&request);
}

void test_get_null_arg(UNUSED void** state) {
    uspl_tx_t* usp_get_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t request;
    amxc_var_t* paths = NULL;

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, "LocalAgent.");

    assert_int_equal(uspl_tx_new(&usp_get_msg, "one", "two"), 0);
    assert_int_equal(uspl_get_new(usp_get_msg, &request), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_get_msg->pbuf, usp_get_msg->pbuf_len);

    assert_int_equal(uspa_handle_get(NULL, &usp_tx, ""), -1);
    assert_int_equal(uspa_handle_get(usp_rx, NULL, ""), -1);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_get_msg);
    amxc_var_clean(&request);
}

void test_cannot_get_protected_param(UNUSED void** state) {
    uspl_tx_t* usp_get_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t request;
    amxc_var_t* response = NULL;
    amxc_var_t* paths = NULL;
    amxc_var_t* contact_1 = NULL;
    const char* requested_path = "Device.Phonebook.PinCode";
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, requested_path);

    assert_int_equal(uspl_tx_new(&usp_get_msg, "one", "two"), 0);
    assert_int_equal(uspl_get_new(usp_get_msg, &request), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_get_msg->pbuf, usp_get_msg->pbuf_len);

    assert_int_equal(uspa_handle_get(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    assert_non_null(usp_tx);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_get_resp_extract(usp_response, &resp_list), 0);
    response = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(response);

    amxc_var_dump(response, STDOUT_FILENO);
    assert_int_equal(GETP_UINT32(response, "result.err_code"), USP_ERR_INVALID_PATH);
    assert_string_equal(GETP_CHAR(response, "result.requested_path"), requested_path);

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_response);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_get_msg);
    amxc_var_clean(&request);
}

void test_empty_search_result_is_successful(UNUSED void** state) {
    uspl_tx_t* usp_get_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t request;
    amxc_var_t* response = NULL;
    amxc_var_t* paths = NULL;
    amxc_llist_t resp_list;
    const char* requested_path = "Device.Phonebook.Contact.[FirstName == 'Sauron'].";

    amxc_llist_init(&resp_list);
    amxc_var_init(&request);

    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, requested_path);

    assert_int_equal(uspl_tx_new(&usp_get_msg, "one", "two"), 0);
    assert_int_equal(uspl_get_new(usp_get_msg, &request), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_get_msg->pbuf, usp_get_msg->pbuf_len);

    assert_int_equal(uspa_handle_get(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    assert_non_null(usp_tx);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_get_resp_extract(usp_response, &resp_list), 0);
    response = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(response);

    amxc_var_dump(response, STDOUT_FILENO);
    assert_null(GETP_ARG(response, "'Device.Phonebook.Contact.1.'.FirstName"));
    assert_int_equal(GETP_UINT32(response, "result.error_code"), 0);
    assert_string_equal(GETP_CHAR(response, "result.requested_path"), requested_path);

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_response);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_get_msg);
    amxc_var_clean(&request);
}

void test_can_get_lower_depth(UNUSED void** state) {
    uspl_tx_t* usp_get_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t request;
    amxc_var_t* response = NULL;
    amxc_var_t* paths = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, "LocalAgent.");
    amxc_var_add_key(uint32_t, &request, "max_depth", 1);

    assert_int_equal(uspl_tx_new(&usp_get_msg, "one", "two"), 0);
    assert_int_equal(uspl_get_new(usp_get_msg, &request), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_get_msg->pbuf, usp_get_msg->pbuf_len);

    assert_int_equal(uspa_handle_get(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    assert_non_null(usp_tx);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_get_resp_extract(usp_response, &resp_list), 0);
    response = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(response);

    amxc_var_dump(response, STDOUT_FILENO);
    assert_non_null(GETP_ARG(response, "'LocalAgent.'"));
    assert_null(GETP_ARG(response, "'LocalAgent.ControllerTrust.'"));
    assert_null(GETP_ARG(response, "'LocalAgent.Controller.1.'"));
    assert_null(GETP_ARG(response, "'LocalAgent.ControllerTrust.Role.1.'"));
    assert_null(GETP_ARG(response, "'LocalAgent.ControllerTrust.Role.1.Permission.1.'"));
    assert_null(GETP_ARG(response, "'LocalAgent.ControllerTrust.Role.1.Permission.2.'"));
    assert_null(GETP_ARG(response, "'LocalAgent.ControllerTrust.Role.1.Permission.3.'"));

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_response);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_get_msg);
    amxc_var_clean(&request);
}

// Device.Phonebook. has no public parameters at depth 0 (or depth 1 in USP terms)
// but it must be possible to retrieve the object path with a USP get. There just
// won't be any parameters
void test_can_get_object_without_params(UNUSED void** state) {
    uspl_tx_t* usp_get_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t request;
    amxc_var_t* response = NULL;
    amxc_var_t* paths = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, "Device.Phonebook.");
    amxc_var_add_key(uint32_t, &request, "max_depth", 1);

    assert_int_equal(uspl_tx_new(&usp_get_msg, "one", "two"), 0);
    assert_int_equal(uspl_get_new(usp_get_msg, &request), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_get_msg->pbuf, usp_get_msg->pbuf_len);

    assert_int_equal(uspa_handle_get(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    assert_non_null(usp_tx);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_get_resp_extract(usp_response, &resp_list), 0);
    response = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(response);

    amxc_var_dump(response, STDOUT_FILENO);
    assert_non_null(GETP_ARG(response, "'Device.Phonebook.'"));
    assert_null(GETP_ARG(response, "'Device.Phonebook.Contact.1.'"));

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_response);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_get_msg);
    amxc_var_clean(&request);
}

void test_can_get_parameter_search_path(UNUSED void** state) {
    uspl_tx_t* usp_get_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t request;
    amxc_var_t* response = NULL;
    amxc_var_t* paths = NULL;
    amxc_llist_t resp_list;
    const char* requested_path = "Device.Phonebook.Contact.[FirstName == 'John'].Alias";

    amxc_llist_init(&resp_list);
    amxc_var_init(&request);

    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, requested_path);

    assert_int_equal(uspl_tx_new(&usp_get_msg, "one", "two"), 0);
    assert_int_equal(uspl_get_new(usp_get_msg, &request), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_get_msg->pbuf, usp_get_msg->pbuf_len);

    assert_int_equal(uspa_handle_get(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    assert_non_null(usp_tx);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_get_resp_extract(usp_response, &resp_list), 0);
    response = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(response);

    amxc_var_dump(response, STDOUT_FILENO);
    assert_true(test_verify_data(response, "'Device.Phonebook.Contact.1.'.Alias", "test"));
    assert_int_equal(GETP_UINT32(response, "result.error_code"), 0);
    assert_string_equal(GETP_CHAR(response, "result.requested_path"), requested_path);

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_response);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_get_msg);
    amxc_var_clean(&request);
}
