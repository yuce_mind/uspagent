MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include)

HEADERS = $(wildcard $(INCDIR)/$(COMPONENT)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c)

MOCK_WRAP = uspi_con_connect \
            uspi_con_get_fd \
            amxo_connection_add

WRAP_FUNC=-Wl,--wrap=

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
          $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. -I../mocks -I../dummy_back_end -I../common \
          -fkeep-inline-functions -fkeep-static-functions \
          -Wno-format-nonliteral -Wno-unused-variable -Wno-unused-but-set-variable \
          $(shell pkg-config --cflags cmocka) -pthread

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
           $(shell pkg-config --libs cmocka) -lamxc -lamxp -lamxj -lamxd -lamxo -lamxb -lamxa\
           -ldl -lpthread -lsahtrace -limtp -lusp -luspi

LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
