/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include <usp/uspl.h>
#include <imtp/imtp_message.h>

#include "uspa.h"
#include "uspa_imtp.h"
#include "uspa_mtp_imtp.h"
#include "test_agent_mtp_imtp.h"
#include "dummy_be.h"
#include "mock.h"
#include "common.h"

#define UNUSED __attribute__((unused))
#define GET_FD_REAL 123

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxo_parser_t parser;

static const char* odl_config = "../common/test_config.odl";
static const char* mqtt_definition = "../common/tr181-mqtt_definition.odl";
static const char* mqtt_defaults = "../common/tr181-mqtt_defaults.odl";
static const char* la_definition = "../common/tr181-localagent_definition.odl";
static const char* discovery_definition = "../../odl/discovery_definition.odl";
static const char* la_defaults = "../common/tr181-localagent_imtp.odl";

static const char* agent_eid = "proto::agent";
static const char* contr_cloud_eid = "proto::controller-cloud";
static const char* contr_container_eid = "proto::controller-container";
static uspi_con_t* con_listen = NULL;
static uspi_con_t* con_connector = NULL;
static int testpipe[2] = {};

static void connection_added(UNUSED const char* const sig_name,
                             const amxc_var_t* const data,
                             UNUSED void* const priv) {
    con_listen = data->data.data;
}

static void read_accepted_socket(void) {
    amxo_connection_t* connection = NULL;
    amxc_llist_it_t* it = NULL;
    uspi_con_t* con_accepted = NULL;

    // First connection is dummy back-end connection
    connection = amxo_connection_get_first(&parser, AMXO_BUS);

    // Second connection is accepted connection
    connection = amxo_connection_get_next(&parser, connection, AMXO_BUS);
    assert_non_null(connection);

    it = amxc_llist_get_first(&con_listen->accepted_cons);
    con_accepted = amxc_container_of(it, uspi_con_t, lit);
    connection->reader(connection->fd, con_accepted);
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "GetMTPInfo", AMXO_FUNC(_Controller_GetMTPInfo));
    amxo_resolver_ftab_add(&parser, "UpdateStatus", AMXO_FUNC(_UpdateStatus));
    amxo_resolver_ftab_add(&parser, "CreateListenSocket", AMXO_FUNC(_Client_CreateListenSocket));
    amxo_resolver_ftab_add(&parser, "assign_default_string", AMXO_FUNC(_assign_default_string));
    assert_int_equal(pipe(testpipe), 0);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    // Register data model
    amxb_register(bus_ctx, &dm);

    // Load USP back end
    assert_int_equal(amxb_be_load("/usr/bin/mods/amxb/mod-amxb-usp.so"), 0);

    handle_events();

    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, discovery_definition, root_obj), 0);
    amxb_set_config(&parser.config);

    amxp_sigmngr_add_signal(NULL, TEST_AMXO_CON_ADDED);
    amxp_slot_connect(NULL, TEST_AMXO_CON_ADDED, NULL, connection_added, NULL);

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    amxp_signal_t* sig = amxp_sigmngr_find_signal(NULL, TEST_AMXO_CON_ADDED);

    amxp_sigmngr_remove_signal(NULL, TEST_AMXO_CON_ADDED);
    amxp_signal_delete(&sig);

    will_return(__wrap_uspi_con_get_fd, testpipe[0]); // uspa_imtp_disconnect
    will_return(__wrap_uspi_con_get_fd, GET_FD_REAL); // uspa_mtp_imtp_instance_delete -> listen con
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);

    close(testpipe[0]);
    close(testpipe[1]);

    assert_int_equal(amxb_be_remove("usp"), 0);
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_can_load_imtp_from_defaults(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_get_root(&dm);
    char* status = NULL;

    assert_int_equal(amxo_parser_parse_file(&parser, la_defaults, object), 0);
    handle_events();

    will_return(__wrap_uspi_con_get_fd, GET_FD_REAL); // uspa_mtp_imtp_configure
    will_return(__wrap_uspi_con_get_fd, testpipe[0]); // uspa_imtp_connect
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);
    handle_events();

    object = amxd_dm_findf(&dm, "LocalAgent.MTP.[Protocol == 'IMTP'].");
    assert_non_null(object);
    status = amxd_object_get_value(cstring_t, object, "Status", NULL);
    assert_string_equal(status, "Up");

    free(status);
}

/**
 * This test checks if the agent can accept an incoming connection by checking the state of the
 * connection
 *
 * When a controller connects and the agent accepts the connection, they will also exchange
 * EndpointIDs. This is also verified in this test. Note that sending the EndpointID is done
 * through the USP back-end for both the agent and controller. The EndpointID is normally retrieved
 * from the config options in the USP back-end. To ensure the right EndpointID is selected before
 * writing it on the connection, the config option is updated in this unit test.
 *
 */
void test_can_accept_connection(UNUSED void** state) {
    const char* uri = "usp:/tmp/agent.sock";
    amxo_connection_t* connection = NULL;
    amxc_var_t* eid = GETP_ARG(&parser.config, "usp.EndpointID");
    amxd_object_t* object = NULL;
    imtp_tlv_t* tlv = NULL;
    const imtp_tlv_t* tlv_eid = NULL;
    amxc_var_t params;

    amxc_var_init(&params);

    // Set controller EndpointID before connecting
    amxc_var_set(cstring_t, eid, contr_container_eid);

    // Connect from to the open listen socket from here
    assert_int_equal(uspi_con_connect(&con_connector, uri), 0);

    connection = amxo_connection_get_first(&parser, AMXO_CUSTOM);
    while(connection != NULL) {
        if((connection->uri != NULL) && (strcmp(connection->uri, uri) == 0)) {
            break;
        } else {
            connection = amxo_connection_get_next(&parser, connection, AMXO_CUSTOM);
        }
    }

    assert_non_null(connection);
    assert_non_null(con_listen);

    // Set agent EndpointID before accepting
    amxc_var_set(cstring_t, eid, agent_eid);

    will_return(__wrap_uspi_con_get_fd, GET_FD_REAL); // uspa_mtp_imtp_accept
    connection->reader(connection->fd, con_listen);

    // On connect, the EndpointID of the controller was sent to the agent through the USP back-end
    read_accepted_socket();

    object = amxd_dm_findf(&dm, "Discovery.Service.1.");
    assert_non_null(object);
    amxd_object_get_params(object, &params, amxd_dm_access_private);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "EndpointID"), contr_container_eid);

    // On accept, the EndpointID of the agent was sent to the controller through the USP back-end
    assert_int_equal(uspi_con_read(con_connector, &tlv), 0);
    tlv_eid = imtp_message_get_first_tlv(tlv, imtp_tlv_type_eid);
    assert_non_null(tlv_eid);
    assert_int_equal(uspi_con_add_eid(con_connector, tlv_eid), 0);
    assert_string_equal(con_connector->eid, agent_eid);

    amxc_var_clean(&params);
    imtp_tlv_delete(&tlv);
}

void test_can_get_con_from_eid(UNUSED void** state) {
    uspi_con_t* con = NULL;
    amxc_llist_it_t* it = NULL;
    uspi_con_t* con_accepted = NULL;

    con = uspa_mtp_imtp_get_con_from_eid(contr_container_eid);
    assert_non_null(con);

    it = amxc_llist_get_first(&con_listen->accepted_cons);
    con_accepted = amxc_container_of(it, uspi_con_t, lit);
    assert_ptr_equal(con, con_accepted);
    assert_string_equal(con->eid, contr_container_eid);
    handle_events();
}

void test_can_register_dm(UNUSED void** state) {
    amxc_var_t request;
    amxc_var_t* reg_paths = NULL;
    amxc_var_t* reg_path = NULL;
    uspl_tx_t* usp_tx = NULL;
    uint8_t* payload = NULL;
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_protobuf = NULL;
    amxd_object_t* object = NULL;
    amxc_var_t params;
    const char* path = "Phonebook.";
    imtp_tlv_t* tlv = NULL;

    amxc_var_init(&request);
    amxc_var_init(&params);

    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    reg_paths = amxc_var_add_key(amxc_llist_t, &request, "reg_paths", NULL);
    reg_path = amxc_var_add(amxc_htable_t, reg_paths, NULL);
    amxc_var_add_key(cstring_t, reg_path, "path", path);

    assert_int_equal(uspl_tx_new(&usp_tx, contr_container_eid, agent_eid), 0);
    assert_int_equal(uspl_register_new(usp_tx, &request), 0);

    payload = (uint8_t*) usp_tx->pbuf;
    imtp_tlv_new(&tlv_head, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE);
    imtp_tlv_new(&tlv_protobuf, imtp_tlv_type_protobuf_bytes,
                 usp_tx->pbuf_len, payload, 0, IMTP_TLV_COPY);
    imtp_tlv_add(tlv_head, tlv_protobuf);

    assert_int_equal(uspi_con_write(con_connector, tlv_head), 0);

    will_return(__wrap_uspi_con_get_fd, GET_FD_REAL); // uspa_imtp_send
    read_accepted_socket();

    // Check Discovery.Service. data model for registered paths
    object = amxd_dm_findf(&dm, "Discovery.Service.1.Registration.1.");
    assert_non_null(object);
    amxd_object_get_params(object, &params, amxd_dm_access_private);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Path"), path);

    // Read register response without checking contents of tlv
    assert_int_equal(uspi_con_read(con_connector, &tlv), 0);

    imtp_tlv_delete(&tlv_head);
    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&request);
    amxc_var_clean(&params);
    imtp_tlv_delete(&tlv);
}

void test_can_subscribe_to_remote_dm(UNUSED void** state) {
    const char* notif_type = "ValueChange";
    const char* ref_list = "MQTT.,Phonebook.";
    const char* recipient = "LocalAgent.Controller.1.";
    amxc_var_t response;
    amxc_var_t* result = NULL;
    uspl_tx_t* usp_tx = NULL;
    amxc_llist_t resp_list;
    uint8_t* payload = NULL;
    imtp_tlv_t* tlv = NULL;
    imtp_tlv_t* tlv_protobuf = NULL;

    // Add a subscription instance with a local and remote data model
    add_subscription_instance(&dm, notif_type, ref_list, recipient);

    // Provide add response from client side before handling events
    assert_int_equal(uspl_tx_new(&usp_tx, contr_container_eid, agent_eid), 0);

    amxc_llist_init(&resp_list);
    amxc_var_init(&response);
    amxc_var_set_type(&response, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &response, "path", "LocalAgent.Subscription.3.");
    amxc_var_add_key(amxc_htable_t, &response, "parameters", NULL);
    result = amxc_var_add_key(amxc_htable_t, &response, "result", NULL);
    amxc_var_add_key(uint32_t, result, "err_code", 0);
    amxc_var_add_key(cstring_t, result, "requested_path", "LocalAgent.Subscription.");
    amxc_llist_append(&resp_list, &response.lit);
    assert_int_equal(uspl_add_resp_new(usp_tx, &resp_list, "123"), 0);

    payload = (uint8_t*) usp_tx->pbuf;
    imtp_tlv_new(&tlv, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE);
    imtp_tlv_new(&tlv_protobuf, imtp_tlv_type_protobuf_bytes,
                 usp_tx->pbuf_len, payload, 0, IMTP_TLV_COPY);
    imtp_tlv_add(tlv, tlv_protobuf);

    // Send add response, then handle events
    uspi_con_write(con_connector, tlv);
    handle_events();

    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&response);
    uspl_tx_delete(&usp_tx);
    imtp_tlv_delete(&tlv);
}

void test_can_send_notification_to_controller(UNUSED void** state) {
    amxd_object_t* object = NULL;
    char* sub_id = NULL;
    uspl_tx_t* usp_tx = NULL;
    amxc_var_t request;
    amxc_var_t* vc_var = NULL;
    imtp_tlv_t* tlv = NULL;
    uint8_t* payload = NULL;
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_protobuf = NULL;
    int8_t* buffer = calloc(1, 1024);
    imtp_tlv_t* tlv_recv = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, contr_container_eid, agent_eid), 0);

    object = amxd_dm_findf(&dm, "LocalAgent.Subscription.1.");
    assert_non_null(object);
    sub_id = amxd_object_get_value(cstring_t, object, "ID", NULL);
    assert_non_null(sub_id);

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &request, "subscription_id", sub_id);
    amxc_var_add_key(bool, &request, "send_resp", false);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_VALUE_CHANGE);
    vc_var = amxc_var_add_key(amxc_htable_t, &request, "value_change", NULL);
    amxc_var_add_key(cstring_t, vc_var, "param_path", "path.foo");
    amxc_var_add_key(cstring_t, vc_var, "param_value", "bar");

    assert_int_equal(uspl_notify_new(usp_tx, &request), 0);

    payload = (uint8_t*) usp_tx->pbuf;
    imtp_tlv_new(&tlv_head, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE);
    imtp_tlv_new(&tlv_protobuf, imtp_tlv_type_protobuf_bytes,
                 usp_tx->pbuf_len, payload, 0, IMTP_TLV_COPY);
    imtp_tlv_add(tlv_head, tlv_protobuf);

    assert_int_equal(uspi_con_write(con_connector, tlv_head), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    read_accepted_socket();

    read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);
    assert_non_null(imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes));

    free(sub_id);
    imtp_tlv_delete(&tlv_recv);
    imtp_tlv_delete(&tlv_head);
    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&request);
}

void test_can_remove_remote_subscription(UNUSED void** state) {
    const char* notif_type = "ValueChange";
    const char* ref_list = "LocalAgent.,Phonebook.";
    const char* recipient = "LocalAgent.Controller.1.";
    amxc_var_t response;
    amxc_var_t* result = NULL;
    amxc_var_t* affected_paths = NULL;
    uspl_tx_t* usp_tx = NULL;
    amxc_llist_t resp_list;
    uint8_t* payload = NULL;
    imtp_tlv_t* tlv = NULL;
    imtp_tlv_t* tlv_protobuf = NULL;

    // Remove remote subscription
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Subscription.");
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    // Provide delete response from client side before handling events
    assert_int_equal(uspl_tx_new(&usp_tx, contr_cloud_eid, agent_eid), 0);

    amxc_llist_init(&resp_list);
    amxc_var_init(&response);
    amxc_var_set_type(&response, AMXC_VAR_ID_HTABLE);
    affected_paths = amxc_var_add_key(amxc_llist_t, &response, "affected_paths", NULL);
    amxc_var_add(cstring_t, affected_paths, "LocalAgent.Subscription.1.");
    amxc_var_add_key(amxc_llist_t, &response, "unaffected_paths", NULL);
    result = amxc_var_add_key(amxc_htable_t, &response, "result", NULL);
    amxc_var_add_key(uint32_t, result, "err_code", 0);
    amxc_var_add_key(cstring_t, result, "requested_path", "LocalAgent.Subscription.1.");
    amxc_llist_append(&resp_list, &response.lit);
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "123"), 0);

    payload = (uint8_t*) usp_tx->pbuf;
    imtp_tlv_new(&tlv, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE);
    imtp_tlv_new(&tlv_protobuf, imtp_tlv_type_protobuf_bytes,
                 usp_tx->pbuf_len, payload, 0, IMTP_TLV_COPY);
    imtp_tlv_add(tlv, tlv_protobuf);

    // Send delete response, then handle events
    uspi_con_write(con_connector, tlv);
    handle_events();

    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&response);
    uspl_tx_delete(&usp_tx);
    imtp_tlv_delete(&tlv);
}

void test_can_handle_tlv_sub(UNUSED void** state) {
    const char* object = "MQTT.";
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_sub = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_int_equal(imtp_tlv_new(&tlv_head, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE), 0);
    assert_int_equal(imtp_tlv_new(&tlv_sub, imtp_tlv_type_subscribe, strlen(object) + 1, (char*) object, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_add(tlv_head, tlv_sub), 0);

    uspi_con_write(con_connector, tlv_head);
    read_accepted_socket();
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.[NotifType=='AmxNotification'].", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_string_equal(GETP_CHAR(&ret, "0.0.ReferenceList"), object);
    assert_string_equal(GETP_CHAR(&ret, "0.0.NotifType"), "AmxNotification");

    amxc_var_clean(&ret);
    imtp_tlv_delete(&tlv_head);
}

void test_can_send_amx_notification(UNUSED void** state) {
    const char* object = "MQTT.Client.1.";
    amxc_var_t values;
    amxc_var_t ret;
    amxc_var_t notification;
    imtp_tlv_t* tlv = NULL;
    const imtp_tlv_t* tlv_pbuf = NULL;
    uspl_rx_t* usp_rx = NULL;

    amxc_var_init(&notification);
    amxc_var_init(&values);
    amxc_var_init(&ret);

    // Read garbage left on fd from other tests
    while(uspi_con_read(con_connector, &tlv) == 0) {
        imtp_tlv_delete(&tlv);
    }

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "TransportProtocol", "TLS");
    assert_int_equal(amxb_set(bus_ctx, object, &values, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    will_return(__wrap_uspi_con_get_fd, GET_FD_REAL); // uspa_imtp_send
    handle_events();

    // Read notification at other side
    assert_int_equal(uspi_con_read(con_connector, &tlv), 0);

    tlv_pbuf = imtp_message_get_first_tlv(tlv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_pbuf);

    usp_rx = uspl_msghandler_unpack_protobuf(tlv_pbuf->value + tlv_pbuf->offset, tlv_pbuf->length);
    assert_non_null(usp_rx);
    assert_int_equal(uspl_msghandler_msg_type(usp_rx), USP__HEADER__MSG_TYPE__NOTIFY);

    assert_int_equal(uspl_notify_extract(usp_rx, &notification), 0);
    amxc_var_dump(&notification, STDOUT_FILENO);

    imtp_tlv_delete(&tlv);
    uspl_rx_delete(&usp_rx);
    amxc_var_clean(&notification);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
}

void test_can_handle_tlv_unsub(UNUSED void** state) {
    const char* object = "MQTT.";
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_sub = NULL;
    imtp_tlv_t* tlv_resp = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_int_equal(imtp_tlv_new(&tlv_head, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE), 0);
    assert_int_equal(imtp_tlv_new(&tlv_sub, imtp_tlv_type_unsubscribe, strlen(object) + 1, (char*) object, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_add(tlv_head, tlv_sub), 0);

    uspi_con_write(con_connector, tlv_head);
    read_accepted_socket();
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.[NotifType=='AmxNotification'].", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_null(GETP_ARG(&ret, "0.0.ReferenceList"));
    assert_null(GETP_ARG(&ret, "0.0.NotifType"));

    amxc_var_clean(&ret);
    imtp_tlv_delete(&tlv_head);
    imtp_tlv_delete(&tlv_resp);
}

void test_can_remove_connection(UNUSED void** state) {
    amxd_object_t* object = NULL;

    // Delete accepted connection on client side
    uspi_con_disconnect(&con_connector);

    will_return(__wrap_uspi_con_get_fd, GET_FD_REAL); // uspa_imtp_connection_remove
    read_accepted_socket();

    object = amxd_dm_findf(&dm, "Discovery.Service.1.");
    assert_null(object);
}
