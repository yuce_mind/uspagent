/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include <usp/uspl.h>

#include "uspa.h"
#include "uspa_msghandler.h"
#include "test_agent_del.h"
#include "dummy_be.h"
#include "common.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;

static amxo_parser_t parser;
static const char* odl_config = "../common/test_config.odl";
static const char* la_definition = "../common/tr181-localagent_definition.odl";
static const char* la_defaults = "../common/tr181-localagent_mtp_disabled.odl";
static const char* mqtt_definition = "../common/tr181-mqtt_definition.odl";
static const char* mqtt_defaults = "../common/tr181-mqtt_defaults.odl";
static const char* phonebook_odl = "phonebook.odl";

static const char* acl_dir = NULL;
static const char* role = "root";
static amxc_string_t acl_file;

static void build_delete_var(amxc_var_t* del_var, const char* path) {
    amxc_var_t* requests = NULL;
    amxc_var_set_type(del_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, del_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, del_var, "requests", NULL);
    amxc_var_add(cstring_t, requests, path);
}

static void uspa_initialize(void) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_odl, root_obj), 0);
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);

    amxc_var_clean(&ret);
}

static void acl_initialize(void) {
    amxc_var_t* root_acls = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);

    // Mimic the acl-manager functionality
    acl_dir = GET_CHAR(&parser.config, "acl_dir");
    amxc_string_setf(&path, "%s/merged", acl_dir);
    mkdir(amxc_string_get(&path, 0), 0700);

    amxc_string_setf(&path, "%s/%s/", acl_dir, role);
    root_acls = amxa_parse_files(amxc_string_get(&path, 0));
    assert_non_null(root_acls);

    amxc_string_setf(&path, "%s/merged/%s.json", acl_dir, role);
    assert_int_equal(amxa_merge_rules(root_acls, amxc_string_get(&path, 0)), 0);

    // Init acl file to use in each of the real test functions
    amxc_string_init(&acl_file, 0);
    amxc_string_setf(&acl_file, "%s/merged/%s.json", acl_dir, role);

    amxc_string_clean(&path);
    amxc_var_delete(&root_acls);
}

static void acl_clean(void) {
    amxc_string_t path;

    amxc_string_init(&path, 0);

    remove(amxc_string_get(&acl_file, 0));

    amxc_string_setf(&path, "%s/merged", acl_dir);
    rmdir(amxc_string_get(&path, 0));

    amxc_string_clean(&acl_file);
    amxc_string_clean(&path);
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "CreateListenSocket", AMXO_FUNC(_Client_CreateListenSocket));

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    // Register data model
    amxb_register(bus_ctx, &dm);

    // Load USP back end
    assert_int_equal(amxb_be_load("/usr/bin/mods/amxb/mod-amxb-usp.so"), 0);

    handle_events();

    uspa_initialize();
    acl_initialize();

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);
    acl_clean();
    assert_int_equal(amxb_be_remove("usp"), 0);
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_can_delete_phonebook_contact(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    const char* delete_path = "Device.Phonebook.Contact.2.";
    amxc_var_t* get_response = NULL;
    amxc_var_t phonebook;
    amxc_var_t del_var;

    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.Contact.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 2);

    // del_var will contain data to del from dm
    amxc_var_init(&del_var);
    build_delete_var(&del_var, delete_path);

    // usp_del_msg holds protobuf
    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len); // unpack to have rx struct

    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    // check content of phonebook again
    amxc_var_clean(&phonebook);
    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.Contact.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 1);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&phonebook);
    amxc_var_clean(&del_var);
}

void test_passing_null_arg(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    const char* delete_path = "Device.Phonebook.Contact.2.";
    amxc_var_t del_var;

    // del_var will contain data to del from dm
    amxc_var_init(&del_var);
    build_delete_var(&del_var, delete_path);

    // usp_del_msg holds protobuf
    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len);

    assert_int_equal(uspa_handle_delete(NULL, &usp_tx, NULL), -1);
    assert_int_equal(uspa_handle_delete(usp_rx, NULL, NULL), -1);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&del_var);
}

void test_del_req_with_non_existing_data(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t phonebook;
    amxc_var_t del_var;
    amxc_var_t* get_response = NULL;

    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.Contact.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 1);

    // del_var will contain data to del from dm
    amxc_var_init(&del_var);
    build_delete_var(&del_var, "Device.Phonebook.Contakt.1.");

    // usp_del_msg holds protobuf
    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len); // unpack to have rx struct

    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    // check content of phonebook again
    amxc_var_clean(&phonebook);
    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.Contact.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 1);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&phonebook);
    amxc_var_clean(&del_var);
}

void test_del_req_with_readonly_data(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t phonebook;
    amxc_var_t del_var;
    amxc_var_t* get_response = NULL;

    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.PrivContact.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 1);

    // del_var will contain data to del from dm
    amxc_var_init(&del_var);
    build_delete_var(&del_var, "Device.Phonebook.PrivContact.1.");

    // usp_del_msg holds protobuf
    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len); // unpack to have rx struct

    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    // check content of phonebook again
    amxc_var_clean(&phonebook);
    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.PrivContact.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 1);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&phonebook);
    amxc_var_clean(&del_var);
}

void test_del_req_with_wildcard_data(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t phonebook;
    amxc_var_t del_var;
    amxc_var_t* get_response = NULL;

    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.Page.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 4);

    // del_var will contain data to del from dm
    amxc_var_init(&del_var);
    build_delete_var(&del_var, "Device.Phonebook.Page.*.");

    // usp_del_msg holds protobuf
    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len); // unpack to have rx struct

    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    // check content of phonebook again
    amxc_var_clean(&phonebook);
    amxc_var_init(&phonebook);
    assert_int_not_equal(amxb_get(bus_ctx, "Device.Phonebook.Contact.Page.", 0, &phonebook, 5), 0);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&phonebook);
    amxc_var_clean(&del_var);
}

void test_del_la_mtp(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t ret;
    amxc_var_t del_var;
    amxc_var_t* get_response = NULL;
    const char* requested_path = "LocalAgent.MTP.1.";

    amxc_var_init(&ret);

    amxb_get(bus_ctx, "LocalAgent.MTP.", 0, &ret, 1);
    amxc_var_dump(&ret, STDOUT_FILENO);
    get_response = GETI_ARG(&ret, 0);
    assert_non_null(get_response);
    assert_false(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, get_response)));

    amxc_var_init(&del_var);
    build_delete_var(&del_var, requested_path);

    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len);
    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    handle_events();

    amxc_var_clean(&ret);
    amxc_var_init(&ret);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    assert_int_not_equal(amxb_get(bus_ctx, requested_path, 0, &ret, 1), 0);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&del_var);
    amxc_var_clean(&ret);
}

void test_cannot_delete_phonenumber(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t ret;
    amxc_var_t del_var;
    amxc_var_t* get_response = NULL;
    const char* requested_path = "Device.Phonebook.Contact.1.PhoneNumber.1.";

    amxc_var_init(&del_var);
    build_delete_var(&del_var, requested_path);

    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len); // unpack to have rx struct

    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    amxc_var_init(&ret);
    amxb_get(bus_ctx, requested_path, 0, &ret, 5);
    amxc_var_dump(&ret, STDOUT_FILENO);
    get_response = GETI_ARG(&ret, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 1);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&ret);
    amxc_var_clean(&del_var);
}
