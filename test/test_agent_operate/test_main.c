/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "test_agent_operate.h"

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_can_invoke_async_operate),
        cmocka_unit_test(test_async_operate_sends_event),
        cmocka_unit_test(test_failed_async_operate_sends_event),
        cmocka_unit_test(test_disabled_subscription_disconnects_slots),
        cmocka_unit_test(test_enabled_subscription_connects_slots),
        cmocka_unit_test(test_async_operate_with_output_args),
        cmocka_unit_test(test_two_async_operates_have_unique_cmd_keys),
        cmocka_unit_test(test_async_operate_without_braces),
        cmocka_unit_test(test_async_operate_not_allowed),
        cmocka_unit_test(test_async_operate_protected_allowed),
        cmocka_unit_test(test_can_invoke_reboot),
        cmocka_unit_test(test_sync_operate_with_output_args),
        cmocka_unit_test(test_sync_operate_with_output_args_failure),
        cmocka_unit_test(test_send_resp_false_gives_no_response),
        cmocka_unit_test(test_sync_operate_not_allowed),
        cmocka_unit_test(test_sync_operate_not_supported),
        cmocka_unit_test(test_sync_operate_not_instantiated),
        cmocka_unit_test(test_sync_operate_protected_allowed),
    };
    return cmocka_run_group_tests(tests, test_uspa_setup, test_uspa_teardown);
}
