/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include <usp/uspl.h>
#include <imtp/imtp_message.h>

#include "uspa.h"
#include "uspa_imtp.h"
#include "uspa_agent.h"
#include "uspa_imtp_mqtt.h"
#include "test_agent_imtp.h"
#include "dummy_be.h"
#include "mock.h"
#include "common.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;

static amxo_parser_t parser;
static const char* odl_config = "../common/test_config.odl";
static const char* la_definition = "../common/tr181-localagent_definition.odl";
static const char* la_defaults = "../common/tr181-localagent_mtp_enabled.odl";
static const char* la_mqtt_defaults = "tr181-localagent_mtp_mqtt.odl";
static const char* mqtt_config = "mqtt_config.odl";
static const char* mqtt_definition = "../common/tr181-mqtt_definition.odl";
static const char* mqtt_defaults = "../common/tr181-mqtt_defaults.odl";
static const char* phonebook_definition = "../common/phonebook_definition.odl";
static const char* phonebook_defaults = "../common/phonebook_defaults.odl";

static int testpipe[2] = {};

static uspl_tx_t* test_create_usp_tx(void) {
    uspl_tx_t* usp_tx = NULL;
    amxc_var_t request;

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_LIST);
    amxc_var_add(cstring_t, &request, "path");

    assert_int_equal(uspl_tx_new(&usp_tx, "me", "you"), 0);
    assert_int_equal(uspl_get_new(usp_tx, &request), 0);

    amxc_var_clean(&request);
    return usp_tx;
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "CreateListenSocket", AMXO_FUNC(_Client_CreateListenSocket));
    amxo_resolver_ftab_add(&parser, "UpdateStatus", AMXO_FUNC(_UpdateStatus));

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    // Register data model
    amxb_register(bus_ctx, &dm);

    // Load USP back end
    assert_int_equal(amxb_be_load("/usr/bin/mods/amxb/mod-amxb-usp.so"), 0);

    handle_events();

    assert_int_equal(pipe(testpipe), 0);

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    will_return(__wrap_uspi_con_get_fd, testpipe[0]); // uspa_imtp_disconnect
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);

    close(testpipe[0]);
    close(testpipe[1]);

    assert_int_equal(amxb_be_remove("usp"), 0);
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_enabled_defaults_create_imtp_connection(UNUSED void** state) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    uspi_con_t* con = NULL;
    uspa_mtp_t* mtp = NULL;
    amxc_var_t ret;
    char* agent_eid = NULL;

    amxc_var_init(&ret);

    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_mqtt_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_defaults, root_obj), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[0]); // uspa_imtp_connect
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);

    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "MQTT.Client.1.", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    assert_true(GETP_BOOL(&ret, "0.0.Enable"));

    agent_eid = uspa_agent_get_eid();
    assert_non_null(agent_eid);
    assert_string_equal(GETP_CHAR(&ret, "0.0.ClientID"), agent_eid);

    con = uspa_imtp_con_from_la_mtp("LocalAgent.MTP.1.");
    assert_non_null(con);
    mtp = (uspa_mtp_t*) con->priv;
    assert_string_equal(mtp->la_mtp, "LocalAgent.MTP.1.");

    //assert_string_equal(GETI_CHAR(&mtp->contr_mtps, 0), "LocalAgent.Controller.1.MTP.1.");
    assert_string_equal(mtp->mtp_data.mqtt->reference, "MQTT.Client.1");

    free(agent_eid);
    amxc_var_clean(&ret);
}

void test_mqtt_client_status_updates_la_mtp_status(UNUSED void** state) {
    amxc_var_t ret;
    amxd_trans_t trans;

    amxc_var_init(&ret);
    amxd_trans_init(&trans);

    assert_int_equal(amxb_get(bus_ctx, "MQTT.Client.1.", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_string_equal(GETP_CHAR(&ret, "0.0.Status"), "Disabled");

    amxd_trans_select_pathf(&trans, "MQTT.Client.1.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "Status", "Connected");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "MQTT.Client.1.", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_string_equal(GETP_CHAR(&ret, "0.0.Status"), "Connected");

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.MTP.1.", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_string_equal(GETP_CHAR(&ret, "0.0.Status"), "Up");

    amxd_trans_clean(&trans);
    amxc_var_clean(&ret);
}

void test_can_disable_and_enable_imtp_connection(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t values;
    uspi_con_t* con = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&values);

    con = uspa_imtp_con_from_la_mtp("LocalAgent.MTP.1.");
    assert_non_null(con);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &values, "Enable", false);
    assert_int_equal(amxb_set(bus_ctx, "LocalAgent.MTP.1.", &values, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    will_return(__wrap_uspi_con_get_fd, testpipe[0]); // uspa_imtp_disconnect
    handle_events();

    con = uspa_imtp_con_from_la_mtp("LocalAgent.MTP.1.");
    assert_null(con);
    amxc_var_clean(&ret);

    amxc_var_init(&ret);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &values, "Enable", true);
    assert_int_equal(amxb_set(bus_ctx, "LocalAgent.MTP.1.", &values, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    will_return(__wrap_uspi_con_get_fd, testpipe[0]); // uspa_imtp_connect
    handle_events();

    con = uspa_imtp_con_from_la_mtp("LocalAgent.MTP.1.");
    assert_non_null(con);

    amxc_var_clean(&ret);
}

void test_can_send_tlv_over_fd(UNUSED void** state) {
    int fd = 1;
    uspl_tx_t* usp_tx = NULL;
    char* topic = strdup("test");
    int8_t* buffer = calloc(1, 1024);
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_part = NULL;
    char json_buf[1024] = {};
    amxc_var_t usp_props;
    uspi_con_t* con = NULL;
    amxc_var_t mtp_info;

    con = uspa_imtp_con_from_la_mtp("LocalAgent.MTP.1.");
    assert_non_null(con);

    amxc_var_init(&mtp_info);
    amxc_var_init(&usp_props);
    memset(json_buf, 0, 1024);

    usp_tx = test_create_usp_tx();

    amxc_var_set_type(&mtp_info, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &mtp_info, "protocol", "MQTT");
    amxc_var_add_key(cstring_t, &mtp_info, "topic", topic);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    assert_true(uspa_imtp_send(con, usp_tx, &mtp_info) > 0);

    read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    assert_non_null(imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes));
    assert_non_null(imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_topic));
    assert_non_null(imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_mqtt_props));

    tlv_part = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_mqtt_props);
    assert_non_null(tlv_part);

    memcpy(json_buf, tlv_part->value + tlv_part->offset, tlv_part->length);

    assert_int_equal(amxc_var_set(jstring_t, &usp_props, json_buf), 0);
    amxc_var_cast(&usp_props, AMXC_VAR_ID_ANY);
    amxc_var_dump(&usp_props, STDOUT_FILENO);
    assert_true(test_verify_data(&usp_props, "content-type", "application/vnd.bbf.usp.msg"));
    assert_true(test_verify_data(&usp_props, "user-props.usp-err-id", "you/0"));
    assert_true(test_verify_data(&usp_props, "response-topic", "reply-to-me"));

    amxc_var_clean(&usp_props);
    amxc_var_clean(&mtp_info);
    imtp_tlv_delete(&tlv_recv);
    uspl_tx_delete(&usp_tx);
    free(topic);
}

void test_can_send_tlv_with_error_content_type(UNUSED void** state) {
    int fd = 1;
    uspl_tx_t* usp_tx = NULL;
    char* topic = strdup("test");
    int8_t* buffer = calloc(1, 1024);
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_part = NULL;
    char json_buf[1024] = {};
    amxc_var_t usp_props;
    amxc_var_t error;
    uspi_con_t* con = NULL;
    amxc_var_t mtp_info;

    con = uspa_imtp_con_from_la_mtp("LocalAgent.MTP.1.");
    assert_non_null(con);

    amxc_var_init(&mtp_info);
    amxc_var_init(&error);
    amxc_var_init(&usp_props);
    memset(json_buf, 0, 1024);

    amxc_var_set_type(&error, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &error, "err_code", USP_ERR_GENERAL_FAILURE);
    amxc_var_add_key(cstring_t, &error, "err_msg", uspl_error_code_to_str(USP_ERR_GENERAL_FAILURE));

    assert_int_equal(uspl_tx_new(&usp_tx, "me", "you"), 0);
    assert_int_equal(uspl_error_resp_new(usp_tx, &error, "456"), 0);

    amxc_var_set_type(&mtp_info, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &mtp_info, "protocol", "MQTT");
    amxc_var_add_key(cstring_t, &mtp_info, "topic", topic);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    assert_true(uspa_imtp_send(con, usp_tx, &mtp_info) > 0);

    read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    assert_non_null(imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes));
    assert_non_null(imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_topic));

    tlv_part = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_mqtt_props);
    assert_non_null(tlv_part);

    memcpy(json_buf, tlv_part->value + tlv_part->offset, tlv_part->length);

    assert_int_equal(amxc_var_set(jstring_t, &usp_props, json_buf), 0);
    amxc_var_cast(&usp_props, AMXC_VAR_ID_ANY);
    amxc_var_dump(&usp_props, STDOUT_FILENO);
    assert_true(test_verify_data(&usp_props, "content-type", "application/vnd.bbf.usp.error"));
    assert_true(test_verify_data(&usp_props, "user-props.usp-err-id", "you/456"));
    assert_true(test_verify_data(&usp_props, "response-topic", "reply-to-me"));

    amxc_var_clean(&mtp_info);
    amxc_var_clean(&error);
    amxc_var_clean(&usp_props);
    imtp_tlv_delete(&tlv_recv);
    uspl_tx_delete(&usp_tx);
    free(topic);
}

void test_ignore_tlv_of_type_eid(UNUSED void** state) {
    uspi_con_t* con = NULL;
    imtp_tlv_t* tlv = NULL;
    imtp_tlv_t* tlv_eid = NULL;
    char* eid = strdup("proto::test");

    imtp_tlv_new(&tlv, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE);
    imtp_tlv_new(&tlv_eid, imtp_tlv_type_eid, strlen(eid), eid, 0, IMTP_TLV_TAKE);
    imtp_tlv_add(tlv, tlv_eid);

    assert_int_equal(uspa_imtp_mqtt_handle_tlv(con, tlv), 0);

    imtp_tlv_delete(&tlv);
}

void test_multiple_controller_use_same_connection(UNUSED void** state) {
    amxd_trans_t trans;
    uspi_con_t* con_1 = NULL;
    uspi_con_t* con_2 = NULL;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "EndpointID", "proto::controller-2");
    amxd_trans_set_value(cstring_t, &trans, "AssignedRole", "LocalAgent.ControllerTrust.Role.1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".MTP");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "MQTT");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".MQTT");
    amxd_trans_set_value(cstring_t, &trans, "Reference", "MQTT.Client.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxc_var_dump(&trans.retvals, STDOUT_FILENO);

    handle_events();

    con_1 = uspa_imtp_con_from_contr("LocalAgent.Controller.1.");
    assert_non_null(con_1);
    con_2 = uspa_imtp_con_from_contr("LocalAgent.Controller.2.");
    assert_non_null(con_2);
    assert_ptr_equal(con_1, con_2);

    amxd_trans_clean(&trans);
}

void test_removed_contr_mtp_will_not_use_imtp_connection(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.2.MTP.");
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);

    handle_events();
    assert_null(uspa_imtp_con_from_contr("LocalAgent.Controller.2."));

    amxd_trans_clean(&trans);
}
