/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <imtp/imtp_connection.h>

#include "mock.h"

int __real_uspi_con_connect(uspi_con_t** con, const char* uri);

int __wrap_uspi_con_connect(uspi_con_t** con, const char* uri) {
    int retval = -1;

    if((strcmp(uri, "dummy:/tmp/dummy.sock") == 0) ||
       ( strcmp(uri, "pcb:/tmp/test.sock") == 0)) {
        retval = __real_uspi_con_connect(con, uri);
    } else if(strcmp(uri, "usp:/tmp/agent.sock") == 0) {
        retval = __real_uspi_con_connect(con, uri);
    } else if(strncmp(uri, "usp", 3) == 0) {
        imtp_connection_t* icon = NULL;
        imtp_connection_listen(&icon, uri, NULL);
        retval = __real_uspi_con_connect(con, uri);
        imtp_connection_delete(&icon);
    }

    return retval;
}

int __real_uspi_con_get_fd(uspi_con_t* con);

int __wrap_uspi_con_get_fd(UNUSED uspi_con_t* con) {
    int retval = mock_type(int);
    if(retval == 123) {
        return __real_uspi_con_get_fd(con);
    } else {
        return retval;
    }
}

int __real_amxo_connection_add(amxo_parser_t* parser,
                               int fd,
                               amxo_fd_read_t reader,
                               const char* uri,
                               amxo_con_type_t type,
                               void* priv);

// For testing the IMTP connection, we need to get the uspi_con_t struct for
// the listen connection in the unit test. It can be passed via a data variant
// to a slot
int __wrap_amxo_connection_add(amxo_parser_t* parser,
                               int fd,
                               amxo_fd_read_t reader,
                               const char* uri,
                               amxo_con_type_t type,
                               void* priv) {
    int retval = -1;
    if((uri != NULL) && (strcmp(uri, "usp:/tmp/agent.sock") == 0)) {
        amxc_var_t var;
        amxc_var_init(&var);
        var.data.data = priv;
        amxp_sigmngr_trigger_signal(NULL, TEST_AMXO_CON_ADDED, &var);
        amxc_var_clean(&var);
    }

    retval = __real_amxo_connection_add(parser, fd, reader, uri, type, priv);

    return retval;
}