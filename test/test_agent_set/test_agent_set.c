/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include <usp/uspl.h>

#include "uspa.h"
#include "uspa_msghandler.h"
#include "test_agent_set.h"
#include "dummy_be.h"
#include "common.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;

static amxo_parser_t parser;
static const char* odl_config = "../common/test_config.odl";
static const char* la_definition = "../common/tr181-localagent_definition.odl";
static const char* la_defaults = "../common/tr181-localagent_mtp_disabled.odl";
static const char* mqtt_definition = "../common/tr181-mqtt_definition.odl";
static const char* mqtt_defaults = "../common/tr181-mqtt_defaults.odl";
static const char* phonebook_definition = "../common/phonebook_definition.odl";
static const char* phonebook_defaults = "../common/phonebook_defaults.odl";

static const char* acl_dir = NULL;
static const char* role = "root";
static amxc_string_t acl_file;

static void uspa_initialize(void) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_defaults, root_obj), 0);
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);

    amxc_var_clean(&ret);
}

static void acl_initialize(void) {
    amxc_var_t* root_acls = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);

    // Mimic the acl-manager functionality
    acl_dir = GET_CHAR(&parser.config, "acl_dir");
    amxc_string_setf(&path, "%s/merged", acl_dir);
    mkdir(amxc_string_get(&path, 0), 0700);

    amxc_string_setf(&path, "%s/%s/", acl_dir, role);
    root_acls = amxa_parse_files(amxc_string_get(&path, 0));
    assert_non_null(root_acls);

    amxc_string_setf(&path, "%s/merged/%s.json", acl_dir, role);
    assert_int_equal(amxa_merge_rules(root_acls, amxc_string_get(&path, 0)), 0);

    // Init acl file to use in each of the real test functions
    amxc_string_init(&acl_file, 0);
    amxc_string_setf(&acl_file, "%s/merged/%s.json", acl_dir, role);

    amxc_string_clean(&path);
    amxc_var_delete(&root_acls);
}

static void acl_clean(void) {
    amxc_string_t path;

    amxc_string_init(&path, 0);

    remove(amxc_string_get(&acl_file, 0));

    amxc_string_setf(&path, "%s/merged", acl_dir);
    rmdir(amxc_string_get(&path, 0));

    amxc_string_clean(&acl_file);
    amxc_string_clean(&path);
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "CreateListenSocket", AMXO_FUNC(_Client_CreateListenSocket));

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    // Register data model
    amxb_register(bus_ctx, &dm);

    // Load USP back end
    assert_int_equal(amxb_be_load("/usr/bin/mods/amxb/mod-amxb-usp.so"), 0);

    handle_events();

    uspa_initialize();
    acl_initialize();
    handle_events();

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);
    acl_clean();
    assert_int_equal(amxb_be_remove("usp"), 0);
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

// This is an ACL verification test. Changing the parameter is allowed.
void test_can_set_contact_firstname(UNUSED void** state) {
    uspl_tx_t* usp_set = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t set_var;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    amxd_object_t* phonebook_inst = NULL;
    char* firstname = NULL;

    amxc_var_init(&set_var);
    amxc_var_set_type(&set_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &set_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &set_var, "requests", NULL);
    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", "Device.Phonebook.Contact.1.");
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);
    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "FirstName");
    amxc_var_add_key(cstring_t, param, "value", "Foo");
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_set, "one", "two"), 0);
    assert_int_equal(uspl_set_new(usp_set, &set_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_set->pbuf, usp_set->pbuf_len);

    assert_int_equal(uspa_handle_set(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    phonebook_inst = amxd_dm_findf(&dm, "Device.Phonebook.Contact.1.");
    assert_non_null(phonebook_inst);
    firstname = amxd_object_get_value(cstring_t, phonebook_inst, "FirstName", NULL);
    assert_string_equal(firstname, "Foo");

    free(firstname);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_set);
    amxc_var_clean(&set_var);
}

// This is an ACL verification test. Changing the parameter is not allowed.
void test_cannot_set_contact_lastname(UNUSED void** state) {
    uspl_tx_t* usp_set = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t set_var;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    amxd_object_t* phonebook_inst = NULL;
    char* lastname = NULL;

    amxc_var_init(&set_var);
    amxc_var_set_type(&set_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &set_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &set_var, "requests", NULL);
    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", "Device.Phonebook.Contact.1.");
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);
    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "LastName");
    amxc_var_add_key(cstring_t, param, "value", "Bar");
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_set, "one", "two"), 0);
    assert_int_equal(uspl_set_new(usp_set, &set_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_set->pbuf, usp_set->pbuf_len);

    assert_int_equal(uspa_handle_set(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    phonebook_inst = amxd_dm_findf(&dm, "Device.Phonebook.Contact.1.");
    assert_non_null(phonebook_inst);
    lastname = amxd_object_get_value(cstring_t, phonebook_inst, "LastName", NULL);
    assert_string_equal(lastname, "Doe");

    free(lastname);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_set);
    amxc_var_clean(&set_var);
}

// Set a LocalAgent. data model parameter (used to be a Device. prefix test)
void test_can_set_localagent(UNUSED void** state) {
    uspl_tx_t* usp_set = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t set_var;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    const char* param_name = "MaxSubscriptionChangeAdoptionTime";
    uint32_t param_value = 50;
    amxd_object_t* localagent = NULL;
    uint32_t param_result = 0;

    amxc_var_init(&set_var);
    amxc_var_set_type(&set_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &set_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &set_var, "requests", NULL);
    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", "LocalAgent.");
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);
    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", param_name);
    amxc_var_add_key(uint32_t, param, "value", param_value);
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_set, "one", "two"), 0);
    assert_int_equal(uspl_set_new(usp_set, &set_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_set->pbuf, usp_set->pbuf_len);

    assert_int_equal(uspa_handle_set(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    localagent = amxd_dm_findf(&dm, "LocalAgent.");
    assert_non_null(localagent);
    param_result = amxd_object_get_value(uint32_t, localagent, param_name, NULL);
    assert_int_equal(param_result, param_value);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_set);
    amxc_var_clean(&set_var);
}

// Test to ensure you cannot change read-only parameters under Device.LocalAgent.
void test_cannot_set_readonly_param_la(UNUSED void** state) {
    uspl_tx_t* usp_set = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t set_var;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    const char* param_name = "SupportedProtocols";
    const char* param_value = "STOMP";
    amxd_object_t* localagent = NULL;
    char* param_result = 0;

    amxc_var_init(&set_var);
    amxc_var_set_type(&set_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &set_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &set_var, "requests", NULL);
    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", "Device.LocalAgent.");
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);
    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", param_name);
    amxc_var_add_key(cstring_t, param, "value", param_value);
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_set, "one", "two"), 0);
    assert_int_equal(uspl_set_new(usp_set, &set_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_set->pbuf, usp_set->pbuf_len);

    assert_int_equal(uspa_handle_set(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    localagent = amxd_dm_findf(&dm, "LocalAgent.");
    assert_non_null(localagent);
    param_result = amxd_object_get_value(cstring_t, localagent, param_name, NULL);
    assert_string_not_equal(param_result, param_value);

    free(param_result);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_set);
    amxc_var_clean(&set_var);
}

// Test to ensure you cannot change read-only parameters under Device.Phonebook.
void test_cannot_set_readonly_param_phonebook(UNUSED void** state) {
    uspl_tx_t* usp_set = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t set_var;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    const char* target_path = "Device.Phonebook.Contact.1.";
    const char* param_name = "Alias";
    const char* param_value = "new-contact";
    amxd_object_t* phonebook_inst = NULL;
    char* param_result = 0;

    amxc_var_init(&set_var);
    amxc_var_set_type(&set_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &set_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &set_var, "requests", NULL);
    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", target_path);
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);
    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", param_name);
    amxc_var_add_key(cstring_t, param, "value", param_value);
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_set, "one", "two"), 0);
    assert_int_equal(uspl_set_new(usp_set, &set_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_set->pbuf, usp_set->pbuf_len);

    assert_int_equal(uspa_handle_set(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    phonebook_inst = amxd_dm_findf(&dm, target_path);
    assert_non_null(phonebook_inst);
    param_result = amxd_object_get_value(cstring_t, phonebook_inst, param_name, NULL);
    assert_string_equal(param_result, "test");

    free(param_result);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_set);
    amxc_var_clean(&set_var);
}

// Make sure set requests with search paths and ACL verification work
void test_can_invoke_set_with_search_path(UNUSED void** state) {
    uspl_tx_t* usp_set = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t set_var;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    const char* target_path = "Device.Phonebook.Contact.[FirstName=='Foo'].";
    const char* param_name = "FirstName";
    const char* param_value = "John";
    amxd_object_t* phonebook_inst = NULL;
    char* firstname = NULL;

    amxc_var_init(&set_var);
    amxc_var_set_type(&set_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &set_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &set_var, "requests", NULL);
    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", target_path);
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);
    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", param_name);
    amxc_var_add_key(cstring_t, param, "value", param_value);
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_set, "one", "two"), 0);
    assert_int_equal(uspl_set_new(usp_set, &set_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_set->pbuf, usp_set->pbuf_len);

    assert_int_equal(uspa_handle_set(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    phonebook_inst = amxd_dm_findf(&dm, "Device.Phonebook.Contact.1.");
    assert_non_null(phonebook_inst);
    firstname = amxd_object_get_value(cstring_t, phonebook_inst, param_name, NULL);
    assert_string_equal(firstname, "John");

    free(firstname);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_set);
    amxc_var_clean(&set_var);
}

// Make sure set requests with search paths fail if access is denied
void test_cannot_invoke_set_with_search_path(UNUSED void** state) {
    uspl_tx_t* usp_set = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t set_var;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    const char* target_path = "Device.Phonebook.Contact.[FirstName=='John'].";
    const char* param_name = "LastName";
    const char* param_value = "Bar";
    amxd_object_t* phonebook_inst = NULL;
    char* lastname = NULL;

    amxc_var_init(&set_var);
    amxc_var_set_type(&set_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &set_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &set_var, "requests", NULL);
    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", target_path);
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);
    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", param_name);
    amxc_var_add_key(cstring_t, param, "value", param_value);
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_set, "one", "two"), 0);
    assert_int_equal(uspl_set_new(usp_set, &set_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_set->pbuf, usp_set->pbuf_len);

    assert_int_equal(uspa_handle_set(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    phonebook_inst = amxd_dm_findf(&dm, "Device.Phonebook.Contact.1.");
    assert_non_null(phonebook_inst);
    lastname = amxd_object_get_value(cstring_t, phonebook_inst, param_name, NULL);
    assert_string_equal(lastname, "Doe");

    free(lastname);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_set);
    amxc_var_clean(&set_var);
}

void test_can_invoke_set_with_plus(UNUSED void** state) {
    uspl_tx_t* usp_set = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t set_var;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    const char* target_path = "Device.Phonebook.Contact.1.";
    const char* param_name = "FirstName";
    const char* param_value = "+123";
    amxd_object_t* phonebook_inst = NULL;
    char* firstname = NULL;

    amxc_var_init(&set_var);
    amxc_var_set_type(&set_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &set_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &set_var, "requests", NULL);
    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", target_path);
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);
    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", param_name);
    amxc_var_add_key(cstring_t, param, "value", param_value);
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_set, "one", "two"), 0);
    assert_int_equal(uspl_set_new(usp_set, &set_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_set->pbuf, usp_set->pbuf_len);

    assert_int_equal(uspa_handle_set(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    phonebook_inst = amxd_dm_findf(&dm, "Device.Phonebook.Contact.1.");
    assert_non_null(phonebook_inst);
    firstname = amxd_object_get_value(cstring_t, phonebook_inst, param_name, NULL);
    assert_string_equal(firstname, param_value);

    free(firstname);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_set);
    amxc_var_clean(&set_var);
}
