/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <poll.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include <usp/uspl.h>
#include <imtp/imtp_message.h>

#include "uspa.h"
#include "uspa_imtp.h"
#include "uspa_msghandler.h"
#include "test_agent_subscription.h"
#include "dummy_be.h"
#include "mock.h"
#include "common.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;

static amxo_parser_t parser;
static const char* odl_config = "../common/test_config.odl";
static const char* la_definition = "../common/tr181-localagent_definition.odl";
static const char* la_defaults = "../common/tr181-localagent_mtp_enabled.odl";
static const char* mqtt_definition = "../common/tr181-mqtt_definition.odl";
static const char* mqtt_defaults = "../common/tr181-mqtt_defaults.odl";
static const char* phonebook_definition = "../common/phonebook_definition.odl";
static const char* phonebook_defaults = "../common/phonebook_defaults.odl";
static const char* device_odl = "device.odl";
static const char* boot_params_odl = "boot-params.odl";
static const char* saved_subs_odl = "saved-subscriptions.odl";

static const char* agent_eid = "proto::agent-test";
static const char* controller_eid = "controller";
static const char* acl_dir = NULL;
static const char* role = "root";
static amxc_string_t acl_file;

static int testpipe[2] = {};

static void uspa_initialize(void) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    uspi_con_t* con = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, device_odl, root_obj), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[0]); // uspa_imtp_connect
    amxp_sigmngr_add_signal(&dm.sigmngr, "OnBoardRequest!");
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);

    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "MQTT.Client.1.", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    assert_true(GETP_BOOL(&ret, "0.0.Enable"));

    con = uspa_imtp_con_from_la_mtp("LocalAgent.MTP.1.");
    assert_non_null(con);

    amxc_var_clean(&ret);
}

static void acl_initialize(void) {
    amxc_var_t* root_acls = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);

    // Mimic the acl-manager functionality
    acl_dir = GET_CHAR(&parser.config, "acl_dir");
    amxc_string_setf(&path, "%s/merged", acl_dir);
    mkdir(amxc_string_get(&path, 0), 0700);

    amxc_string_setf(&path, "%s/%s/", acl_dir, role);
    root_acls = amxa_parse_files(amxc_string_get(&path, 0));
    assert_non_null(root_acls);

    amxc_string_setf(&path, "%s/merged/%s.json", acl_dir, role);
    assert_int_equal(amxa_merge_rules(root_acls, amxc_string_get(&path, 0)), 0);

    // Init acl file to use in each of the real test functions
    amxc_string_init(&acl_file, 0);
    amxc_string_setf(&acl_file, "%s/merged/%s.json", acl_dir, role);

    amxc_string_clean(&path);
    amxc_var_delete(&root_acls);
}

static void acl_clean(void) {
    amxc_string_t path;

    amxc_string_init(&path, 0);

    remove(amxc_string_get(&acl_file, 0));

    amxc_string_setf(&path, "%s/merged", acl_dir);
    rmdir(amxc_string_get(&path, 0));

    amxc_string_clean(&acl_file);
    amxc_string_clean(&path);
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "GetMTPInfo", AMXO_FUNC(_Controller_GetMTPInfo));
    amxo_resolver_ftab_add(&parser, "DeferredCall", AMXO_FUNC(_Phonebook_DeferredCall));
    amxo_resolver_ftab_add(&parser, "CreateListenSocket", AMXO_FUNC(_Client_CreateListenSocket));

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxb_be_load("/usr/bin/mods/amxb/mod-amxb-usp.so"), 0);

    handle_events();

    assert_int_equal(pipe(testpipe), 0);
    capture_sigalrm();

    uspa_initialize();
    acl_initialize();
    handle_events();

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    amxp_signal_t* sig = NULL;

    will_return(__wrap_uspi_con_get_fd, testpipe[0]); // uspa_imtp_disconnect
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);

    close(testpipe[0]);
    close(testpipe[1]);
    acl_clean();

    sig = amxp_sigmngr_find_signal(&dm.sigmngr, "OnBoardRequest!");
    amxp_sigmngr_remove_signal(&dm.sigmngr, "OnBoardRequest!");
    amxp_signal_delete(&sig);

    assert_int_equal(amxb_be_remove("usp"), 0);
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_can_subscribe_to_boot_event(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(&dm, "Device.");
    const char* notif_type = "Event";
    const char* ref_list = "Device.Boot!";
    const char* recipient = "LocalAgent.Controller.1.";
    int8_t* buffer = calloc(1, 1024);
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    amxc_var_t notification;

    amxc_var_init(&notification);

    assert_non_null(object);
    add_subscription_instance(&dm, notif_type, ref_list, recipient);
    handle_events();

    amxd_object_send_signal(object, "Boot!", NULL, false);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "event.Cause", "LocalReboot"));
    assert_true(test_verify_data(&notification, "event.CommandKey", ""));
    assert_true(test_verify_data(&notification, "event.event_name", "Boot!"));
    assert_true(test_verify_data(&notification, "event.path", "Device."));

    // Handle timer to reset Cause and CommandKey
    read_sigalrm(5);
    amxp_timers_calculate();
    amxp_timers_check();

    uspl_rx_delete(&usp_notify);
    imtp_tlv_delete(&tlv_recv);
    amxc_var_clean(&notification);
}

void test_boot_event_with_boot_params(UNUSED void** state) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    amxd_object_t* object = amxd_dm_findf(&dm, "Device.");
    int8_t* buffer = calloc(1, 1024);
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    amxc_var_t notification;
    amxc_var_t ret;

    amxc_var_init(&notification);
    amxc_var_init(&ret);

    assert_int_equal(amxo_parser_parse_file(&parser, boot_params_odl, root_obj), 0);

    assert_non_null(object);
    amxd_object_send_signal(object, "Boot!", NULL, false);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "event.Cause", "RemoteReboot"));
    assert_true(test_verify_data(&notification, "event.CommandKey", "cmd-key"));
    assert_true(test_verify_data(&notification, "event.event_name", "Boot!"));
    assert_true(test_verify_data(&notification, "event.path", "Device."));
    assert_true(test_verify_data(&notification, "event.ParameterMap",
                                 "{\"Device.Phonebook.Contact.2.LastName\":\"Bond\",\"Device.Phonebook.Contact.1.LastName\":\"Doe\",\"Device.Phonebook.Contact.1.FirstName\":\"John\"}"));

    // Handle timer to reset Cause and CommandKey
    read_sigalrm(5);
    amxp_timers_calculate();
    amxp_timers_check();

    // Check that Cause and CommandKey are reset
    amxb_get(bus_ctx, "LocalAgent.", 0, &ret, 5);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_true(test_verify_data(&ret, "0.'LocalAgent.'.Cause", "LocalReboot"));
    assert_true(test_verify_data(&ret, "0.'LocalAgent.'.CommandKey", ""));

    uspl_rx_delete(&usp_notify);
    imtp_tlv_delete(&tlv_recv);
    amxc_var_clean(&ret);
    amxc_var_clean(&notification);
}

void test_recipient_can_contain_device_prefix(UNUSED void** state) {
    const char* notif_type = "ValueChange";
    const char* ref_list = "MQTT.Client.1.";
    const char* recipient = "Device.LocalAgent.Controller.1.";
    int8_t* buffer = calloc(1, 1024);
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    amxc_var_t notification;
    amxd_trans_t trans;

    amxc_var_init(&notification);
    amxd_trans_init(&trans);

    add_subscription_instance(&dm, notif_type, ref_list, recipient);
    handle_events();

    amxd_trans_select_pathf(&trans, "MQTT.Client.1.");
    amxd_trans_set_value(cstring_t, &trans, "Name", "value-changed");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_string_equal(uspl_msghandler_from_id(usp_notify), "proto::agent-test");
    assert_string_equal(uspl_msghandler_to_id(usp_notify), "controller");

    assert_true(test_verify_data(&notification, "value_change.param_path", "MQTT.Client.1.Name"));
    assert_true(test_verify_data(&notification, "value_change.param_value", "value-changed"));

    uspl_rx_delete(&usp_notify);
    imtp_tlv_delete(&tlv_recv);
    amxd_trans_clean(&trans);
    amxc_var_clean(&notification);
}

void test_can_send_onboard_request(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(&dm, "LocalAgent.Controller.1.");
    amxc_var_t event;
    amxc_var_t notification;
    int8_t* buffer = calloc(1, 1024);
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    const char* aspv = "1.0";
    const char* oui = "AABBCC";
    const char* product_class = "123321";
    const char* serial_number = "123456789";
    const char* agent_eid = "proto::agent-test";
    const char* contr_eid = "controller";

    amxc_var_init(&event);
    amxc_var_init(&notification);

    amxc_var_set_type(&event, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &event, "oui", oui);
    amxc_var_add_key(cstring_t, &event, "product_class", product_class);
    amxc_var_add_key(cstring_t, &event, "serial_number", serial_number);
    amxc_var_add_key(cstring_t, &event, "agent_supported_protocol_version", aspv);
    amxc_var_add_key(cstring_t, &event, "agent_eid", agent_eid);
    amxc_var_add_key(cstring_t, &event, "contr_eid", contr_eid);

    amxd_object_send_signal(object, "OnBoardRequest!", &event, false);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "on_board_req.agent_supported_protocol_version", aspv));
    assert_true(test_verify_data(&notification, "on_board_req.oui", oui));
    assert_true(test_verify_data(&notification, "on_board_req.product_class", product_class));
    assert_true(test_verify_data(&notification, "on_board_req.serial_number", serial_number));
    assert_true(test_verify_data(&notification, "send_resp", "false"));
    assert_true(test_verify_data(&notification, "subscription_id", ""));

    uspl_rx_delete(&usp_notify);
    imtp_tlv_delete(&tlv_recv);
    amxc_var_clean(&notification);
    amxc_var_clean(&event);
}

void test_send_vc_notif_after_boot(UNUSED void** state) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    int8_t* buffer = calloc(1, 1024);
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    amxc_var_t notification;
    amxd_trans_t trans;
    const char* path = "LocalAgent.Subscription.boot-value-change.LastValue";
    const char* last_value = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_var_init(&notification);
    amxd_trans_init(&trans);

    // Load save file with Subscription instance that contains a LastValue
    assert_int_equal(amxo_parser_parse_file(&parser, saved_subs_odl, root_obj), 0);

    assert_int_equal(amxb_get(bus_ctx, path, 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    last_value = GETP_CHAR(&ret, "0.0.LastValue");
    assert_string_equal(last_value, "Tim");

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_string_equal(uspl_msghandler_from_id(usp_notify), "proto::agent-test");
    assert_string_equal(uspl_msghandler_to_id(usp_notify), "controller");

    assert_true(test_verify_data(&notification, "value_change.param_path", "Device.Phonebook.Contact.1.FirstName"));
    assert_true(test_verify_data(&notification, "value_change.param_value", "John"));

    // Check that LastValue has been updated
    assert_int_equal(amxb_get(bus_ctx, path, 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    last_value = GETP_CHAR(&ret, "0.0.LastValue");
    assert_string_equal(last_value, "John");

    amxc_var_clean(&ret);
    uspl_rx_delete(&usp_notify);
    imtp_tlv_delete(&tlv_recv);
    amxd_trans_clean(&trans);
    amxc_var_clean(&notification);
}

void test_can_remove_subscription(UNUSED void** state) {
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxb_get(bus_ctx, "LocalAgent.Subscription.", UINT32_MAX, &ret, 5);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_non_null(GETP_ARG(&ret, "0.'LocalAgent.Subscription.1.'"));

    amxb_del(bus_ctx, "LocalAgent.Subscription.*.", 0, NULL, &ret, 5);
    amxc_var_dump(&ret, STDOUT_FILENO);

    handle_events();

    amxb_get(bus_ctx, "LocalAgent.Subscription.", UINT32_MAX, &ret, 5);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_null(GETP_ARG(&ret, "0.'LocalAgent.Subscription.1.'"));

    amxc_var_clean(&ret);
}

void test_removed_subscription_does_not_trigger_cb(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    amxc_var_t oper_complete;
    amxc_var_t ret;
    const char* notif_type = "OperationComplete";
    const char* ref_list = "Device.Phonebook.Contact.1.";
    const char* recipient = "LocalAgent.Controller.1.";
    const char* cmd_key = "dummy";
    struct pollfd fds[1] = {};

    amxc_llist_init(&resp_list);
    amxc_var_init(&oper_complete);
    amxc_var_init(&operate_var);
    amxc_var_init(&ret);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &operate_var, "command", "Device.Phonebook.Contact.1.DeferredCall()");
    amxc_var_add_key(cstring_t, &operate_var, "command_key", cmd_key);
    amxc_var_add_key(bool, &operate_var, "send_resp", true);

    add_subscription_instance(&dm, notif_type, ref_list, recipient);
    handle_events();

    assert_int_equal(uspl_tx_new(&usp_operate_msg, "one", "two"), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(&acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.DeferredCall()"));
    assert_true(test_verify_data(result, "req_obj_path", "LocalAgent.Request.1."));

    assert_int_equal(amxb_del(bus_ctx, "LocalAgent.Subscription.*.", 0, NULL, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    handle_events();
    _Phonebook_DeferredCall_Done();

    // There will be nothing to read
    handle_events();
    fds[0].fd = testpipe[0];
    fds[0].events = POLLIN;

    assert_int_equal(poll(fds, 1, 3), 0);

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&oper_complete);
    amxc_var_clean(&operate_var);
    amxc_var_clean(&ret);
}

void test_can_create_periodic_subscription(UNUSED void** state) {
    const char* notif_type = "Event";
    const char* ref_list = "LocalAgent.Periodic!";
    const char* recipient = "LocalAgent.Controller.1.";
    amxc_var_t notification;
    amxc_var_t ret;
    int8_t* buffer = calloc(1, 1024);
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;

    amxc_var_init(&notification);
    amxc_var_init(&ret);

    add_subscription_instance(&dm, notif_type, ref_list, recipient);
    handle_events();

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    read_sigalrm(5);
    amxp_timers_calculate();
    amxp_timers_check();

    read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "event.event_name", "Periodic!"));
    assert_true(test_verify_data(&notification, "event.path", "Device.LocalAgent."));
    assert_true(test_verify_data(&notification, "send_resp", "false"));

    assert_int_equal(amxb_del(bus_ctx, "LocalAgent.Subscription.*.", 0, NULL, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    handle_events();

    uspl_rx_delete(&usp_notify);
    imtp_tlv_delete(&tlv_recv);
    amxc_var_clean(&ret);
    amxc_var_clean(&notification);
}

void test_can_subscribe_to_multiple_params(UNUSED void** state) {
    const char* notif_type = "ValueChange";
    const char* ref_list = "MQTT.Client.1.Name,LocalAgent.Controller.1.ProvisioningCode";
    const char* recipient = "LocalAgent.Controller.1.";
    amxc_var_t notification;
    amxc_var_t values;
    amxc_var_t ret;
    int8_t* buffer = calloc(1, 1024);
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;

    amxc_var_init(&notification);
    amxc_var_init(&values);
    amxc_var_init(&ret);

    add_subscription_instance(&dm, notif_type, ref_list, recipient);
    handle_events();

    // First element in ReferenceList
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Name", "Matthias");
    assert_int_equal(amxb_set(bus_ctx, "MQTT.Client.1.", &values, &ret, 5), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "MQTT.Client.1.Name"));
    assert_true(test_verify_data(&notification, "value_change.param_value", "Matthias"));

    uspl_rx_delete(&usp_notify);
    imtp_tlv_delete(&tlv_recv);
    buffer = calloc(1, 1024);

    // Second element in ReferenceList
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "ProvisioningCode", "abc");
    assert_int_equal(amxb_set(bus_ctx, "LocalAgent.Controller.1.", &values, &ret, 5), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "LocalAgent.Controller.1.ProvisioningCode"));
    assert_true(test_verify_data(&notification, "value_change.param_value", "abc"));

    assert_int_equal(amxb_del(bus_ctx, "LocalAgent.Subscription.*.", 0, NULL, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    handle_events();

    uspl_rx_delete(&usp_notify);
    imtp_tlv_delete(&tlv_recv);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
    amxc_var_clean(&notification);
}

void test_can_subscribe_to_multiple_params_same_object(UNUSED void** state) {
    const char* notif_type = "ValueChange";
    const char* ref_list = "MQTT.Client.1.Name,MQTT.Client.1.Username";
    const char* recipient = "LocalAgent.Controller.1.";
    amxc_var_t notification;
    amxc_var_t values;
    amxc_var_t ret;
    int8_t* buffer = calloc(1, 1024);
    int8_t* buffer2 = NULL;
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    int readlen = 0;
    int tlv1_len = 0;

    amxc_var_init(&notification);
    amxc_var_init(&values);
    amxc_var_init(&ret);

    add_subscription_instance(&dm, notif_type, ref_list, recipient);
    handle_events();

    // First element in ReferenceList
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Name", "Frederic");
    amxc_var_add_key(cstring_t, &values, "Username", "Frederic");
    assert_int_equal(amxb_set(bus_ctx, "MQTT.Client.1.", &values, &ret, 5), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    readlen = read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);
    tlv1_len = tlv_recv->length;

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "MQTT.Client.1.Name"));
    assert_true(test_verify_data(&notification, "value_change.param_value", "Frederic"));

    // The second message will start right after the first with an offset of 8 bytes
    buffer2 = calloc(1, readlen - tlv1_len - 8);
    memcpy(buffer2, buffer + tlv1_len + 8, readlen - tlv1_len - 8);

    // Clean up first received IMTP message after copying data to new buffer to avoid losing it
    imtp_tlv_delete(&tlv_recv);
    uspl_rx_delete(&usp_notify);

    assert_int_equal(imtp_message_parse(&tlv_recv, buffer2), 0);
    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "MQTT.Client.1.Username"));
    assert_true(test_verify_data(&notification, "value_change.param_value", "Frederic"));

    assert_int_equal(amxb_del(bus_ctx, "LocalAgent.Subscription.*.", 0, NULL, &ret, 5), 0);
    amxc_var_dump(&ret, 1);
    handle_events();

    uspl_rx_delete(&usp_notify);
    imtp_tlv_delete(&tlv_recv);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
    amxc_var_clean(&notification);
}

void test_notification_will_expire(UNUSED void** state) {
    const char* notif_type = "ValueChange";
    const char* ref_list = "MQTT.Client.1.Name";
    const char* recipient = "LocalAgent.Controller.1.";
    const char* id = "notif_expire";
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxc_var_t notification;
    amxc_var_t values;
    amxc_var_t ret;
    int8_t* buffer = calloc(1, 1024);
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    amxd_trans_t trans;

    amxc_var_init(&notification);
    amxc_var_init(&values);
    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, id);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", id);
    amxd_trans_set_value(cstring_t, &trans, "NotifType", notif_type);
    amxd_trans_set_value(cstring_t, &trans, "Recipient", recipient);
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", ref_list);
    amxd_trans_set_value(bool, &trans, "NotifRetry", true);
    amxd_trans_set_value(uint32_t, &trans, "NotifExpiration", 12);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();
    assert_non_null(amxd_object_findf(sub_obj, "[ID == '%s'].", id));

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Name", "Dummy");
    assert_int_equal(amxb_set(bus_ctx, "MQTT.Client.1.", &values, &ret, 5), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "MQTT.Client.1.Name"));
    assert_true(test_verify_data(&notification, "value_change.param_value", "Dummy"));
    assert_true(test_verify_data(&notification, "send_resp", "true"));

    // Cleanup before reading notif retry
    imtp_tlv_delete(&tlv_recv);
    uspl_rx_delete(&usp_notify);
    buffer = calloc(1, 1024);

    // Using the default retry values, the first retry will take between 5 and 10 seconds
    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    read_sigalrm(10);
    amxp_timers_calculate();
    amxp_timers_check();
    handle_events();

    read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "MQTT.Client.1.Name"));
    assert_true(test_verify_data(&notification, "value_change.param_value", "Dummy"));
    assert_true(test_verify_data(&notification, "send_resp", "true"));

    // The second retry will be 10 to 20 seconds after the first, but it will have expired at this point
    // No need to read from testpipe again
    read_sigalrm(10);
    amxp_timers_calculate();
    amxp_timers_check();
    handle_events();

    assert_int_equal(amxb_del(bus_ctx, "LocalAgent.Subscription.*.", 0, NULL, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    handle_events();

    uspl_rx_delete(&usp_notify);
    imtp_tlv_delete(&tlv_recv);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
    amxc_var_clean(&notification);
}

void test_can_subscribe_to_single_param(UNUSED void** state) {
    const char* notif_type = "ValueChange";
    const char* ref_list = "MQTT.Client.1.Name";
    const char* recipient = "LocalAgent.Controller.1.";
    amxc_var_t notification;
    amxc_var_t values;
    amxc_var_t ret;
    int8_t* buffer = calloc(1, 1024);
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    ssize_t read_len = 0;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;

    amxc_var_init(&notification);
    amxc_var_init(&values);
    amxc_var_init(&ret);

    add_subscription_instance(&dm, notif_type, ref_list, recipient);
    handle_events();

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Name", "Kowalski");
    amxc_var_add_key(cstring_t, &values, "Username", "Skipper");
    assert_int_equal(amxb_set(bus_ctx, "MQTT.Client.1.", &values, &ret, 5), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    read_len = read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);

    // Verify only 1 tlv was received even though 2 parameters changed
    assert_int_equal(read_len, tlv_recv->length + 8);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "MQTT.Client.1.Name"));
    assert_true(test_verify_data(&notification, "value_change.param_value", "Kowalski"));

    assert_int_equal(amxb_del(bus_ctx, "LocalAgent.Subscription.*.", 0, NULL, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    handle_events();

    uspl_rx_delete(&usp_notify);
    imtp_tlv_delete(&tlv_recv);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
    amxc_var_clean(&notification);
}

void test_multiple_changes_give_multiple_notifications(UNUSED void** state) {
    const char* notif_type = "ValueChange";
    const char* ref_list = "MQTT.Client.1.";
    const char* recipient = "LocalAgent.Controller.1.";
    amxc_var_t notification;
    amxc_var_t values;
    amxc_var_t ret;
    int8_t* buffer = calloc(1, 1024);
    int8_t* buffer2 = NULL;
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    int readlen = 0;
    int tlv1_len = 0;

    amxc_var_init(&notification);
    amxc_var_init(&values);
    amxc_var_init(&ret);

    add_subscription_instance(&dm, notif_type, ref_list, recipient);
    handle_events();

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Name", "Private");
    amxc_var_add_key(cstring_t, &values, "Username", "Rico");
    assert_int_equal(amxb_set(bus_ctx, "MQTT.Client.1.", &values, &ret, 5), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    readlen = read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);
    tlv1_len = tlv_recv->length;

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "MQTT.Client.1.Username"));
    assert_true(test_verify_data(&notification, "value_change.param_value", "Rico"));

    // The second message will start right after the first with an offset of 8 bytes
    buffer2 = calloc(1, readlen - tlv1_len - 8);
    memcpy(buffer2, buffer + tlv1_len + 8, readlen - tlv1_len - 8);

    // Clean up first received IMTP message after copying data to new buffer to avoid losing it
    imtp_tlv_delete(&tlv_recv);
    uspl_rx_delete(&usp_notify);

    assert_int_equal(imtp_message_parse(&tlv_recv, buffer2), 0);
    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "MQTT.Client.1.Name"));
    assert_true(test_verify_data(&notification, "value_change.param_value", "Private"));

    assert_int_equal(amxb_del(bus_ctx, "LocalAgent.Subscription.*.", 0, NULL, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    handle_events();

    uspl_rx_delete(&usp_notify);
    imtp_tlv_delete(&tlv_recv);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
    amxc_var_clean(&notification);
}

void test_object_creation_subscription(UNUSED void** state) {
    const char* notif_type = "ObjectCreation";
    const char* ref_list = "MQTT.Client.";
    const char* recipient = "LocalAgent.Controller.1.";
    const char* alias = "test-notification";
    const char* path = NULL;
    amxc_var_t notification;
    amxc_var_t values;
    amxc_var_t ret;
    int8_t* buffer = calloc(1, 1024);
    imtp_tlv_t* tlv_recv = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    ssize_t read_len = 0;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;

    amxc_var_init(&notification);
    amxc_var_init(&values);
    amxc_var_init(&ret);

    add_subscription_instance(&dm, notif_type, ref_list, recipient);
    handle_events();

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxb_add(bus_ctx, ref_list, 0, alias, &values, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    path = GETP_CHAR(&ret, "0.path");

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    read_len = read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_message_parse(&tlv_recv, buffer), 0);

    tlv_proto = imtp_message_get_first_tlv(tlv_recv, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);

    assert_int_equal(read_len, tlv_recv->length + 8);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "obj_creation.keys.Alias", alias));
    assert_true(test_verify_data(&notification, "obj_creation.path", path));

    assert_int_equal(amxb_del(bus_ctx, "LocalAgent.Subscription.*.", 0, NULL, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    handle_events();

    uspl_rx_delete(&usp_notify);
    imtp_tlv_delete(&tlv_recv);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
    amxc_var_clean(&notification);
}

void test_subscription_object_creation_allowed(UNUSED void** state) {
    uspl_tx_t* usp_add_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    const char* requested_path = "LocalAgent.Subscription.";
    const char* reference_list = "Device.Phonebook.Contact.1.PhoneNumber.";
    const char* notif_type = "ObjectCreation";
    amxc_var_t ret;
    amxc_var_t add_var;

    amxc_var_init(&ret);
    amxc_var_init(&add_var);
    amxc_var_set_type(&add_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &add_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &add_var, "requests", NULL);

    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", requested_path);
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "ReferenceList");
    amxc_var_add_key(cstring_t, param, "value", reference_list);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "NotifType");
    amxc_var_add_key(cstring_t, param, "value", notif_type);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "ID");
    amxc_var_add_key(cstring_t, param, "value", notif_type);
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_add_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_add_new(usp_add_msg, &add_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_add_msg->pbuf, usp_add_msg->pbuf_len);

    assert_int_equal(uspa_handle_add(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.", 0, &ret, 1), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_non_null(GETP_ARG(&ret, "0.1")); // Parent object and instance in result

    assert_int_equal(amxb_del(bus_ctx, "LocalAgent.Subscription.*.", 0, NULL, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    handle_events();

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_add_msg);
    amxc_var_clean(&ret);
    amxc_var_clean(&add_var);
}

void test_subscription_object_deletion_allowed(UNUSED void** state) {
    uspl_tx_t* usp_add_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    const char* requested_path = "LocalAgent.Subscription.";
    const char* reference_list = "Device.Phonebook.Contact.1.PhoneNumber.*.";
    const char* notif_type = "ObjectDeletion";
    amxc_var_t ret;
    amxc_var_t add_var;

    amxc_var_init(&ret);
    amxc_var_init(&add_var);
    amxc_var_set_type(&add_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &add_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &add_var, "requests", NULL);

    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", requested_path);
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "ReferenceList");
    amxc_var_add_key(cstring_t, param, "value", reference_list);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "NotifType");
    amxc_var_add_key(cstring_t, param, "value", notif_type);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "ID");
    amxc_var_add_key(cstring_t, param, "value", notif_type);
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_add_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_add_new(usp_add_msg, &add_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_add_msg->pbuf, usp_add_msg->pbuf_len);

    assert_int_equal(uspa_handle_add(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.", 0, &ret, 1), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_non_null(GETP_ARG(&ret, "0.1")); // Parent object and instance in result

    assert_int_equal(amxb_del(bus_ctx, "LocalAgent.Subscription.*.", 0, NULL, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    handle_events();

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_add_msg);
    amxc_var_clean(&ret);
    amxc_var_clean(&add_var);
}

void test_subscription_vc_not_allowed(UNUSED void** state) {
    uspl_tx_t* usp_add_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    const char* requested_path = "LocalAgent.Subscription.";
    const char* reference_list = "Device.Phonebook.Contact.1.PhoneNumber.";
    const char* notif_type = "ValueChange";
    amxc_var_t ret;
    amxc_var_t add_var;

    amxc_var_init(&ret);
    amxc_var_init(&add_var);
    amxc_var_set_type(&add_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &add_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &add_var, "requests", NULL);

    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", requested_path);
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "ReferenceList");
    amxc_var_add_key(cstring_t, param, "value", reference_list);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "NotifType");
    amxc_var_add_key(cstring_t, param, "value", notif_type);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "ID");
    amxc_var_add_key(cstring_t, param, "value", notif_type);
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_add_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_add_new(usp_add_msg, &add_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_add_msg->pbuf, usp_add_msg->pbuf_len);

    assert_int_equal(uspa_handle_add(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.", 0, &ret, 1), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_null(GETP_ARG(&ret, "0.1")); // Only parent object in result

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_add_msg);
    amxc_var_clean(&ret);
    amxc_var_clean(&add_var);
}

void test_subscription_event_not_allowed(UNUSED void** state) {
    uspl_tx_t* usp_add_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    const char* requested_path = "LocalAgent.Subscription.";
    const char* reference_list = "Device.Phonebook.Contact.1.PhoneNumber.";
    const char* notif_type = "Event";
    amxc_var_t ret;
    amxc_var_t add_var;

    amxc_var_init(&ret);
    amxc_var_init(&add_var);
    amxc_var_set_type(&add_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &add_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &add_var, "requests", NULL);

    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", requested_path);
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "ReferenceList");
    amxc_var_add_key(cstring_t, param, "value", reference_list);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "NotifType");
    amxc_var_add_key(cstring_t, param, "value", notif_type);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "ID");
    amxc_var_add_key(cstring_t, param, "value", notif_type);
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_add_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_add_new(usp_add_msg, &add_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_add_msg->pbuf, usp_add_msg->pbuf_len);

    assert_int_equal(uspa_handle_add(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.", 0, &ret, 1), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_null(GETP_ARG(&ret, "0.1")); // Only parent object in result

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_add_msg);
    amxc_var_clean(&ret);
    amxc_var_clean(&add_var);
}

void test_subscription_operation_complete_not_allowed(UNUSED void** state) {
    uspl_tx_t* usp_add_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    const char* requested_path = "LocalAgent.Subscription.";
    const char* reference_list = "Device.Phonebook.Contact.1.PhoneNumber.";
    const char* notif_type = "OperationComplete";
    amxc_var_t ret;
    amxc_var_t add_var;

    amxc_var_init(&ret);
    amxc_var_init(&add_var);
    amxc_var_set_type(&add_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &add_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &add_var, "requests", NULL);

    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", requested_path);
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "ReferenceList");
    amxc_var_add_key(cstring_t, param, "value", reference_list);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "NotifType");
    amxc_var_add_key(cstring_t, param, "value", notif_type);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "ID");
    amxc_var_add_key(cstring_t, param, "value", notif_type);
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_add_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_add_new(usp_add_msg, &add_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_add_msg->pbuf, usp_add_msg->pbuf_len);

    assert_int_equal(uspa_handle_add(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.", 0, &ret, 1), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_null(GETP_ARG(&ret, "0.1")); // Only parent object in result

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_add_msg);
    amxc_var_clean(&ret);
    amxc_var_clean(&add_var);
}

// Creating subscription will be allowed for the fixed part of the search path, but the notification
// needs to be filtered
void test_subscription_vc_filter_notif(UNUSED void** state) {
    uspl_tx_t* usp_add_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    const char* requested_path = "LocalAgent.Subscription.";
    const char* reference_list = "Device.Phonebook.Contact.*.PhoneNumber.";
    const char* notif_type = "ValueChange";
    amxc_var_t ret;
    amxc_var_t add_var;
    amxc_var_t values;

    amxc_var_init(&values);
    amxc_var_init(&ret);
    amxc_var_init(&add_var);
    amxc_var_set_type(&add_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &add_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &add_var, "requests", NULL);

    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", requested_path);
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "ReferenceList");
    amxc_var_add_key(cstring_t, param, "value", reference_list);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "NotifType");
    amxc_var_add_key(cstring_t, param, "value", notif_type);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "ID");
    amxc_var_add_key(cstring_t, param, "value", notif_type);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "Enable");
    amxc_var_add_key(bool, param, "value", true);
    amxc_var_add_key(bool, param, "required", true);

    assert_int_equal(uspl_tx_new(&usp_add_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_add_new(usp_add_msg, &add_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_add_msg->pbuf, usp_add_msg->pbuf_len);

    assert_int_equal(uspa_handle_add(usp_rx, &usp_tx, amxc_string_get(&acl_file, 0)), 0);
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.", 0, &ret, 1), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_non_null(GETP_ARG(&ret, "0.1")); // Subscription was created

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Phone", "789");
    assert_int_equal(amxb_set(bus_ctx, "Device.Phonebook.Contact.1.PhoneNumber.1.", &values, &ret, 5), 0);

    // No notification will be sent, so no need to provide return value for __wrap_uspi_con_get_fd
    handle_events();

    assert_int_equal(amxb_del(bus_ctx, "LocalAgent.Subscription.*.", 0, NULL, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    handle_events();

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_add_msg);
    amxc_var_clean(&ret);
    amxc_var_clean(&add_var);
    amxc_var_clean(&values);
}
