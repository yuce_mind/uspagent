/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include <usp/uspl.h>

#include "uspa.h"
#include "uspa_msghandler.h"
#include "test_agent_gsp.h"
#include "dummy_be.h"
#include "common.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxo_parser_t parser;

static const char* la_definition = "../common/tr181-localagent_definition.odl";
static const char* la_defaults = "tr181-localagent_defaults.odl";

static void uspa_initialize(void) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, la_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_defaults, root_obj), 0);
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    // Register data model
    amxb_register(bus_ctx, &dm);

    // Load USP back end
    assert_int_equal(amxb_be_load("/usr/bin/mods/amxb/mod-amxb-usp.so"), 0);

    handle_events();

    uspa_initialize();

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);
    assert_int_equal(amxb_be_remove("usp"), 0);
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_handle_supported_proto_msg(UNUSED void** state) {
    uspl_tx_t* incoming_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t request;
    amxc_var_t* response = NULL;
    const char* contr_versions = "1.0";
    const char* agent_versions = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_CSV_STRING);
    amxc_var_set(csv_string_t, &request, contr_versions);

    assert_int_equal(uspl_tx_new(&incoming_msg, "one", "two"), 0);
    assert_int_equal(uspl_get_supported_protocol_new(incoming_msg, &request), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(incoming_msg->pbuf, incoming_msg->pbuf_len);

    assert_int_equal(uspa_handle_get_supported_proto(usp_rx, &usp_tx, NULL), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_get_supported_protocol_resp_extract(usp_response, &resp_list), 0);
    response = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));

    agent_versions = amxc_var_constcast(cstring_t, response);
    assert_string_equal(agent_versions, AGENT_SUPPORTED_PROTOCOL_VERSIONS);

    amxc_llist_clean(&resp_list, variant_list_it_free);
    amxc_var_clean(&request);
    uspl_tx_delete(&incoming_msg);
    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    uspl_rx_delete(&usp_response);
}

void test_handle_supported_proto_msg_invalid(UNUSED void** state) {
    uspl_rx_t usp_rx;

    assert_int_equal(uspa_handle_get_supported_proto(NULL, NULL, NULL), -1);
    assert_int_equal(uspa_handle_get_supported_proto(&usp_rx, NULL, NULL), -1);
}
