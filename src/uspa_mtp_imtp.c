/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>

#include <imtp/imtp_connection.h>
#include <uspi/uspi_connection.h>
#include <uspi/uspi_discovery.h>

#include "uspa_object.h"
#include "uspa_mtp_imtp.h"
#include "uspa_imtp.h"
#include "uspa_discovery.h"
#include "uspa_agent.h"
#include "uspa_msghandler.h"
#include "uspa_controller.h"

// Table with key: LocalAgent.MTP.{i}.
//            value: struct of type uspa_mtp_instance_t
static amxc_htable_t uspa_mtp_imtp_instances;

static int uspa_mtp_imtp_instance_new(uspa_mtp_instance_t** inst, uspi_con_t* con) {
    int retval = -1;

    when_null(inst, exit);
    when_null(con, exit);

    *inst = (uspa_mtp_instance_t*) calloc(1, sizeof(uspa_mtp_instance_t));
    when_null(*inst, exit);

    (*inst)->con = con;

    retval = 0;
exit:
    return retval;
}

static void uspa_mtp_imtp_instance_delete(uspa_mtp_instance_t** inst) {
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    amxo_parser_t* parser = uspa_get_parser();
    uspi_con_t* con = NULL;

    when_null(inst, exit);
    when_null(*inst, exit);

    con = (*inst)->con;
    if(con->type == uspi_con_type_listen) {
        amxc_llist_for_each(it, &con->accepted_cons) {
            uspi_con_t* con_accepted = amxc_container_of(it, uspi_con_t, lit);
            amxo_connection_remove(parser, uspi_con_get_fd(con_accepted));
            uspi_discovery_del_service(ctx, con_accepted->eid);
        }
    }

    amxo_connection_remove(parser, uspi_con_get_fd(con));
    uspi_con_disconnect(&con);

    free(*inst);

exit:
    return;
}

static void uspa_mtp_imtp_instance_hit_clean(UNUSED const char* key, amxc_htable_it_t* hit) {
    uspa_mtp_instance_t* inst = amxc_htable_it_get_data(hit, uspa_mtp_instance_t, hit);
    uspa_mtp_imtp_instance_delete(&inst);
}

static int uspa_mtp_imtp_contr_con_add(uspi_con_t* con) {
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    int retval = -1;
    const char* contr_inst = NULL;
    const char* contr_mtp_inst = NULL;
    amxc_var_t ret_1;
    amxc_var_t ret_2;
    amxc_string_t path;

    amxc_var_init(&ret_1);
    amxc_var_init(&ret_2);
    amxc_string_init(&path, 0);

    amxc_string_setf(&path, "LocalAgent.Controller.[EndpointID == '%s' && Enable == true].",
                     con->eid);
    amxb_get(ctx, amxc_string_get(&path, 0), 0, &ret_1, 5);
    contr_inst = amxc_var_key(GETP_ARG(&ret_1, "0.0"));
    when_str_empty_trace(contr_inst, exit, WARNING, "Failed to find enabled controller with EID = %s", con->eid);

    amxc_string_setf(&path, "%sMTP.[Protocol == 'IMTP' && Enable == true].", contr_inst);
    amxb_get(ctx, amxc_string_get(&path, 0), 0, &ret_2, 5);
    contr_mtp_inst = amxc_var_key(GETP_ARG(&ret_2, "0.0"));
    when_str_empty_trace(contr_mtp_inst, exit, WARNING, "Failed to find enabled MTP of type IMTP for controller with EID = %s", con->eid);

    retval = uspa_controller_con_add(contr_inst, contr_mtp_inst, con);

exit:
    amxc_var_clean(&ret_2);
    amxc_var_clean(&ret_1);
    amxc_string_clean(&path);
    return retval;
}

static void uspa_mtp_imtp_handle_protobuf(uspi_con_t* con, const imtp_tlv_t* tlv_protobuf) {
    unsigned char* pbuf = (unsigned char*) tlv_protobuf->value + tlv_protobuf->offset;
    int pbuf_len = tlv_protobuf->length;
    amxc_var_t mtp_info;

    amxc_var_init(&mtp_info);
    amxc_var_set_type(&mtp_info, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &mtp_info, "protocol", "IMTP");

    uspa_msghandler_process_binary_record(pbuf, pbuf_len, con, &mtp_info);

    amxc_var_clean(&mtp_info);
}

static int uspa_mtp_imtp_handle_eid(uspi_con_t* con, const imtp_tlv_t* tlv_eid) {
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    int retval = -1;

    retval = uspi_con_add_eid(con, tlv_eid);
    if(retval == 0) {
        SAH_TRACEZ_INFO(ME, "Add eid '%s' to con with fd %d", con->eid, uspi_con_get_fd(con));
    } else {
        SAH_TRACEZ_WARNING(ME, "Failed to add eid to con with fd %d", uspi_con_get_fd(con));
        goto exit;
    }

    // Note that this will check for uniqueness, since the parameter is unique
    retval = uspi_discovery_add_service(ctx, con->eid);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Possible duplicate EndpointID: %s", con->eid);
        goto exit;
    }

    // Add connection to list of connections for the controller (if a controller is found)
    retval = uspa_mtp_imtp_contr_con_add(con);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to add con for LocalAgent.Controller. instance with EID: %s", con->eid);
        goto exit;
    }

exit:
    return retval;
}

static void uspa_mtp_imtp_handle_sub(uspi_con_t* con, const imtp_tlv_t* tlv_sub) {
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    char* pbuf = (char*) tlv_sub->value + tlv_sub->offset;
    char* recipient = uspa_controller_get_instance_from_eid(con->eid);
    amxc_var_t values;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_var_init(&values);

    SAH_TRACEZ_INFO(ME, "Handle TLV of type subscribe for path %s", pbuf);
    when_null(recipient, exit);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "ReferenceList", pbuf);
    amxc_var_add_key(cstring_t, &values, "Recipient", recipient);
    amxc_var_add_key(cstring_t, &values, "NotifType", "AmxNotification");
    amxc_var_add_key(bool, &values, "Enable", true);

    if(amxb_add(ctx, "LocalAgent.Subscription.", 0, NULL, &values, &ret, 5) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add subscription for path %s", pbuf);
    }

exit:
    free(recipient);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
}

static void uspa_mtp_imtp_handle_unsub(uspi_con_t* con, const imtp_tlv_t* tlv_unsub) {
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    char* pbuf = (char*) tlv_unsub->value + tlv_unsub->offset;
    char* recipient = uspa_controller_get_instance_from_eid(con->eid);
    amxc_var_t ret;
    amxc_string_t path;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    SAH_TRACEZ_INFO(ME, "Handle TLV of type unsubscribe for path %s", pbuf);
    when_null(recipient, exit);

    amxc_string_setf(&path, "LocalAgent.Subscription.[Recipient=='%s' && ReferenceList=='%s'].",
                     recipient, pbuf);

    if(amxb_del(ctx, amxc_string_get(&path, 0), 0, NULL, &ret, 5) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to del subscription for path %s", amxc_string_get(&path, 0));
    }

exit:
    free(recipient);
    amxc_string_clean(&path);
    amxc_var_clean(&ret);
}

static void uspa_mtp_imtp_handle_msg(uspi_con_t* con, imtp_tlv_t* tlv) {
    const imtp_tlv_t* tlv_eid = NULL;
    const imtp_tlv_t* tlv_pbuf = NULL;
    const imtp_tlv_t* tlv_sub = NULL;
    const imtp_tlv_t* tlv_unsub = NULL;
    int retval = 0;

    tlv_eid = imtp_message_get_first_tlv(tlv, imtp_tlv_type_eid);
    if(tlv_eid != NULL) {
        retval = uspa_mtp_imtp_handle_eid(con, tlv_eid);
        when_failed(retval, exit);
    }

    if(con->eid == NULL) {
        SAH_TRACEZ_WARNING(ME, "Received IMTP message from connection with unknown EID");
        goto exit;
    }

    tlv_pbuf = imtp_message_get_first_tlv(tlv, imtp_tlv_type_protobuf_bytes);
    if(tlv_pbuf != NULL) {
        uspa_mtp_imtp_handle_protobuf(con, tlv_pbuf);
    }

    tlv_sub = imtp_message_get_first_tlv(tlv, imtp_tlv_type_subscribe);
    if(tlv_sub != NULL) {
        uspa_mtp_imtp_handle_sub(con, tlv_sub);
    }

    tlv_unsub = imtp_message_get_first_tlv(tlv, imtp_tlv_type_unsubscribe);
    if(tlv_unsub != NULL) {
        uspa_mtp_imtp_handle_unsub(con, tlv_unsub);
    }

exit:
    return;
}

static void uspa_imtp_connection_remove(uspi_con_t* con) {
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    amxo_parser_t* parser = uspa_get_parser();

    when_null(con, exit);

    uspa_controller_con_unlink(con);
    uspi_discovery_del_service(ctx, con->eid);
    amxo_connection_remove(parser, uspi_con_get_fd(con));
    uspi_con_disconnect(&con);

exit:
    return;
}

static void uspa_mtp_imtp_con_read(UNUSED int fd, void* priv) {
    int retval = -1;
    uspi_con_t* con = (uspi_con_t*) priv;
    imtp_tlv_t* tlv = NULL;
    SAH_TRACEZ_INFO(ME, "Incoming message on fd: [%d]", fd);

    retval = uspi_con_read(con, &tlv);
    when_failed(retval, exit);

    uspa_mtp_imtp_handle_msg(con, tlv);

exit:
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Removing accepted connection and Service instance");
        uspa_imtp_connection_remove(con);
    }
    imtp_tlv_delete(&tlv);
    return;
}

static void uspa_mtp_imtp_accept(UNUSED int fd, void* priv) {
    SAH_TRACEZ_INFO(ME, "New incoming IMTP connection");
    uspi_con_t* con_listen = (uspi_con_t*) priv;
    uspi_con_t* con_accepted = NULL;
    int retval = -1;

    // Note that the agent EndpointID is automatically sent to the other end via the USP back-end when accepting the connection
    retval = uspi_con_accept(con_listen, &con_accepted);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to accept incoming connection");
        goto exit;
    }

    amxo_connection_add(uspa_get_parser(),
                        uspi_con_get_fd(con_accepted),
                        uspa_mtp_imtp_con_read,
                        NULL,
                        AMXO_BUS,
                        con_accepted);

exit:
    return;
}

static void uspa_mtp_imtp_subscription_filter_params(amxc_var_t* params) {
    amxc_var_t* ref_list = GET_ARG(params, "ReferenceList");
    amxc_var_t* alias = GET_ARG(params, "Alias");
    amxc_var_t* recipient = GET_ARG(params, "Recipient");
    amxc_var_t* creation_date = GET_ARG(params, "CreationDate");
    amxc_var_t* last_value = GET_ARG(params, "LastValue");
    amxc_var_delete(&ref_list);
    amxc_var_delete(&alias);
    amxc_var_delete(&recipient);
    amxc_var_delete(&creation_date);
    amxc_var_delete(&last_value);
}

uspi_con_t* uspa_mtp_imtp_get_con_from_eid(const char* eid) {
    uspi_con_t* con = NULL;

    when_str_empty(eid, exit);

    amxc_htable_for_each(hit, &uspa_mtp_imtp_instances) {
        uspa_mtp_instance_t* inst = amxc_htable_it_get_data(hit, uspa_mtp_instance_t, hit);
        uspi_con_t* con_listen = inst->con;
        amxc_llist_for_each(it, &con_listen->accepted_cons) {
            uspi_con_t* current = (uspi_con_t*) amxc_container_of(it, uspi_con_t, lit);
            if((current->eid != NULL)) {
                if(strcmp(eid, current->eid) == 0) {
                    con = current;
                    goto exit;
                }
            }
        }
    }

exit:
    return con;
}

int uspa_mtp_imtp_configure(const char* mtp) {
    amxo_parser_t* parser = uspa_get_parser();
    amxb_bus_ctx_t* bus_ctx = uspa_get_busctxt();
    uspi_con_t* con = NULL;
    uspa_mtp_instance_t* imtp_inst = NULL;
    amxc_htable_it_t* hit = NULL;
    int retval = -1;
    amxc_var_t ret;
    amxc_string_t path;
    const char* uri = NULL;
    const char* mode = NULL;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    hit = amxc_htable_get(&uspa_mtp_imtp_instances, mtp);
    when_not_null(hit, exit);

    amxc_string_setf(&path, "%sIMTP.", mtp);

    retval = amxb_get(bus_ctx, amxc_string_get(&path, 0), 0, &ret, 5);

    mode = GETP_CHAR(&ret, "0.0.Mode");
    when_str_empty(mode, exit);
    if(strcmp(mode, "Listen") != 0) {
        SAH_TRACEZ_ERROR(ME, "MTP IMTP is of mode '%s' and only 'Listen' is supported", mode);
        goto exit;
    }

    uri = GETP_CHAR(&ret, "0.0.URI");
    when_str_empty(uri, exit);

    SAH_TRACEZ_INFO(ME, "Listening for connections on uri: %s", uri);
    retval = uspi_con_listen(&con, uri);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to create listen socket at %s", uri);
        goto exit;
    }

    retval = uspa_mtp_imtp_instance_new(&imtp_inst, con);
    when_failed(retval, exit);

    amxc_htable_insert(&uspa_mtp_imtp_instances, mtp, &imtp_inst->hit);
    amxo_connection_add(parser, uspi_con_get_fd(con), uspa_mtp_imtp_accept,
                        uri, AMXO_CUSTOM, con);
    uspa_object_update_status(mtp, "Up");

exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&path);
    return retval;
}

void uspa_mtp_imtp_remove(const char* mtp) {
    amxc_htable_it_t* hit = amxc_htable_take(&uspa_mtp_imtp_instances, mtp);

    when_null(hit, exit);
    amxc_htable_it_clean(hit, uspa_mtp_imtp_instance_hit_clean);

exit:
    return;
}

void uspa_mtp_imtp_init(void) {
    amxc_htable_init(&uspa_mtp_imtp_instances, 2);
}

void uspa_mtp_imtp_clean(void) {
    amxc_htable_clean(&uspa_mtp_imtp_instances, uspa_mtp_imtp_instance_hit_clean);
}

int uspa_mtp_imtp_subscription_forward(amxc_var_t* forwarding_list, amxc_var_t* params) {
    amxb_bus_ctx_t* ctx_base = NULL;
    amxc_var_t ret;
    int retval = 0;
    const amxc_llist_t* fw_list = amxc_var_constcast(amxc_llist_t, forwarding_list);

    amxc_var_init(&ret);
    uspa_mtp_imtp_subscription_filter_params(params);

    while(!amxc_llist_is_empty(fw_list)) {
        bool first = true;
        amxc_var_t* ref_list = amxc_var_add_key(amxc_llist_t, params, "ReferenceList", NULL);
        amxc_var_for_each(ref, forwarding_list) {
            const char* path = amxc_var_constcast(cstring_t, ref);
            amxb_bus_ctx_t* ctx_current = uspa_discovery_ctx_from_imtp(path);
            if(first) {
                ctx_base = ctx_current;
                first = false;
            }
            if(ctx_current == ctx_base) {
                amxc_var_add(cstring_t, ref_list, path);
                amxc_var_delete(&ref);
            } else if(ctx_current == NULL) {
                SAH_TRACEZ_ERROR(ME, "Cannot create subscription for %s. Path not found", path);
                amxc_var_delete(&ref);
            }
        }

        retval = amxb_add(ctx_base, "LocalAgent.Subscription.", 0, NULL, params, &ret, 5);
        if(retval != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to forward subscription on IMTP");
        }
        amxc_var_delete(&ref_list);
    }

    amxc_var_clean(&ret);
    return retval;
}

int uspa_mtp_imtp_subscription_remove(amxc_var_t* forwarding_list, uspa_sub_t* sub) {
    amxc_string_t sub_path;
    amxc_var_t ret;
    int retval = -1;

    amxc_string_init(&sub_path, 0);
    amxc_var_init(&ret);

    amxc_string_setf(&sub_path, "LocalAgent.Subscription.[ID == '%s'].", sub->id);
    amxc_var_for_each(ref, forwarding_list) {
        amxb_bus_ctx_t* ctx = uspa_discovery_ctx_from_imtp(amxc_var_constcast(cstring_t, ref));
        amxb_del(ctx, amxc_string_get(&sub_path, 0), 0, NULL, &ret, 5);
    }

    amxc_string_clean(&sub_path);
    amxc_var_clean(&ret);
    return retval;
}
