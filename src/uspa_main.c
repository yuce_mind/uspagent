/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "uspa.h"
#include "uspa_agent.h"
#include "uspa_mtp.h"
#include "uspa_controller.h"
#include "uspa_controller_mtp.h"
#include "uspa_imtp.h"
#include "uspa_subscription.h"
#include "uspa_mtp_mqtt.h"
#include "uspa_mtp_imtp.h"

static uspa_app_t app;

static void uspa_set_busctx(amxo_parser_t* parser) {
    amxc_llist_for_each(it, parser->connections) {
        amxo_connection_t* con = amxc_container_of(it, amxo_connection_t, it);
        if(con->type == AMXO_BUS) {
            app.bus_ctx = (amxb_bus_ctx_t*) con->priv;
        }
    }
    return;
}

static void uspa_config_set_eid(amxo_parser_t* parser) {
    amxc_var_t* usp = GET_ARG(&parser->config, "usp");
    amxc_var_t* eid = NULL;

    if(usp == NULL) {
        usp = amxc_var_add_key(amxc_htable_t, &parser->config, "usp", NULL);
    }

    eid = GET_ARG(usp, "EndpointID");
    if(eid == NULL) {
        char* eid_str = uspa_agent_get_eid();
        when_str_empty(eid_str, exit);
        amxc_var_add_key(cstring_t, usp, "EndpointID", eid_str);
        free(eid_str);
    }

exit:
    return;
}

static void sigpipe_ignore(UNUSED const char* const sig_name,
                           UNUSED const amxc_var_t* const data,
                           UNUSED void* const priv) {
    SAH_TRACEZ_WARNING(ME, "Ignore SIGPIPE. Expected if other end closed connection without waiting for response");
}

static void uspa_init(amxo_parser_t* parser) {
    SAH_TRACEZ_INFO(ME, "uspagent started");
    uspa_set_busctx(parser);
    amxp_sigmngr_add_signal(NULL, "dm:OperationComplete");

    uspa_config_set_eid(parser);
    uspa_mtp_subscribe(app.bus_ctx);
    uspa_controller_subscribe(app.bus_ctx);
    uspa_controller_mtp_subscribe(app.bus_ctx);
    uspa_subscription_subscribe(app.bus_ctx);
    uspa_mtp_mqtt_subscribe(app.bus_ctx);

    uspa_mtp_imtp_init();
    uspa_mtp_init(app.bus_ctx);
    uspa_controller_init(app.bus_ctx);
    uspa_subscription_init(app.bus_ctx);

    amxp_syssig_enable(SIGPIPE, true);
    amxp_slot_connect(NULL, strsignal(SIGPIPE), NULL, sigpipe_ignore, NULL);
}

static void uspa_cleanup(void) {
    SAH_TRACEZ_INFO(ME, "uspagent stopped");
    amxp_signal_t* sig = amxp_sigmngr_find_signal(NULL, "dm:OperationComplete");

    amxp_sigmngr_remove_signal(NULL, "dm:OperationComplete");
    amxp_signal_delete(&sig);

    uspa_agent_set_status("Down");
    uspa_subscription_clean();
    uspa_mtp_imtp_clean();
    uspa_controller_clean();
    uspa_mtp_clean();
    app.parser = NULL;
    app.bus_ctx = NULL;
    return;
}

amxb_bus_ctx_t* uspa_get_busctxt(void) {
    return app.bus_ctx;
}

amxo_parser_t* uspa_get_parser(void) {
    return app.parser;
}

int _uspa_main(int reason,
               UNUSED amxd_dm_t* dm,
               amxo_parser_t* parser) {
    switch(reason) {
    case 0:     // START
        app.parser = parser;
        uspa_init(parser);
        break;
    case 1:     // STOP
        uspa_cleanup();
        break;
    default:
        break;
    }
    return 0;
}
