/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>

#include "uspa_controller.h"
#include "uspa_controller_mtp.h"
#include "uspa_controller_mtp_mqtt.h"
#include "uspa_controller_mtp_imtp.h"
#include "uspa_mtp.h"
#include "uspa_imtp.h"

static int uspa_controller_mtp_configure_protocol(const char* contr_inst, const char* contr_mtp_inst, const char* protocol) {
    int retval = -1;

    when_str_empty(protocol, exit);

    if(strcmp(protocol, "MQTT") == 0) {
        retval = uspa_controller_mtp_mqtt_init(contr_inst, contr_mtp_inst);
    } else if(strcmp(protocol, "IMTP") == 0) {
        retval = uspa_controller_mtp_imtp_init(contr_inst, contr_mtp_inst);
    } else {
        SAH_TRACEZ_WARNING(ME, "%sProtocol = %s, not supported", contr_mtp_inst, protocol);
    }

exit:
    return retval;
}

static char* uspa_controller_mtp_get_protocol(const char* contr_mtp) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    char* protocol = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    retval = amxb_get(ctx, contr_mtp, 0, &ret, 5);
    when_failed(retval, exit);

    protocol = strdup(GETP_CHAR(&ret, "0.0.Protocol"));

exit:
    amxc_var_clean(&ret);
    return protocol;
}

static void uspa_controller_mtp_enabled(const char* contr_mtp_inst) {
    char* contr_inst = NULL;
    bool enable = false;

    contr_inst = uspa_controller_mtp_get_contr(contr_mtp_inst);
    when_str_empty(contr_inst, exit);

    enable = uspa_controller_get_enable(contr_inst);

    if(enable) {
        char* protocol = uspa_controller_mtp_get_protocol(contr_mtp_inst);

        uspa_controller_mtp_configure_protocol(contr_inst, contr_mtp_inst, protocol);
        free(protocol);
    }

exit:
    free(contr_inst);
}

static void uspa_controller_mtp_disabled(const char* contr_mtp_inst) {
    amxd_path_t contr_path;
    amxd_path_init(&contr_path, contr_mtp_inst);

    free(amxd_path_get_last(&contr_path, true));
    free(amxd_path_get_last(&contr_path, true));
    uspa_controller_con_del(amxd_path_get(&contr_path, AMXD_OBJECT_TERMINATE), contr_mtp_inst);

    amxd_path_clean(&contr_path);
}

static void uspa_controller_mtp_enable_toggled(UNUSED const char* const sig_name,
                                               const amxc_var_t* const data,
                                               UNUSED void* const priv) {
    const char* contr_mtp = GET_CHAR(data, "path");
    bool enabled = GETP_BOOL(data, "parameters.Enable.to");
    SAH_TRACEZ_INFO(ME, "Enable of %s is %d", contr_mtp, enabled);

    if(enabled) {
        uspa_controller_mtp_enabled(contr_mtp);
    } else {
        uspa_controller_mtp_disabled(contr_mtp);
    }
}

static void uspa_controller_mtp_added(UNUSED const char* const sig_name,
                                      const amxc_var_t* const data,
                                      UNUSED void* const priv) {
    const char* mtp_template = GET_CHAR(data, "path");
    uint32_t index = GET_UINT32(data, "index");
    amxc_string_t mtp_instance;

    amxc_string_init(&mtp_instance, 0);
    amxc_string_setf(&mtp_instance, "%s%d.", mtp_template, index);
    uspa_controller_mtp_enabled(amxc_string_get(&mtp_instance, 0));

    amxc_string_clean(&mtp_instance);
}

static void uspa_controller_mtp_removed(UNUSED const char* const sig_name,
                                        const amxc_var_t* const data,
                                        UNUSED void* const priv) {
    const char* mtp_template = GET_CHAR(data, "path");
    uint32_t index = GET_UINT32(data, "index");
    amxd_path_t mtp_instance;

    amxd_path_init(&mtp_instance, NULL);
    amxd_path_setf(&mtp_instance, true, "%s%d.", mtp_template, index);
    uspa_controller_mtp_disabled(amxd_path_get(&mtp_instance, AMXD_OBJECT_TERMINATE));

    amxd_path_clean(&mtp_instance);
}

char* uspa_controller_mtp_get_contr(const char* contr_mtp) {
    const char* tmp = NULL;
    char* contr = NULL;
    amxd_path_t path;

    amxd_path_init(&path, NULL);

    when_str_empty(contr_mtp, exit);

    amxd_path_setf(&path, true, "%s", contr_mtp);
    free(amxd_path_get_last(&path, true));
    free(amxd_path_get_last(&path, true));

    tmp = amxd_path_get(&path, AMXD_OBJECT_TERMINATE);
    when_null(tmp, exit);

    contr = strdup(tmp);

exit:
    amxd_path_clean(&path);
    return contr;
}

int uspa_controller_mtp_subscribe(amxb_bus_ctx_t* ctx) {
    int retval = -1;
    const char* path = "LocalAgent.Controller.";
    const char* expr_changed = "(notification == 'dm:object-changed') && \
                                (contains('parameters.Enable')) && \
                                (path matches 'LocalAgent.Controller.[0-9]+.MTP.[0-9]+.$')";
    const char* expr_added = "(notification == 'dm:instance-added') && \
                              (parameters.Enable == true) && \
                              (path matches 'LocalAgent.Controller.[0-9]+.MTP.$')";
    const char* expr_removed = "(notification == 'dm:instance-removed') && \
                                (path matches 'LocalAgent.Controller.[0-9]+.MTP.$')";

    // Subscribe to events for enabled controller MTPs
    retval = amxb_subscribe(ctx, path, expr_changed, uspa_controller_mtp_enable_toggled, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s changed events", path);
        goto exit;
    }

    // Subscribe to events for newly added controller MTPs that are enabled
    retval = amxb_subscribe(ctx, path, expr_added, uspa_controller_mtp_added, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s added events", path);
        goto exit;
    }

    // Subscribe to events for removed controller MTPs
    retval = amxb_subscribe(ctx, path, expr_removed, uspa_controller_mtp_removed, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s removed events", path);
        goto exit;
    }

exit:
    return retval;
}

int uspa_controller_mtp_init(amxb_bus_ctx_t* ctx, const char* contr_inst) {
    int retval = -1;
    amxc_var_t ret;
    amxc_var_t* mtps = NULL;
    amxc_string_t mtp_path;

    amxc_string_init(&mtp_path, 0);
    amxc_var_init(&ret);

    amxc_string_setf(&mtp_path, "%sMTP.", contr_inst);
    retval = amxb_get(ctx, amxc_string_get(&mtp_path, 0), 0, &ret, 5);
    when_failed(retval, exit);

    mtps = GETI_ARG(&ret, 0);
    amxc_var_for_each(mtp, mtps) {
        const char* protocol = NULL;
        bool enable = GET_BOOL(mtp, "Enable");

        if(!enable) {
            continue;
        }

        protocol = GET_CHAR(mtp, "Protocol");
        retval = uspa_controller_mtp_configure_protocol(contr_inst, amxc_var_key(mtp), protocol);
    }

exit:
    amxc_string_clean(&mtp_path);
    amxc_var_clean(&ret);
    return retval;
}
