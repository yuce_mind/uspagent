/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>

#include "uspa_mtp.h"
#include "uspa_mtp_mqtt.h"
#include "uspa_mtp_imtp.h"
#include "uspa_controller.h"
#include "uspa_imtp.h"

amxc_htable_t mtp_instances;        // Track mtp instances: key = LocalAgent.MTP.{i}., value = uspa_mtp_instance_t struct

/**
   @brief
   Callback function when a LocalAgent.MTP.{i}. instance is enabled or an enabled instance is added.

   This function will check which MTP protocol is enabled and call the configuration function
   corresponding to that protocol.
 */
static void uspa_mtp_enabled(const char* mtp) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    uspa_mtp_instance_t* mtp_inst = NULL;
    const char* protocol = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    retval = amxb_get(ctx, mtp, 0, &ret, 5);
    when_failed(retval, exit);

    if(amxc_htable_get(&mtp_instances, mtp) == 0) {
        retval = uspa_mtp_instance_new(&mtp_inst);
        when_failed(retval, exit);
        amxc_htable_insert(&mtp_instances, mtp, &mtp_inst->hit);
    }

    protocol = GETP_CHAR(&ret, "0.0.Protocol");
    when_str_empty(protocol, exit);

    if(strcmp(protocol, "MQTT") == 0) {
        retval = uspa_mtp_mqtt_configure(mtp);
    } else if(strcmp(protocol, "IMTP") == 0) {
        retval = uspa_mtp_imtp_configure(mtp);
    }
    if(retval != 0) {
        SAH_TRACEZ_INFO(ME, "%s with protocol %s was enabled, but not connected", mtp, protocol);
    }

exit:
    amxc_var_clean(&ret);
}

/**
   @brief
   Disables the IMTP connection when an MTP gets disabled.

   This function will be called when a LocalAgent.MTP.{i}. or LocalAgent.Controller.{i}.MTP.{i}.
   instance is disabled.

   @param mtp path to an MTP instance
 */
static void uspa_mtp_disabled(const char* mtp, const char* protocol) {
    if(strcmp(protocol, "MQTT") == 0) {
        uspa_mtp_mqtt_disable(mtp);
    } else if(strcmp(protocol, "IMTP") == 0) {
        uspa_mtp_imtp_remove(mtp);
    }
}

static void uspa_mtp_enable_toggled(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    const char* mtp = GET_CHAR(data, "path");
    bool enabled = GETP_BOOL(data, "parameters.Enable.to");
    const char* protocol = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    retval = amxb_get(ctx, mtp, 0, &ret, 5);
    when_failed(retval, exit);

    protocol = GETP_CHAR(&ret, "0.0.Protocol");
    when_str_empty(protocol, exit);

    SAH_TRACEZ_INFO(ME, "Enable of %s is %d", mtp, enabled);
    if(enabled) {
        uspa_mtp_enabled(mtp);
    } else {
        uspa_mtp_disabled(mtp, protocol);
    }

exit:
    amxc_var_clean(&ret);
}

static void uspa_mtp_added(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* const priv) {
    const char* mtp_template = GET_CHAR(data, "path");
    uint32_t index = GET_UINT32(data, "index");
    amxc_string_t mtp_instance;

    amxc_string_init(&mtp_instance, 0);
    amxc_string_setf(&mtp_instance, "%s%d.", mtp_template, index);
    uspa_mtp_enabled(amxc_string_get(&mtp_instance, 0));

    amxc_string_clean(&mtp_instance);
}

static void uspa_mtp_removed(UNUSED const char* const sig_name,
                             const amxc_var_t* const data,
                             UNUSED void* const priv) {
    const char* mtp_template = GET_CHAR(data, "path");
    const char* protocol = GETP_CHAR(data, "parameters.Protocol");
    uint32_t index = GET_UINT32(data, "index");
    amxc_string_t mtp_instance;
    amxc_htable_it_t* it = NULL;

    amxc_string_init(&mtp_instance, 0);
    amxc_string_setf(&mtp_instance, "%s%d.", mtp_template, index);

    SAH_TRACEZ_INFO(ME, "%s removed", amxc_string_get(&mtp_instance, 0));

    when_str_empty(protocol, exit);
    if(strcmp(protocol, "MQTT") == 0) {
        uspi_con_t* con = uspa_imtp_con_from_la_mtp(amxc_string_get(&mtp_instance, 0));
        when_null(con, exit);

        uspa_controller_con_unlink(con);
        uspa_imtp_disconnect(con);
    } else if(strcmp(protocol, "IMTP") == 0) {
        uspa_mtp_imtp_remove(amxc_string_get(&mtp_instance, 0));
    }

    it = amxc_htable_take(&mtp_instances, amxc_string_get(&mtp_instance, 0));
    amxc_htable_it_clean(it, uspa_mtp_instance_hit_clean);

exit:
    amxc_string_clean(&mtp_instance);
}

int uspa_mtp_subscribe(amxb_bus_ctx_t* ctx) {
    int retval = -1;
    const char* path = "LocalAgent.MTP.";
    const char* expr_changed = "(notification == 'dm:object-changed') && \
                                (contains('parameters.Enable'))";
    const char* expr_added = "(notification == 'dm:instance-added') && (parameters.Enable == true)";
    const char* expr_removed = "(notification == 'dm:instance-removed')";

    retval = amxb_subscribe(ctx, path, expr_changed, uspa_mtp_enable_toggled, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s changed events", path);
        goto exit;
    }

    retval = amxb_subscribe(ctx, path, expr_added, uspa_mtp_added, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s added events", path);
        goto exit;
    }

    retval = amxb_subscribe(ctx, path, expr_removed, uspa_mtp_removed, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s added events", path);
        goto exit;
    }

exit:
    return retval;
}

int uspa_mtp_init(amxb_bus_ctx_t* ctx) {
    int retval = -1;
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxc_htable_init(&mtp_instances, 0);

    retval = amxb_get(ctx, "LocalAgent.MTP.", 0, &ret, 5);

    amxc_var_for_each(mtp, GETI_ARG(&ret, 0)) {
        const char* mtp_path = amxc_var_key(mtp);
        uspa_mtp_instance_t* mtp_inst = NULL;

        if(GET_CHAR(mtp, "Alias") == NULL) {
            continue;
        }

        retval = uspa_mtp_instance_new(&mtp_inst);
        when_failed(retval, exit);

        amxc_htable_insert(&mtp_instances, mtp_path, &mtp_inst->hit);

        if(GET_BOOL(mtp, "Enable")) {
            uspa_mtp_enabled(mtp_path);
        }
    }

exit:
    amxc_var_clean(&ret);
    return retval;
}

void uspa_mtp_clean(void) {
    amxc_htable_clean(&mtp_instances, uspa_mtp_instance_hit_clean);
}

amxc_htable_t* uspa_mtp_get_instances(void) {
    return &mtp_instances;
}

int uspa_mtp_new(uspa_mtp_t** mtp, uint32_t type_id, const char* la_mtp) {
    int retval = -1;

    when_null(mtp, exit);
    when_not_null(*mtp, exit);
    when_str_empty(la_mtp, exit);

    *mtp = calloc(1, sizeof(uspa_mtp_t));
    when_null(*mtp, exit);

    (*mtp)->type_id = type_id;
    (*mtp)->la_mtp = strdup(la_mtp);
    if((*mtp)->la_mtp == NULL) {
        free(*mtp);
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}

void uspa_mtp_delete(uspa_mtp_t** mtp) {
    when_null(mtp, exit);
    when_null(*mtp, exit);

    free((*mtp)->la_mtp);
    free(*mtp);
    *mtp = NULL;

exit:
    return;
}

int uspa_mtp_instance_new(uspa_mtp_instance_t** inst) {
    int retval = -1;

    when_null(inst, exit);

    *inst = (uspa_mtp_instance_t*) calloc(1, sizeof(uspa_mtp_instance_t));
    when_null(*inst, exit);

    retval = 0;
exit:
    return retval;
}

void uspa_mtp_instance_delete(uspa_mtp_instance_t** inst) {
    uspi_con_t* con = NULL;

    when_null(inst, exit);
    when_null(*inst, exit);

    con = (*inst)->con;
    if(con != NULL) {
        uspa_imtp_disconnect(con);
    }

    free(*inst);
    *inst = NULL;

exit:
    return;
}

void uspa_mtp_instance_hit_clean(UNUSED const char* key, amxc_htable_it_t* hit) {
    uspa_mtp_instance_t* inst = amxc_htable_it_get_data(hit, uspa_mtp_instance_t, hit);
    uspa_mtp_instance_delete(&inst);
}

int uspa_mtp_instance_con_add(const char* la_mtp, uspi_con_t* con) {
    int retval = -1;
    amxc_htable_it_t* it = NULL;
    uspa_mtp_instance_t* mtp_inst = NULL;

    when_str_empty(la_mtp, exit);
    when_null(con, exit);

    it = amxc_htable_get(&mtp_instances, la_mtp);
    when_null(it, exit);

    mtp_inst = amxc_container_of(it, uspa_mtp_instance_t, hit);
    mtp_inst->con = con;

    retval = 0;
exit:
    return retval;
}

int uspa_mtp_instance_con_rm(const char* la_mtp) {
    int retval = -1;
    amxc_htable_it_t* it = NULL;
    uspa_mtp_instance_t* mtp_inst = NULL;

    when_str_empty(la_mtp, exit);

    it = amxc_htable_get(&mtp_instances, la_mtp);
    when_null(it, exit);

    mtp_inst = amxc_container_of(it, uspa_mtp_instance_t, hit);
    mtp_inst->con = NULL;

    retval = 0;
exit:
    return retval;
}
