/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <usp/uspl.h>

#include "uspa.h"
#include "uspa_msghandler.h"
#include "uspa_controller.h"

// For object paths, the AMX depth will be USP depth - 1
// For parameter paths, the AMX depth will always be 0
static void uspa_get_determine_depth(const char* path, uint32_t* max_depth) {
    when_str_empty(path, exit);

    if(path[strlen(path) - 1] == '.') {
        *max_depth = *max_depth - 1;
    } else {
        *max_depth = 0;
    }
exit:
    return;
}

static int uspa_validate_get(amxb_bus_ctx_t* ctx,
                             const char* requested_path,
                             const char* acl_file,
                             amxc_llist_t* filters) {
    int retval = -1;
    char* fixed_part = NULL;
    amxc_var_t* acls = NULL;
    bool allowed = false;
    amxd_path_t path;

    amxd_path_init(&path, requested_path);

    acls = amxa_parse_files(acl_file);
    when_null(acls, exit);
    fixed_part = amxd_path_get_fixed_part(&path, false);

    retval = amxa_resolve_search_paths(ctx, acls, fixed_part);
    when_failed(retval, exit);
    retval = amxa_get_filters(acls, AMXA_PERMIT_GET, filters, fixed_part);
    when_failed(retval, exit);

    allowed = amxa_is_get_allowed(filters, fixed_part);
    when_false_do(allowed, exit, SAH_TRACEZ_NOTICE(ME, "USP get not allowed for acl file '%s'", acl_file));

    retval = 0;
exit:
    amxc_var_delete(&acls);
    free(fixed_part);
    amxd_path_clean(&path);

    return retval;
}

static void uspa_get_convert_output(amxc_var_t* get_resp, const char* path, int retval) {
    amxc_var_t* result = NULL;

    if((retval != 0)) {
        SAH_TRACEZ_WARNING(ME, "amxb_get failed with status: [%d] for path: [%s]",
                           retval, path);
        amxc_var_set_type(get_resp, AMXC_VAR_ID_HTABLE);
        result = amxc_var_add_key(amxc_htable_t, get_resp, "result", NULL);
        amxc_var_add_key(cstring_t, result, "requested_path", path);
        amxc_var_add_key(int64_t, result, "err_code", USP_ERR_INVALID_PATH);
        amxc_var_add_key(cstring_t, result, "err_msg", "Invalid path");
    } else {
        result = amxc_var_add_key(amxc_htable_t, get_resp, "result", NULL);
        amxc_var_add_key(cstring_t, result, "requested_path", path);
        amxc_var_add_key(int64_t, result, "err_code", USP_ERR_OK);
    }
}

static void uspa_filter_get_resp(const char* requested_path,
                                 amxc_var_t* rv,
                                 amxc_llist_t* filters,
                                 int* retval) {
    amxd_path_t path;
    const char* param = NULL;
    amxc_var_t* res_before_filtering = GETP_ARG(rv, "0.0");
    amxc_var_t* res_after_filtering = NULL;

    amxd_path_init(&path, requested_path);
    param = amxd_path_get_param(&path);

    amxa_filter_get_resp(rv, filters);
    res_after_filtering = GETP_ARG(rv, "0.0");
    if((res_before_filtering != NULL) && (res_after_filtering == NULL)) {
        *retval = -1;
        goto exit;
    }

    // If the requested path is a param path, it will be part of the return variant
    // If it gets filetered out -> not in supported dm
    if(param != NULL) {
        amxc_var_t* result = GETP_ARG(rv, "0.0");
        if(result != NULL) {
            bool filtered = amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, result));
            if(filtered) {
                *retval = -1;
            }
        }
    }

exit:
    amxd_path_clean(&path);
}

static void uspa_invoke_get(amxb_bus_ctx_t* ctx,
                            amxc_llist_t* resp_list,
                            const char* path,
                            uint32_t max_depth,
                            const char* acl_file,
                            uint32_t access) {
    SAH_TRACEZ_INFO(ME, "Invoke get on path: [%s]", path);
    int retval = -1;
    amxc_var_t* resolved = NULL;
    amxc_var_t rv_table;
    amxc_var_t rv;
    amxc_llist_t filters;

    amxc_var_init(&rv_table);
    amxc_var_set_type(&rv_table, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&rv);
    amxc_var_set_type(&rv, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&resolved);
    amxc_llist_init(&filters);

    uspa_get_determine_depth(path, &max_depth);

    amxb_set_access(ctx, access);
    retval = uspa_validate_get(ctx, path, acl_file, &filters);
    when_failed(retval, exit);

    retval = amxb_get(ctx, path, max_depth, &rv, 5);
    if(retval == 0) {
        uspa_filter_get_resp(path, &rv, &filters, &retval);
    }

    amxc_var_move(&rv_table, GETI_ARG(&rv, 0));

exit:
    amxb_set_access(ctx, AMXB_PROTECTED);
    uspa_get_convert_output(&rv_table, path, retval);
    amxc_llist_clean(&filters, amxc_string_list_it_free);
    amxc_var_move(resolved, &rv_table);
    amxc_llist_append(resp_list, &resolved->lit);
    amxc_var_clean(&rv);
    amxc_var_clean(&rv_table);
}

int uspa_handle_get(uspl_rx_t* usp_rx,
                    uspl_tx_t** usp_tx,
                    const char* acl_file) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type Get");
    int retval = -1;
    amxc_var_t result;
    amxc_var_t* paths = NULL;
    uint32_t max_depth = 0;
    amxc_llist_t resp_list;
    uint32_t access = AMXB_PUBLIC;

    amxc_var_init(&result);
    amxc_llist_init(&resp_list);

    when_null(usp_rx, exit);
    when_null(usp_tx, exit);

    when_false(uspl_get_extract(usp_rx, &result) == 0, exit);

    access = uspa_controller_get_access(uspl_msghandler_from_id(usp_rx));
    paths = GET_ARG(&result, "paths");
    max_depth = GET_UINT32(&result, "max_depth");
    amxc_var_for_each(var, paths) {
        const char* path = amxc_var_constcast(cstring_t, var);
        amxb_bus_ctx_t* ctx = uspa_discovery_get_ctx(path);
        uspa_invoke_get(ctx, &resp_list, path, max_depth, acl_file, access);
    }

    retval = uspa_msghandler_build_reply(usp_rx, &resp_list, uspl_get_resp_new, usp_tx);

exit:
    amxc_var_clean(&result);
    amxc_llist_clean(&resp_list, variant_list_it_free);

    return retval;
}
