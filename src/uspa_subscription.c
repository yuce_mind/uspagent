/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "uspa_subscription.h"
#include "uspa_imtp.h"
#include "uspa_agent.h"
#include "uspa_controller.h"
#include "uspa_msghandler.h"
#include "uspa_object.h"
#include "uspa_prefix_check.h"
#include "uspa_mtp_imtp.h"
#include "uspa_role.h"

typedef int (* uspa_request_fn_t) (amxc_var_t*, uspl_tx_t*, uspa_sub_ref_t*, bool);

typedef struct _uspa_request_builder {
    const char* notification_type;
    uspa_request_fn_t fn;
} uspa_request_builder_t;

static amxc_llist_t sub_list;
static amxp_timer_t* boot_event_timer = NULL;

static int uspa_sub_new(uspa_sub_t** sub,
                        const char* inst_path,
                        const char* contr_path,
                        const char* id,
                        const char* notif_type) {
    int retval = -1;
    amxc_string_t path;

    amxc_string_init(&path, 0);

    when_null(sub, exit);
    when_str_empty(inst_path, exit);
    when_str_empty(contr_path, exit);
    when_str_empty(id, exit);
    when_str_empty(notif_type, exit);

    *sub = calloc(1, sizeof(uspa_sub_t));
    when_null((*sub), exit);

    (*sub)->instance_path = strdup(inst_path);
    if((*sub)->instance_path == NULL) {
        free(*sub);
        goto exit;
    }

    amxc_string_setf(&path, "%s", contr_path);
    if(uspa_prefix_check_device(contr_path)) {
        uspa_prefix_device_strip(&path);
    }
    (*sub)->contr_path = amxc_string_take_buffer(&path);

    (*sub)->id = strdup(id);
    if((*sub)->id == NULL) {
        free((*sub)->contr_path);
        free((*sub)->instance_path);
        free(*sub);
        goto exit;
    }

    (*sub)->notif_type = strdup(notif_type);
    if((*sub)->notif_type == NULL) {
        free((*sub)->id);
        free((*sub)->contr_path);
        free((*sub)->instance_path);
        free(*sub);
        goto exit;
    }

    amxc_llist_init(&(*sub)->sub_ref_list);
    amxc_llist_init(&(*sub)->retry_list);

    retval = 0;
exit:
    amxc_string_clean(&path);
    return retval;
}

static void uspa_sub_ref_list_it_delete(amxc_llist_it_t* it) {
    uspa_sub_ref_t* sub_ref = amxc_container_of(it, uspa_sub_ref_t, lit);
    free(sub_ref->ref_path);
    free(sub_ref);
}

static void uspa_sub_retry_list_it_delete(amxc_llist_it_t* it) {
    uspa_sub_retry_t* sub_retry = amxc_container_of(it, uspa_sub_retry_t, lit);
    uspa_sub_retry_delete(&sub_retry);
}

static void uspa_sub_delete(uspa_sub_t** sub) {
    when_null(sub, exit);
    when_null(*sub, exit);

    amxp_timer_delete(&(*sub)->periodic_timer);
    amxc_llist_clean(&(*sub)->sub_ref_list, uspa_sub_ref_list_it_delete);
    amxc_llist_clean(&(*sub)->retry_list, uspa_sub_retry_list_it_delete);
    free((*sub)->instance_path);
    free((*sub)->contr_path);
    free((*sub)->id);
    free((*sub)->notif_type);
    free((*sub));
    *sub = NULL;

exit:
    return;
}

static void uspa_sub_list_it_delete(amxc_llist_it_t* it) {
    uspa_sub_t* sub = amxc_container_of(it, uspa_sub_t, lit);

    when_null(sub, exit);

    uspa_sub_delete(&sub);

exit:
    return;
}

static uspa_sub_t* uspa_sub_find_from_instance(const char* instance_path) {
    uspa_sub_t* sub = NULL;

    amxc_llist_for_each(it, &sub_list) {
        uspa_sub_t* tmp = amxc_container_of(it, uspa_sub_t, lit);
        if(strcmp(tmp->instance_path, instance_path) == 0) {
            sub = tmp;
            break;
        }
    }

    return sub;
}

static uspl_tx_t* uspa_sub_build_tx(uspa_sub_t* sub, amxc_var_t* request) {
    int retval = -1;
    char* agent_eid = NULL;
    char* controller_eid = NULL;
    uspl_tx_t* usp_tx = NULL;

    controller_eid = uspa_controller_get_eid(sub->contr_path);
    when_str_empty(controller_eid, exit);

    agent_eid = uspa_agent_get_eid();
    when_str_empty(agent_eid, exit);

    uspl_tx_new(&usp_tx, agent_eid, controller_eid);
    retval = uspl_notify_new(usp_tx, request);
    if(retval != 0) {
        uspl_tx_delete(&usp_tx);
        usp_tx = NULL;
    }

exit:
    free(agent_eid);
    free(controller_eid);
    return usp_tx;
}

static void uspa_operation_complete_resp(UNUSED const char* const sig_name,
                                         const amxc_var_t* const data,
                                         void* const priv) {
    uspl_tx_t* usp_tx = NULL;
    amxc_var_t request;
    uspa_sub_t* sub = (uspa_sub_t*) priv;
    uspi_con_t* con = NULL;
    amxc_var_t mtp_info;
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Operation completed with path: %s", GET_CHAR(data, "cmd_name"));
    SAH_TRACEZ_INFO(ME, "and command_key: %s", GET_CHAR(data, "cmd_key"));
    amxc_var_init(&request);
    amxc_var_init(&mtp_info);

    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &request, "subscription_id", sub->id);
    amxc_var_add_key(bool, &request, "send_resp", false);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);
    amxc_var_add_key(amxc_htable_t, &request, "oper_complete", amxc_var_constcast(amxc_htable_t, data));

    usp_tx = uspa_sub_build_tx(sub, &request);
    when_null(usp_tx, exit);

    con = uspa_imtp_con_from_contr(sub->contr_path);
    if(con == NULL) {
        SAH_TRACEZ_WARNING(ME, "No IMTP connection found for %s, not sending notification", sub->contr_path);
        goto exit;
    }

    retval = amxb_call(uspa_get_busctxt(), sub->contr_path, "GetMTPInfo", NULL, &mtp_info, 5);
    when_failed(retval, exit);

    uspa_msghandler_send_usp_record(con, usp_tx, GETI_ARG(&mtp_info, 0));

exit:
    amxc_var_clean(&mtp_info);
    amxc_var_clean(&request);
    uspl_tx_delete(&usp_tx);
}

static void subscribe_oper_complete(amxd_path_t* path, uspa_sub_t* sub) {
    amxc_string_t expr;
    const char* command = NULL;

    amxc_string_init(&expr, 0);
    amxc_string_setf(&expr, "path starts with '%s'", amxd_path_get(path, AMXD_OBJECT_TERMINATE));

    command = amxd_path_get_param(path);
    if(command != NULL) {
        amxc_string_appendf(&expr, " && cmd_name == '%s'", command);
    }
    SAH_TRACEZ_INFO(ME, "Subscribing to dm:OperationComplete signals with expression: %s",
                    amxc_string_get(&expr, 0));
    amxp_slot_connect(NULL, "dm:OperationComplete", amxc_string_get(&expr, 0),
                      uspa_operation_complete_resp, sub);

    amxc_string_clean(&expr);
    return;
}

static void uspa_subscription_expr_event(amxd_path_t* path, amxc_string_t* expr) {
    const char* event = amxd_path_get_param(path);

    if((event != NULL) && (*event != 0)) {
        amxc_string_setf(expr, "(notification == '%s')", event);
    } else {
        amxc_string_setf(expr, "notification matches '.*!$'");
    }
}

static char* uspa_subscription_expression(amxd_path_t* path,
                                          const char* notif_type) {
    char* expr = NULL;
    amxc_string_t expr_str;
    amxc_string_init(&expr_str, 0);
    const char* param = amxd_path_get_param(path);

    when_str_empty(notif_type, exit);

    if(strcmp("ValueChange", notif_type) == 0) {
        amxc_string_setf(&expr_str, "(notification == 'dm:object-changed')");
    } else if(strcmp("ObjectCreation", notif_type) == 0) {
        amxc_string_setf(&expr_str, "(notification == 'dm:instance-added')");
    } else if(strcmp("ObjectDeletion", notif_type) == 0) {
        amxc_string_setf(&expr_str, "(notification == 'dm:instance-removed')");
    } else if(strcmp("Event", notif_type) == 0) {
        uspa_subscription_expr_event(path, &expr_str);
        expr = amxc_string_take_buffer(&expr_str);
        goto exit;
    } else {
        goto exit;
    }

    if(param != NULL) {
        amxc_string_appendf(&expr_str, " && contains('parameters.%s')", param);
    }

    expr = amxc_string_take_buffer(&expr_str);

exit:
    return expr;
}

static amxc_var_t* uspi_subs_vc_get_param(amxc_var_t* sig_params, const char* ref_path) {
    amxd_path_t path;
    const char* param = NULL;
    amxc_var_t* result = NULL;

    amxd_path_init(&path, ref_path);
    param = amxd_path_get_param(&path);
    if(param != NULL) {
        result = GET_ARG(sig_params, param);
    } else {
        result = GETI_ARG(sig_params, 0);
    }

    amxd_path_clean(&path);
    return result;
}

// Return
// -1 in case of error or when no parameters remain to be sent
// 0 when only 1 parameter has to be sent
// 1 if more param notifications need to be built
static int uspa_subs_build_value_change(amxc_var_t* data,
                                        uspl_tx_t* usp_tx,
                                        uspa_sub_ref_t* sub_ref,
                                        bool retry) {
    int retval = 1;
    amxc_var_t request;
    amxc_string_t param_path;
    char* param_value = NULL;
    uspa_sub_t* sub = sub_ref->sub;
    const char* param_name = NULL;
    const char* path = GET_CHAR(data, "path");
    amxc_var_t* sig_params = GET_ARG(data, "parameters");
    amxc_var_t* value_change = NULL;
    amxc_var_t* param = NULL;

    amxc_var_init(&request);
    amxc_string_init(&param_path, 0);

    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_VALUE_CHANGE);
    amxc_var_add_key(cstring_t, &request, "subscription_id", sub->id);
    amxc_var_add_key(bool, &request, "send_resp", retry);
    value_change = amxc_var_add_key(amxc_htable_t, &request, "value_change", NULL);

    param = uspi_subs_vc_get_param(sig_params, sub_ref->ref_path);
    when_null_status(param, exit, retval = -1);

    param_name = amxc_var_key(param);
    param_value = amxc_var_dyncast(cstring_t, GET_ARG(param, "to"));

    amxc_string_setf(&param_path, "%s%s", path, param_name);
    amxc_var_add_key(cstring_t, value_change, "param_path", amxc_string_get(&param_path, 0));
    amxc_var_add_key(cstring_t, value_change, "param_value", param_value);

    SAH_TRACEZ_INFO(ME, "Build notification of type ValueChange for %s = %s",
                    amxc_string_get(&param_path, 0), param_value);
    uspl_notify_new(usp_tx, &request);

exit:
    amxc_var_delete(&param);
    if(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, sig_params))) {
        retval = 0;
    }
    free(param_value);
    amxc_var_clean(&request);
    amxc_string_clean(&param_path);
    return retval;
}

static int uspa_subs_build_instance_added(amxc_var_t* data,
                                          uspl_tx_t* usp_tx,
                                          uspa_sub_ref_t* sub_ref,
                                          bool retry) {
    int retval = -1;
    amxc_var_t request;
    uspa_sub_t* sub = sub_ref->sub;
    amxc_var_t* keys = GET_ARG(data, "keys");
    amxc_var_t* obj_creation = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);
    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);

    amxc_string_setf(&path, "%s%d.", GET_CHAR(data, "path"), GET_UINT32(data, "index"));
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_OBJ_CREATION);
    amxc_var_add_key(cstring_t, &request, "subscription_id", sub->id);
    amxc_var_add_key(bool, &request, "send_resp", retry);
    obj_creation = amxc_var_add_key(amxc_htable_t, &request, "obj_creation", NULL);

    amxc_var_add_key(amxc_htable_t, obj_creation, "keys", amxc_var_constcast(amxc_htable_t, keys));
    amxc_var_add_key(cstring_t, obj_creation, "path", amxc_string_get(&path, 0));

    SAH_TRACEZ_INFO(ME, "Build notification of type ObjectCreation for path = %s", amxc_string_get(&path, 0));
    retval = uspl_notify_new(usp_tx, &request);

    amxc_string_clean(&path);
    amxc_var_clean(&request);
    return retval;
}

static int uspa_subs_build_instance_removed(amxc_var_t* data,
                                            uspl_tx_t* usp_tx,
                                            uspa_sub_ref_t* sub_ref,
                                            bool retry) {
    int retval = -1;
    amxc_var_t request;
    uspa_sub_t* sub = sub_ref->sub;
    const char* path = GET_CHAR(data, "path");
    amxc_var_t* obj_deletion = NULL;

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_OBJ_DELETION);
    amxc_var_add_key(cstring_t, &request, "subscription_id", sub->id);
    amxc_var_add_key(bool, &request, "send_resp", retry);
    obj_deletion = amxc_var_add_key(amxc_htable_t, &request, "obj_deletion", NULL);
    amxc_var_add_key(cstring_t, obj_deletion, "path", path);

    SAH_TRACEZ_INFO(ME, "Build notification of type ObjectDeletion for path = %s", path);
    retval = uspl_notify_new(usp_tx, &request);

    amxc_var_clean(&request);
    return retval;
}

static int uspa_subs_build_amx(amxc_var_t* data,
                               uspl_tx_t* usp_tx,
                               uspa_sub_ref_t* sub_ref,
                               bool retry) {
    int retval = -1;
    amxc_var_t request;
    uspa_sub_t* sub = sub_ref->sub;
    amxc_var_t* amx_notification = NULL;

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_AMX_NOTIFICATION);
    amxc_var_add_key(cstring_t, &request, "subscription_id", sub->id);
    amxc_var_add_key(bool, &request, "send_resp", retry);
    amx_notification = amxc_var_add_key(amxc_htable_t, &request, "amx_notification", NULL);
    amxc_var_move(amx_notification, data);

    SAH_TRACEZ_INFO(ME, "Build notification of type AmxNotification for path = %s",
                    GET_CHAR(amx_notification, "path"));
    retval = uspl_notify_new(usp_tx, &request);

    amxc_var_clean(&request);
    return retval;
}

static int uspa_subs_boot_event_add_param(const char* param_pattern, amxc_var_t* boot_params) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    amxc_var_t ret;
    amxc_string_t param_path;

    amxc_string_init(&param_path, 0);
    amxc_var_init(&ret);
    retval = amxb_get(ctx, param_pattern, 0, &ret, 5);
    when_failed(retval, exit);
    amxc_var_for_each(result, GETI_ARG(&ret, 0)) {
        const char* obj_path = amxc_var_key(result);

        amxc_var_for_each(param, result) {
            const char* param_name = amxc_var_key(param);
            amxc_string_setf(&param_path, "%s%s", obj_path, param_name);
            amxc_var_set_key(boot_params, amxc_string_get(&param_path, 0), param, AMXC_VAR_FLAG_DEFAULT);
        }
    }

exit:
    amxc_string_clean(&param_path);
    amxc_var_clean(&ret);
    return retval;
}

// Reset LocalAgent.Cause and LocalAgent.CommandKey after Boot! event is sent.
static void uspa_subs_boot_params_reset(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Cause", "LocalReboot");
    amxc_var_add_key(cstring_t, &args, "CommandKey", "");

    if(amxb_set(ctx, "LocalAgent.", &args, &ret, 5) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to reset LocalAgent.{Cause, CommandKey}");
    }

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

// TODO add FirmwareUpdated
static int uspa_subs_populate_boot_event(amxc_var_t* event, uspa_sub_t* sub) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    const char* cause = NULL;
    const char* cmd_key = NULL;
    amxc_string_t path;
    amxc_var_t ret;
    amxc_var_t boot_params;

    amxc_var_init(&ret);
    amxc_var_init(&boot_params);
    amxc_var_set_type(&boot_params, AMXC_VAR_ID_HTABLE);
    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "%sBootParameter.[Enable == true].", sub->contr_path);

    retval = amxb_get(ctx, "LocalAgent.", 0, &ret, 5);
    when_failed(retval, exit);

    cmd_key = GETP_CHAR(&ret, "0.0.CommandKey");
    amxc_var_add_key(cstring_t, event, "CommandKey", cmd_key);

    cause = GETP_CHAR(&ret, "0.0.Cause");
    amxc_var_add_key(cstring_t, event, "Cause", cause);

    retval = amxb_get(ctx, amxc_string_get(&path, 0), 0, &ret, 5);
    when_true(retval != 0 || GETP_ARG(&ret, "0.0") == NULL, exit);

    amxc_var_for_each(param, GETI_ARG(&ret, 0)) {
        const char* param_pattern = GET_CHAR(param, "ParameterName");
        uspa_subs_boot_event_add_param(param_pattern, &boot_params);
    }

    amxc_var_cast(&boot_params, AMXC_VAR_ID_JSON);
    amxc_var_add_key(cstring_t, event, "ParameterMap", amxc_var_constcast(jstring_t, &boot_params));

    // Use timer to go through event loop
    amxp_timer_start(boot_event_timer, 1000);

exit:
    amxc_string_clean(&path);
    amxc_var_clean(&boot_params);
    amxc_var_clean(&ret);
    return retval;
}

static int uspa_subs_build_event(const amxc_var_t* const data,
                                 uspl_tx_t* usp_tx,
                                 uspa_sub_t* sub,
                                 bool retry) {
    int retval = -1;
    amxc_var_t request;
    const char* path = GET_CHAR(data, "path");
    const char* event_name = GET_CHAR(data, "notification");
    amxc_var_t* params = GET_ARG(data, "data");
    amxc_var_t* event = NULL;

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_EVENT);
    amxc_var_add_key(cstring_t, &request, "subscription_id", sub->id);
    amxc_var_add_key(bool, &request, "send_resp", retry);
    event = amxc_var_add_key(amxc_htable_t, &request, "event", NULL);
    amxc_var_add_key(cstring_t, event, "path", path);
    amxc_var_add_key(cstring_t, event, "event_name", event_name);

    amxc_var_for_each(param, params) {
        const char* key = amxc_var_key(param);
        const char* value = NULL;

        // Add composite parameters in JSON string format
        if((amxc_var_type_of(param) == AMXC_VAR_ID_HTABLE) ||
           (amxc_var_type_of(param) == AMXC_VAR_ID_LIST)) {
            amxc_var_cast(param, AMXC_VAR_ID_JSON);
            value = amxc_var_constcast(jstring_t, param);
        } else {
            amxc_var_cast(param, AMXC_VAR_ID_CSTRING);
            value = amxc_var_constcast(cstring_t, param);
        }
        amxc_var_add_key(cstring_t, event, key, value);
    }
    if(strcmp(event_name, "Boot!") == 0) {
        uspa_subs_populate_boot_event(event, sub);
    }

    SAH_TRACEZ_INFO(ME, "Build notification of type Event: %s%s", path, event_name);
    retval = uspl_notify_new(usp_tx, &request);

    amxc_var_clean(&request);
    return retval;
}

static int uspa_subscription_build_pbuf(amxc_var_t* data,
                                        uspa_sub_ref_t* sub_ref,
                                        uspl_tx_t** usp_tx,
                                        bool retry) {
    int retval = -1;
    char* from_id = NULL;
    char* to_id = NULL;
    const char* notification = GET_CHAR(data, "notification");
    uspa_request_builder_t fn[] = {
        {"dm:object-changed", uspa_subs_build_value_change},
        {"dm:instance-added", uspa_subs_build_instance_added},
        {"dm:instance-removed", uspa_subs_build_instance_removed},
        {NULL, NULL},
    };

    when_str_empty(notification, exit);

    from_id = uspa_agent_get_eid();
    to_id = uspa_controller_get_eid(sub_ref->sub->contr_path);
    retval = uspl_tx_new(usp_tx, from_id, to_id);
    when_failed(retval, exit);

    if(strcmp(sub_ref->sub->notif_type, "AmxNotification") == 0) {
        retval = uspa_subs_build_amx(data, *usp_tx, sub_ref, retry);
        goto exit;
    }

    for(int i = 0; fn[i].notification_type != NULL; i++) {
        if(strcmp(fn[i].notification_type, notification) == 0) {
            retval = fn[i].fn(data, *usp_tx, sub_ref, retry);
            goto exit;
        }
    }

    if(notification[strlen(notification) - 1] == '!') {
        uspa_subs_build_event(data, *usp_tx, sub_ref->sub, retry);
    }

exit:
    free(from_id);
    free(to_id);
    return retval;
}

static void retry_timeout(UNUSED amxp_timer_t* timer, void* priv) {
    uspa_sub_retry_t* sub_retry = (uspa_sub_retry_t*) priv;
    int retval = -1;
    uspa_sub_t* sub = NULL;
    uspi_con_t* con = NULL;
    amxc_var_t mtp_info;

    SAH_TRACEZ_INFO(ME, "Retrying notification");
    amxc_var_init(&mtp_info);

    sub = amxc_container_of(sub_retry->lit.llist, uspa_sub_t, retry_list);
    when_null(sub, exit);

    con = uspa_imtp_con_from_contr(sub->contr_path);
    if(con == NULL) {
        SAH_TRACEZ_WARNING(ME, "No IMTP connection found for %s, not sending notification", sub->contr_path);
        goto exit;
    }

    retval = amxb_call(uspa_get_busctxt(), sub->contr_path, "GetMTPInfo", NULL, &mtp_info, 5);
    when_failed(retval, exit);

    if(sub_retry->retry_attempts < USPA_SUB_RETRY_MAX) {
        uint32_t interval_mult = uspa_object_get_value(uint32_t, sub->contr_path,
                                                       "USPNotifRetryIntervalMultiplier");
        (sub_retry->retry_attempts)++;
        (sub_retry->retry_time) *= (interval_mult / 1000);
    }

    uspa_imtp_send(con, sub_retry->usp_tx, GETI_ARG(&mtp_info, 0));

    SAH_TRACEZ_NOTICE(ME, "Next USP notification retry with id %s in %d seconds", sub->id, sub_retry->retry_time);
    amxp_timer_start(sub_retry->retry_timer, 1000 * sub_retry->retry_time);

exit:
    amxc_var_clean(&mtp_info);
}

// TODO check what happens if parameters are set to 0
static uint32_t uspa_calc_retry_timeout(uspa_sub_t* sub) {
    int retval = -1;
    uint32_t interval_mult = 0;
    uint32_t interval_min = 0;
    uint32_t interval_max = 0;
    uint32_t result = 0;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    amxc_var_t ret;

    amxc_var_init(&ret);

    retval = amxb_get(ctx, sub->contr_path, 0, &ret, 5);
    when_failed(retval, exit);

    interval_mult = GETP_UINT32(&ret, "0.0.USPNotifRetryIntervalMultiplier");
    interval_min = GETP_UINT32(&ret, "0.0.USPNotifRetryMinimumWaitInterval");

    interval_max = interval_min * interval_mult / 1000;
    srand(time(0));
    result = (rand() % (interval_max - interval_min)) + interval_min;

exit:
    amxc_var_clean(&ret);

    return result;
}

static void notif_expire(UNUSED amxp_timer_t* timer, void* priv) {
    SAH_TRACEZ_INFO(ME, "Notification expired");
    uspa_sub_retry_t* sub_retry = (uspa_sub_retry_t*) priv;
    uspa_sub_retry_delete(&sub_retry);
}

static void uspa_sub_retry_new(uspa_sub_t* sub, uspl_tx_t* usp_tx) {
    uspa_sub_retry_t* sub_retry = NULL;
    uint32_t notif_expiration = 0;

    sub_retry = calloc(1, sizeof(uspa_sub_retry_t));
    when_null(sub_retry, exit);

    amxc_llist_append(&sub->retry_list, &sub_retry->lit);

    sub_retry->usp_tx = usp_tx;
    notif_expiration = uspa_object_get_uint32_t(sub->instance_path, "NotifExpiration");

    amxp_timer_new(&sub_retry->retry_timer, retry_timeout, sub_retry);
    sub_retry->retry_time = uspa_calc_retry_timeout(sub);
    SAH_TRACEZ_NOTICE(ME, "Next USP notification retry with id %s in %d seconds", sub->id, sub_retry->retry_time);
    amxp_timer_start(sub_retry->retry_timer, 1000 * sub_retry->retry_time);
    if(notif_expiration > 0) {
        amxp_timer_new(&sub_retry->expiration_timer, notif_expire, sub_retry);
        amxp_timer_start(sub_retry->expiration_timer, 1000 * notif_expiration);
    }

exit:
    return;
}

static uspa_sub_ref_t* uspa_sub_ref_new(uspa_sub_t* sub, const char* ref_path) {
    uspa_sub_ref_t* sub_ref = (uspa_sub_ref_t*) calloc(1, sizeof(uspa_sub_ref_t));
    when_null(sub_ref, exit);

    sub_ref->ref_path = strdup(ref_path);
    if(sub_ref->ref_path == NULL) {
        free(sub_ref);
        sub_ref = NULL;
        goto exit;
    }
    sub_ref->sub = sub;
    amxc_llist_append(&sub->sub_ref_list, &sub_ref->lit);

exit:
    return sub_ref;
}

static uspa_sub_ref_t* uspa_sub_ref_find(uspa_sub_t* sub, const char* ref_path) {
    uspa_sub_ref_t* result = NULL;

    amxc_llist_for_each(lit, &sub->sub_ref_list) {
        uspa_sub_ref_t* sub_ref = amxc_llist_it_get_data(lit, uspa_sub_ref_t, lit);
        if(strcmp(sub_ref->ref_path, ref_path) == 0) {
            result = sub_ref;
            break;
        }
    }

    return result;
}

static amxd_path_t* uspa_subscription_build_param_path(uspa_sub_t* sub,
                                                       const amxc_llist_t* ref_list) {
    int retval = -1;
    amxc_var_t* first = NULL;
    amxd_path_t* sub_path = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxd_path_new(&sub_path, NULL);

    if(ref_list == NULL) {
        if(amxb_get(uspa_get_busctxt(), sub->instance_path, 0, &ret, 5) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to get %s", sub->instance_path);
            goto exit;
        }
        ref_list = amxc_var_constcast(amxc_llist_t, GETP_ARG(&ret, "0.0.ReferenceList"));
    }

    first = amxc_var_from_llist_it(amxc_llist_get_first(ref_list));
    amxd_path_setf(sub_path, false, "%s", amxc_var_constcast(cstring_t, first));

    retval = 0;
exit:
    if(retval != 0) {
        amxd_path_delete(&sub_path);
    }
    amxc_var_clean(&ret);
    return sub_path;
}

static void uspa_subscription_save_last_value(uspa_sub_t* sub, amxd_path_t* param_path) {
    amxb_bus_ctx_t* bus_ctx = uspa_get_busctxt();
    amxc_var_t* target_obj = NULL;
    amxc_var_t* current_value = NULL;
    const char* param = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    SAH_TRACEZ_INFO(ME, "Save last value for %s", sub->instance_path);
    if(amxb_get(bus_ctx, amxd_path_get(param_path, AMXD_OBJECT_TERMINATE), 0, &ret, 5) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to get %s", amxd_path_get(param_path, AMXD_OBJECT_TERMINATE));
        goto exit;
    }

    target_obj = GETP_ARG(&ret, "0.0");
    param = amxd_path_get_param(param_path);
    current_value = GET_ARG(target_obj, param);
    when_null(current_value, exit);

    amxc_var_cast(current_value, AMXC_VAR_ID_CSTRING);
    amxc_var_set_key(&args, "LastValue", current_value, AMXC_VAR_FLAG_DEFAULT);

    SAH_TRACEZ_INFO(ME, "Set %sLastValue to %s", sub->instance_path,
                    GET_CHAR(&args, "LastValue"));
    if(amxb_set(bus_ctx, sub->instance_path, &args, &ret, 5) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to set %sLastValue", sub->instance_path);
        goto exit;
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}

static int uspa_subscription_filter_notification(amxc_var_t* data,
                                                 const char* eid,
                                                 const char* contr_path) {
    int retval = -1;
    const char* path = GET_CHAR(data, "path");
    amxb_bus_ctx_t* ctx = uspa_discovery_get_ctx(path);
    amxc_string_t* acl_file = NULL;
    amxc_var_t* acls = NULL;

    // For IMTP connections to containers, eid will be known
    // For IMTP connections to other MTP plugins, eid isn't known
    if((eid == NULL) || (*eid == 0)) {
        char* fetched_eid = uspa_controller_get_eid(contr_path);
        acl_file = uspa_role_get_acl_file(fetched_eid);
        free(fetched_eid);
    } else {
        acl_file = uspa_role_get_acl_file(eid);
    }
    when_null(acl_file, exit);

    acls = amxa_parse_files(amxc_string_get(acl_file, 0));
    when_null(acls, exit);

    retval = amxa_resolve_search_paths(ctx, acls, path);
    when_failed(retval, exit);

    retval = amxa_filter_notif(data, acls);
    when_failed(retval, exit);

    if(GET_BOOL(data, "filter-all")) {
        SAH_TRACEZ_WARNING(ME, "Controller '%s' doesn't have permission for notification", eid);
        retval = -1;
        goto exit;
    }

exit:
    amxc_string_delete(&acl_file);
    amxc_var_delete(&acls);
    return retval;
}

static void uspa_subscription_notify(UNUSED const char* const sig_name,
                                     const amxc_var_t* const data,
                                     void* const priv) {
    SAH_TRACEZ_INFO(ME, "Notify controller");
    int retval = 1;
    uspa_sub_ref_t* sub_ref = (uspa_sub_ref_t*) priv;
    uspa_sub_t* sub = sub_ref->sub;
    uspi_con_t* con = NULL;
    amxc_var_t mtp_info;
    uspl_tx_t* usp_tx = NULL;
    bool retry = false;
    amxc_var_t data_copy;

    amxc_var_init(&mtp_info);
    amxc_var_init(&data_copy);
    // uspa_subscription_build_pbuf will modify variant, so we need a copy
    amxc_var_copy(&data_copy, data);

    con = uspa_imtp_con_from_contr(sub->contr_path);
    if(con == NULL) {
        SAH_TRACEZ_WARNING(ME, "No IMTP connection found for %s, not sending notification", sub->contr_path);
        goto exit;
    }

    retval = uspa_subscription_filter_notification(&data_copy, con->eid, sub->contr_path);
    when_failed(retval, exit);

    retval = amxb_call(uspa_get_busctxt(), sub->contr_path, "GetMTPInfo", NULL, &mtp_info, 5);
    when_failed(retval, exit);

    retry = uspa_object_get_value(bool, sub->instance_path, "NotifRetry");

    do {
        retval = uspa_subscription_build_pbuf(&data_copy, sub_ref, &usp_tx, retry);
        if(retval < 0) {
            uspl_tx_delete(&usp_tx);
            goto exit;
        } else {
            uspa_imtp_send(con, usp_tx, GETI_ARG(&mtp_info, 0));

            if(retry) {
                uspa_sub_retry_new(sub, usp_tx);
            } else {
                uspl_tx_delete(&usp_tx);
            }
        }
    } while(retval == 1);

    if(sub->boot_notif) {
        amxd_path_t* param_path = uspa_subscription_build_param_path(sub, NULL);
        uspa_subscription_save_last_value(sub, param_path);
        amxd_path_delete(&param_path);
    }

exit:
    amxc_var_clean(&data_copy);
    amxc_var_clean(&mtp_info);
    return;
}

/**
   @brief
   This function checks the following things:
     - whether the ReferenceList only contains a single parameter path that isn't a search path.
     - whether the NotifType is ValueChange

   @param sub the uspa_sub_t struct that summarizes the subscription
   @param ref_list the ReferenceList parameter for the subscription
   @param notif_type the NotifType parameter for the subscription
   @return path for the parameter that will be monitored by the subscription or NULL if any of the
           above checks fail
 */
static amxd_path_t* uspa_subscription_boot_notif_validate(uspa_sub_t* sub,
                                                          const amxc_llist_t* ref_list,
                                                          const char* notif_type) {
    int retval = -1;
    const char* param = NULL;
    amxd_path_t* sub_path = NULL;

    when_true(amxc_llist_size(ref_list) > 1, exit);
    when_false(strcmp(notif_type, "ValueChange") == 0, exit);

    sub_path = uspa_subscription_build_param_path(sub, ref_list);
    when_null(sub_path, exit);

    param = amxd_path_get_param(sub_path);
    when_str_empty(param, exit);
    when_true(amxd_path_is_search_path(sub_path), exit);

    retval = 0;
exit:
    if(retval != 0) {
        amxd_path_delete(&sub_path);
    }
    return sub_path;
}

static void uspa_subscription_boot_notif_build(amxc_var_t* event_data,
                                               amxc_var_t* object,
                                               const char* param,
                                               const char* current_value) {
    const char* object_path = amxc_var_key(object);
    amxc_var_t* parameters = NULL;
    amxc_var_t* param_entry = NULL;

    amxc_var_set_type(event_data, AMXC_VAR_ID_HTABLE);

    SAH_TRACEZ_INFO(ME, "Building boot notification for %s%s = %s",
                    object_path, param, current_value);
    amxc_var_add_key(cstring_t, event_data, "notification", "dm:object-changed");
    amxc_var_add_key(cstring_t, event_data, "object", object_path);
    amxc_var_add_key(cstring_t, event_data, "path", object_path);
    parameters = amxc_var_add_key(amxc_htable_t, event_data, "parameters", NULL);
    param_entry = amxc_var_add_key(amxc_htable_t, parameters, param, NULL);
    amxc_var_add_key(cstring_t, param_entry, "to", current_value);
}

static void uspa_subscription_boot_notif_send(uspa_sub_t* sub,
                                              const char* last_value,
                                              amxd_path_t* param_path) {
    amxb_bus_ctx_t* bus_ctx = uspa_get_busctxt();
    amxc_var_t ret;
    amxc_var_t event_data;
    amxc_var_t* target_obj = NULL;
    const char* param = NULL;
    const char* current_value = NULL;
    amxc_llist_it_t* lit = amxc_llist_get_first(&sub->sub_ref_list); // ref_list was validated and contains only a single item
    uspa_sub_ref_t* sub_ref = amxc_llist_it_get_data(lit, uspa_sub_ref_t, lit);

    amxc_var_init(&ret);
    amxc_var_init(&event_data);

    when_str_empty_trace(last_value, exit, INFO, "No previous value known for boot notification for %s", sub->instance_path);

    amxb_get(bus_ctx, amxd_path_get(param_path, AMXD_OBJECT_TERMINATE), 0, &ret, 5);

    target_obj = GETP_ARG(&ret, "0.0");
    param = amxd_path_get_param(param_path);
    current_value = GET_CHAR(target_obj, param);
    when_str_empty(current_value, exit);

    SAH_TRACEZ_INFO(ME, "Last value of %s = %s and current value = %s", param, current_value, last_value);
    when_true(strcmp(current_value, last_value) == 0, exit);

    uspa_subscription_boot_notif_build(&event_data, target_obj, param, current_value);

    uspa_subscription_notify(NULL, &event_data, sub_ref);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&event_data);
}

static void uspa_subscription_handle_boot_notif(uspa_sub_t* sub,
                                                const amxc_llist_t* ref_list,
                                                const char* notif_type,
                                                const char* last_value) {
    amxd_path_t* param_path = uspa_subscription_boot_notif_validate(sub, ref_list, notif_type);

    when_null(param_path, exit);

    uspa_subscription_boot_notif_send(sub, last_value, param_path);

    sub->boot_notif = true;
    uspa_subscription_save_last_value(sub, param_path);

exit:
    amxd_path_delete(&param_path);
}

static void uspa_subscription_periodic_trigger(UNUSED amxp_timer_t* timer, void* priv) {
    SAH_TRACEZ_INFO(ME, "Sending periodic event");
    uspa_sub_ref_t* sub_ref = (uspa_sub_ref_t*) priv;
    amxc_var_t data;

    amxc_var_init(&data);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "path", "Device.LocalAgent.");
    amxc_var_add_key(cstring_t, &data, "notification", "Periodic!");
    uspa_subscription_notify(NULL, &data, sub_ref);

    amxc_var_clean(&data);
}

static void uspa_subscription_periodic(uspa_sub_ref_t* sub_ref) {
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    uspa_sub_t* sub = sub_ref->sub;
    uint32_t notif_interval = 0;
    amxc_ts_t* notif_time = NULL;
    amxc_ts_t current_time;
    int compare = 0;
    uint64_t time_diff = 0;
    uint32_t first_timeout = 0;
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxb_get(ctx, sub->contr_path, 0, &ret, 5);
    notif_interval = GETP_UINT32(&ret, "0.0.PeriodicNotifInterval");
    notif_time = amxc_var_dyncast(amxc_ts_t, GETP_ARG(&ret, "0.0.PeriodicNotifTime"));

    amxc_ts_now(&current_time);
    compare = amxc_ts_compare(&current_time, notif_time);
    if(compare == 0) {        // 0 => invalid
        SAH_TRACEZ_ERROR(ME, "Failed to create Periodic! subscription");
        goto exit;
    } else if(compare == 1) { // 1 => tsp 1 after tsp 2
        time_diff = current_time.sec - notif_time->sec;
        first_timeout = notif_interval - (time_diff % notif_interval);
    } else {
        time_diff = notif_time->sec - current_time.sec;
        first_timeout = time_diff % notif_interval;
    }
    if(sub->periodic_timer == NULL) {
        amxp_timer_new(&(sub->periodic_timer), uspa_subscription_periodic_trigger, sub_ref);
    }
    amxp_timer_set_interval(sub->periodic_timer, 1000 * notif_interval);
    amxp_timer_start(sub->periodic_timer, 1000 * first_timeout);
    SAH_TRACEZ_INFO(ME, "Created periodic subscription with first timeout in %d seconds", first_timeout);

exit:
    free(notif_time);
    amxc_var_clean(&ret);
}

static int uspa_subscription_subscribe_to_path(amxd_path_t* path,
                                               const char* notif_type,
                                               uspa_sub_ref_t* sub_ref) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    char* expr = NULL;

    expr = uspa_subscription_expression(path, notif_type);
    SAH_TRACEZ_INFO(ME, "Creating subscription for %s with expression %s", amxd_path_get(path, AMXD_OBJECT_TERMINATE), expr);

    retval = amxb_subscribe(ctx, amxd_path_get(path, AMXD_OBJECT_TERMINATE), expr, uspa_subscription_notify, sub_ref);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to create subscription");
    }

    free(expr);
    return retval;
}

static int uspa_subscription_handle_ref(const char* ref,
                                        const char* notif_type,
                                        uspa_sub_t* sub,
                                        amxc_var_t* forwarding_list) {
    int retval = 0;
    amxd_path_t path;
    const char* last = NULL;
    uspa_sub_ref_t* sub_ref = uspa_sub_ref_new(sub, ref);

    amxd_path_init(&path, ref);

    when_null(sub_ref, exit);
    last = amxd_path_get_param(&path);

    if(!uspa_discovery_object_is_local(ref)) {
        amxc_var_add(cstring_t, forwarding_list, ref);
        goto exit;
    }

    if(strcmp("OperationComplete", notif_type) == 0) {
        subscribe_oper_complete(&path, sub);
    } else if((last != NULL) && (strcmp(last, "Periodic!") == 0) && (strcmp(notif_type, "Event") == 0)) {
        uspa_subscription_periodic(sub_ref);
    } else {
        uspa_subscription_subscribe_to_path(&path, notif_type, sub_ref);
    }

exit:
    amxd_path_clean(&path);
    return retval;
}

static int uspa_subscription_create(amxc_var_t* params, const char* instance_path) {
    int retval = -1;
    amxc_var_t* refs = GET_ARG(params, "ReferenceList");
    bool enable = GET_BOOL(params, "Enable");
    const char* notif_type = GET_CHAR(params, "NotifType");
    const char* controller = GET_CHAR(params, "Recipient");
    const char* id = GET_CHAR(params, "ID");
    const amxc_llist_t* ref_list = NULL;
    uspa_sub_t* sub = NULL;
    amxc_var_t forwarding_list;

    amxc_var_init(&forwarding_list);
    amxc_var_set_type(&forwarding_list, AMXC_VAR_ID_LIST);

    amxc_var_cast(refs, AMXC_VAR_ID_LIST);
    ref_list = amxc_var_constcast(amxc_llist_t, refs);

    when_false(enable, exit);
    when_true(amxc_llist_is_empty(ref_list), exit);
    when_str_empty(notif_type, exit);

    retval = uspa_sub_new(&sub, instance_path, controller, id, notif_type);
    when_failed(retval, exit);
    amxc_llist_append(&sub_list, &sub->lit);

    amxc_llist_for_each(it, ref_list) {
        const char* ref = amxc_var_constcast(cstring_t, amxc_var_from_llist_it(it));
        uspa_subscription_handle_ref(ref, notif_type, sub, &forwarding_list);
    }

    uspa_subscription_handle_boot_notif(sub, ref_list, notif_type, GET_CHAR(params, "LastValue"));

    uspa_mtp_imtp_subscription_forward(&forwarding_list, params);

exit:
    amxc_var_clean(&forwarding_list);
    return retval;
}

static void uspa_subscription_enabled(const char* sub_path) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    amxc_var_t ret;
    amxc_var_t* subscription = NULL;
    const char* instance_path = NULL;

    amxc_var_init(&ret);

    retval = amxb_get(ctx, sub_path, 0, &ret, 5);
    when_failed(retval, exit);

    subscription = GETP_ARG(&ret, "0.0");
    instance_path = amxc_var_key(subscription);

    retval = uspa_subscription_create(subscription, instance_path);

exit:
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to create subscription for %s", sub_path);
    }
    amxc_var_clean(&ret);
}

static int uspa_subscription_unsubscribe(amxd_path_t* path, uspa_sub_ref_t* sub_ref) {
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Removing subscription for %s", amxd_path_get(path, AMXD_OBJECT_TERMINATE));
    retval = amxb_unsubscribe(ctx, amxd_path_get(path, AMXD_OBJECT_TERMINATE),
                              uspa_subscription_notify, sub_ref);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to unsubscribe to %s",
                           amxd_path_get(path, AMXD_OBJECT_TERMINATE));
    }

    return retval;
}

static void uspa_subscription_remove_ref(const char* ref,
                                         const char* notif_type,
                                         uspa_sub_t* sub,
                                         amxc_var_t* forwarding_list) {
    amxd_path_t path;

    amxd_path_init(&path, ref);

    if(!uspa_discovery_object_is_local(ref)) {
        amxc_var_add(cstring_t, forwarding_list, ref);
        goto exit;
    }

    if(strcmp("OperationComplete", notif_type) == 0) {
        amxp_slot_disconnect_with_priv(NULL, uspa_operation_complete_resp, sub);
    } else {
        uspa_sub_ref_t* sub_ref = uspa_sub_ref_find(sub, ref);
        uspa_subscription_unsubscribe(&path, sub_ref);
    }

exit:
    amxd_path_clean(&path);
}

static void uspa_subscription_remove(const amxc_llist_t* ref_list,
                                     const char* notif_type,
                                     uspa_sub_t* sub) {
    amxc_var_t forwarding_list;

    amxc_var_init(&forwarding_list);
    amxc_var_set_type(&forwarding_list, AMXC_VAR_ID_LIST);

    amxc_llist_for_each(it, ref_list) {
        const char* ref = amxc_var_constcast(cstring_t, amxc_var_from_llist_it(it));
        uspa_subscription_remove_ref(ref, notif_type, sub, &forwarding_list);
    }

    uspa_mtp_imtp_subscription_remove(&forwarding_list, sub);

    amxc_llist_it_take(&sub->lit);
    uspa_sub_delete(&sub);
    amxc_var_clean(&forwarding_list);
}

static void uspa_subscription_disabled(const char* sub_path) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    uspa_sub_t* sub = uspa_sub_find_from_instance(sub_path);
    amxc_var_t ret;
    amxc_var_t* refs = NULL;
    const amxc_llist_t* ref_list = NULL;
    const char* notif_type = NULL;

    amxc_var_init(&ret);

    when_null(sub, exit);

    retval = amxb_get(ctx, sub_path, 0, &ret, 5);
    when_failed(retval, exit);

    refs = GETP_ARG(&ret, "0.0.ReferenceList");
    when_null(refs, exit);

    notif_type = GETP_CHAR(&ret, "0.0.NotifType");
    when_str_empty(notif_type, exit);

    amxc_var_cast(refs, AMXC_VAR_ID_LIST);
    ref_list = amxc_var_constcast(amxc_llist_t, refs);

    uspa_subscription_remove(ref_list, notif_type, sub);

exit:
    amxc_var_clean(&ret);
}

static void uspa_subscription_toggled(UNUSED const char* const sig_name,
                                      const amxc_var_t* const data,
                                      UNUSED void* const priv) {
    const char* sub_path = GET_CHAR(data, "path");
    bool enabled = GETP_BOOL(data, "parameters.Enable.to");
    SAH_TRACEZ_INFO(ME, "Enable of %s is %d", sub_path, enabled);
    if(enabled) {
        uspa_subscription_enabled(sub_path);
    } else {
        uspa_subscription_disabled(sub_path);
    }
}

static void uspa_subscription_added(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    const char* template = GET_CHAR(data, "path");
    uint32_t index = GET_UINT32(data, "index");
    amxc_var_t* params = GET_ARG(data, "parameters");
    amxc_string_t subs_instance;

    amxc_string_init(&subs_instance, 0);
    amxc_string_setf(&subs_instance, "%s%d.", template, index);
    uspa_subscription_create(params, amxc_string_get(&subs_instance, 0));

    amxc_string_clean(&subs_instance);
}

static void uspa_subscription_deleted(UNUSED const char* const sig_name,
                                      const amxc_var_t* const data,
                                      UNUSED void* const priv) {
    const char* template = GET_CHAR(data, "path");
    const char* notif_type = NULL;
    uint32_t index = GET_UINT32(data, "index");
    uspa_sub_t* sub = NULL;
    amxc_var_t* refs = NULL;
    const amxc_llist_t* ref_list = NULL;
    amxc_string_t subs_instance;

    amxc_string_init(&subs_instance, 0);
    amxc_string_setf(&subs_instance, "%s%d.", template, index);

    SAH_TRACEZ_INFO(ME, "%s removed", amxc_string_get(&subs_instance, 0));
    sub = uspa_sub_find_from_instance(amxc_string_get(&subs_instance, 0));
    when_null(sub, exit);

    refs = GETP_ARG(data, "parameters.ReferenceList");
    when_null(refs, exit);

    amxc_var_cast(refs, AMXC_VAR_ID_LIST);
    ref_list = amxc_var_constcast(amxc_llist_t, refs);

    notif_type = GETP_CHAR(data, "parameters.NotifType");

    uspa_subscription_remove(ref_list, notif_type, sub);

exit:
    amxc_string_clean(&subs_instance);
}

int uspa_subscription_subscribe(amxb_bus_ctx_t* ctx) {
    int retval = -1;
    const char* path = "LocalAgent.Subscription.";
    const char* expr_changed = "(notification == 'dm:object-changed') && \
                                (contains('parameters.Enable'))";
    const char* expr_added = "(notification == 'dm:instance-added') && (parameters.Enable == true)";
    const char* expr_removed = "(notification == 'dm:instance-removed')";

    retval = amxb_subscribe(ctx, path, expr_changed, uspa_subscription_toggled, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s changed events", path);
        goto exit;
    }

    retval = amxb_subscribe(ctx, path, expr_added, uspa_subscription_added, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s added events", path);
        goto exit;
    }

    retval = amxb_subscribe(ctx, path, expr_removed, uspa_subscription_deleted, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s added events", path);
        goto exit;
    }

exit:
    return retval;
}

int uspa_subscription_init(amxb_bus_ctx_t* ctx) {
    int retval = -1;
    amxc_var_t ret;
    amxc_var_t* subscriptions = NULL;

    amxc_var_init(&ret);
    amxc_llist_init(&sub_list);

    retval = amxb_get(ctx, "LocalAgent.Subscription.*.", 0, &ret, 5);
    when_failed(retval, exit);

    subscriptions = GETI_ARG(&ret, 0);
    amxc_var_for_each(sub, subscriptions) {
        const char* instance_path = amxc_var_key(sub);
        uspa_subscription_create(sub, instance_path);
    }

    amxp_timer_new(&boot_event_timer, uspa_subs_boot_params_reset, NULL);

exit:
    amxc_var_clean(&ret);
    return retval;
}

void uspa_subscription_clean(void) {
    amxp_timer_delete(&boot_event_timer);
    amxc_llist_clean(&sub_list, uspa_sub_list_it_delete);
}

uspa_sub_t* uspa_sub_find_from_id(const char* id) {
    uspa_sub_t* sub = NULL;

    amxc_llist_for_each(it, &sub_list) {
        uspa_sub_t* tmp = amxc_container_of(it, uspa_sub_t, lit);
        if(strcmp(tmp->id, id) == 0) {
            sub = tmp;
            break;
        }
    }

    return sub;
}

uspa_sub_retry_t* uspa_sub_retry_find_from_msg_id(uspa_sub_t* sub, const char* msg_id) {
    uspa_sub_retry_t* sub_retry = NULL;

    amxc_llist_for_each(it, &sub->retry_list) {
        uspa_sub_retry_t* tmp = amxc_container_of(it, uspa_sub_retry_t, lit);
        if(tmp->usp_tx == NULL) {
            continue;
        }
        if(strcmp(tmp->usp_tx->msg_id, msg_id) == 0) {
            sub_retry = tmp;
            break;
        }
    }

    return sub_retry;
}

void uspa_sub_retry_delete(uspa_sub_retry_t** sub_retry) {
    when_null(sub_retry, exit);
    when_null(*sub_retry, exit);

    amxc_llist_it_take(&(*sub_retry)->lit); // Can this cause issues?
    amxp_timer_delete(&(*sub_retry)->retry_timer);
    amxp_timer_delete(&(*sub_retry)->expiration_timer);
    uspl_tx_delete(&(*sub_retry)->usp_tx);
    free(*sub_retry);

exit:
    return;
}

int uspa_sub_notify_controller(const char* sub_id, amxc_var_t* notification) {
    SAH_TRACEZ_INFO(ME, "Notify controller");
    int retval = -1;
    uspa_sub_t* sub = uspa_sub_find_from_id(sub_id);
    uspi_con_t* con = NULL;
    amxc_var_t mtp_info;
    uspl_tx_t* usp_tx = NULL;
    bool retry = false;

    amxc_var_init(&mtp_info);

    when_null(sub, exit);

    con = uspa_imtp_con_from_contr(sub->contr_path);
    if(con == NULL) {
        SAH_TRACEZ_WARNING(ME, "No IMTP connection found for %s, not sending notification", sub->contr_path);
        goto exit;
    }

    retval = amxb_call(uspa_get_busctxt(), sub->contr_path, "GetMTPInfo", NULL, &mtp_info, 5);
    when_failed(retval, exit);

    usp_tx = uspa_sub_build_tx(sub, notification);
    when_null(usp_tx, exit);

    retval = uspa_imtp_send(con, usp_tx, GETI_ARG(&mtp_info, 0));
    if(retval < 0) {
        goto exit;
    }

    // TODO check NotifExpiration > 0
    retry = uspa_object_get_value(bool, sub->instance_path, "NotifRetry");
    if(retry) {
        uspa_sub_retry_new(sub, usp_tx);
    } else {
        uspl_tx_delete(&usp_tx);
    }

exit:
    amxc_var_clean(&mtp_info);
    return retval;
}

bool uspa_subscription_validate(amxc_var_t* acls, const char* ref_list, const char* notif_type) {
    bool allowed = true;
    amxc_var_t ref_list_var;
    uint32_t flag = 0;

    amxc_var_init(&ref_list_var);
    amxc_var_set(cstring_t, &ref_list_var, ref_list);
    amxc_var_cast(&ref_list_var, AMXC_VAR_ID_LIST);

    if(strcmp(notif_type, "ValueChange") == 0) {
        flag = AMXA_PERMIT_SUBS_VAL_CHANGE;
    } else if(strcmp(notif_type, "ObjectCreation") == 0) {
        flag = AMXA_PERMIT_SUBS_OBJ_ADD;
    } else if(strcmp(notif_type, "ObjectDeletion") == 0) {
        flag = AMXA_PERMIT_SUBS_OBJ_DEL;
    } else if((strcmp(notif_type, "OperationComplete") == 0) ||
              (strcmp(notif_type, "Event") == 0)) {
        flag = AMXA_PERMIT_SUBS_EVT_OPER_COMP;
    } else {
        goto exit;
    }

    // Check if creating subscription is allowed for fixed path (path could be a search path
    // pointing to instances that don't exist yet) and filter notifications later if needed.
    amxc_var_for_each(ref, &ref_list_var) {
        char* fixed = NULL;
        amxd_path_t path;

        amxd_path_init(&path, amxc_var_constcast(cstring_t, ref));
        fixed = amxd_path_get_fixed_part(&path, false);
        allowed = amxa_is_subs_allowed(acls, fixed, flag);
        amxd_path_clean(&path);
        free(fixed);
        when_false(allowed, exit);
    }

exit:
    amxc_var_clean(&ref_list_var);
    return allowed;
}
