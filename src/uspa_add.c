/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <usp/uspl.h>

#include "uspa.h"
#include "uspa_msghandler.h"
#include "uspa_utils.h"
#include "uspa_controller.h"
#include "uspa_subscription.h"

static bool uspa_validate_params(amxb_bus_ctx_t* ctx,
                                 amxc_var_t* acls,
                                 const char* obj_path,
                                 amxc_var_t* params) {
    bool allowed = false;
    bool req_params_allowed = true;
    amxc_var_for_each(var, params) {
        const char* param = GET_CHAR(var, "param");
        allowed = amxa_is_set_allowed(ctx, acls, obj_path, param);
        if(!allowed) {
            SAH_TRACEZ_WARNING(ME, "Insufficient permissions to set %s%s", obj_path, param);
            bool required = GET_BOOL(var, "required");
            amxc_var_add_key(bool, var, "allowed", false);
            if(required) {
                SAH_TRACEZ_WARNING(ME, "Parameter is required, aborting add");
                req_params_allowed = false;
                goto exit;
            }
        } else {
            amxc_var_add_key(bool, var, "allowed", true);
        }
    }

exit:
    return req_params_allowed;
}

static bool uspa_validate_add_subscription(amxc_var_t* params, amxc_var_t* acls) {
    bool allowed = true;
    const char* ref_list = NULL;
    const char* notif_type = NULL;

    amxc_var_for_each(var, params) {
        const char* param = GET_CHAR(var, "param");
        if(strcmp(param, "ReferenceList") == 0) {
            ref_list = GET_CHAR(var, "value");
        } else if(strcmp(param, "NotifType") == 0) {
            notif_type = GET_CHAR(var, "value");
        }
    }

    // ReferenceList and NotifType could be empty on creation of subscription and set later
    when_str_empty(ref_list, exit);
    when_str_empty(notif_type, exit);

    allowed = uspa_subscription_validate(acls, ref_list, notif_type);

exit:
    return allowed;
}

static int uspa_validate_add(amxb_bus_ctx_t* ctx,
                             const char* obj_path,
                             amxc_var_t* params,
                             const char* acl_file,
                             bool check_subscription) {
    amxd_path_t path;
    amxc_var_t* acls = NULL;
    amxc_var_t resolved;
    char* fixed_part = NULL;
    int retval = -1;
    bool allowed = false;
    const amxc_htable_t* params_table = amxc_var_constcast(amxc_htable_t, GETI_ARG(params, 0));

    amxc_var_init(&resolved);
    amxd_path_init(&path, obj_path);

    acls = amxa_parse_files(acl_file);
    when_null(acls, exit);
    fixed_part = amxd_path_get_fixed_part(&path, false);
    amxa_resolve_search_paths(ctx, acls, fixed_part);

    if(amxd_path_is_search_path(&path)) {
        amxb_resolve(ctx, &path, &resolved);
        amxc_var_for_each(var, &resolved) {
            const char* resolved_path = amxc_var_constcast(cstring_t, var);
            allowed = amxa_is_add_allowed(ctx, acls, resolved_path);
            when_false_do(allowed, exit, SAH_TRACEZ_NOTICE(ME, "USP add not allowed for acl file '%s'", acl_file));
            if((params_table != NULL) && !amxc_htable_is_empty(params_table)) {
                allowed = uspa_validate_params(ctx, acls, resolved_path, params);
                when_false_do(allowed, exit, SAH_TRACEZ_NOTICE(ME, "USP add not allowed for acl file '%s", acl_file));
            }
        }
    } else {
        allowed = amxa_is_add_allowed(ctx, acls, obj_path);
        when_false_do(allowed, exit, SAH_TRACEZ_NOTICE(ME, "USP add not allowed for acl file '%s'", acl_file));

        if((params_table != NULL) && !amxc_htable_is_empty(params_table)) {
            allowed = uspa_validate_params(ctx, acls, obj_path, params);
            when_false_do(allowed, exit, SAH_TRACEZ_NOTICE(ME, "USP add not allowed for acl file '%s", acl_file));
        }

        if(check_subscription) {
            allowed = uspa_validate_add_subscription(params, acls);
            when_false_do(allowed, exit, SAH_TRACEZ_NOTICE(ME, "Creating subscription not allowed for acl file '%s", acl_file));
        }
    }

    retval = 0;
exit:
    free(fixed_part);
    amxd_path_clean(&path);
    amxc_var_delete(&acls);
    amxc_var_clean(&resolved);

    return retval;
}

static void uspa_add_convert_output_table(amxc_llist_t* resp_list,
                                          amxc_var_t* add_resp,
                                          const char* path,
                                          int retval) {
    amxc_var_t* result = NULL;
    amxc_var_t* resp_list_entry = NULL;

    amxc_var_new(&resp_list_entry);

    // TODO: make distinction between different error codes
    if((retval != 0) || (amxc_var_type_of(add_resp) == AMXC_VAR_ID_NULL)) {
        SAH_TRACEZ_WARNING(ME, "amxb_add failed with status: [%d] for path: [%s]", retval, path);
        amxc_var_set_type(resp_list_entry, AMXC_VAR_ID_HTABLE);
        result = amxc_var_add_key(amxc_htable_t, resp_list_entry, "result", NULL);
        amxc_var_add_key(cstring_t, result, "requested_path", path);
        amxc_var_add_key(cstring_t, result, "err_msg", "Add request failed");
        amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_INTERNAL_ERROR);
    } else {
        result = amxc_var_add_key(amxc_htable_t, add_resp, "result", NULL);
        amxc_var_add_key(cstring_t, result, "requested_path", path);
        amxc_var_add_key(uint32_t, result, "err_code", 0);
        amxc_var_move(resp_list_entry, add_resp);
    }
    amxc_llist_append(resp_list, &resp_list_entry->lit);
}

static void uspa_add_convert_output(amxc_llist_t* resp_list,
                                    amxc_var_t* add_resp,
                                    const char* path,
                                    int retval) {
    amxc_var_t* first = GETI_ARG(add_resp, 0);
    if(amxc_var_type_of(first) == AMXC_VAR_ID_HTABLE) {
        uspa_add_convert_output_table(resp_list, first, path, retval);
    } else if(amxc_var_type_of(first) == AMXC_VAR_ID_LIST) {
        amxc_var_for_each(entry, first) {
            uspa_add_convert_output_table(resp_list, entry, path, retval);
        }
    } else {
        uspa_add_convert_output_table(resp_list, add_resp, path, retval);
    }
}

static void uspa_add_controller(const char* from_id, amxc_var_t* values, const char* param_name) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    amxc_string_t contr_path;
    amxc_var_t ret;
    amxc_var_t* contr = NULL;

    amxc_var_init(&ret);
    amxc_string_init(&contr_path, 0);
    amxc_string_setf(&contr_path, "LocalAgent.Controller.[EndpointID == '%s'].", from_id);

    retval = amxb_get(ctx, amxc_string_get(&contr_path, 0), 0, &ret, 5);
    contr = GETP_ARG(&ret, "0.0");

    if((retval != 0) || (contr == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Failed to find controller with EID: %s", from_id);
        goto exit;
    }

    amxc_var_add_key(cstring_t, values, param_name, amxc_var_key(contr));

exit:
    amxc_string_clean(&contr_path);
    amxc_var_clean(&ret);
}

static int uspa_invoke_add(amxb_bus_ctx_t* ctx,
                           amxc_var_t* entry,
                           amxc_llist_t* resp_list,
                           const char* path,
                           const char* from_id,
                           const char* acl_file,
                           uint32_t access) {
    SAH_TRACEZ_INFO(ME, "Invoke add on path: [%s]", path);
    int retval = -1;
    amxc_var_t* params = GET_ARG(entry, "parameters");
    bool check_subscription = false;
    amxc_var_t values;
    amxc_var_t rv;

    amxc_var_init(&rv);
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);

    if(uspa_utils_matches("^(Device\\.)?LocalAgent\\.Subscription\\.$", path)) {
        uspa_add_controller(from_id, &values, "Recipient");
        check_subscription = true;
    }
    if(uspa_utils_matches("^(Device\\.)?BulkData\\.Profile\\.$", path)) {
        uspa_add_controller(from_id, &values, "Controller");
    }

    amxb_set_access(ctx, access);
    retval = uspa_validate_add(ctx, path, params, acl_file, check_subscription);
    when_failed(retval, exit);

    amxc_var_for_each(item, params) {
        if(GET_BOOL(item, "allowed")) {
            amxc_var_add_key(cstring_t, &values, GET_CHAR(item, "param"), GET_CHAR(item, "value"));
        }
    }

    retval = amxb_add(ctx, path, 0, NULL, &values, &rv, 5);

exit:
    amxb_set_access(ctx, AMXB_PROTECTED);
    uspa_add_convert_output(resp_list, &rv, path, retval);

    amxc_var_clean(&values);
    amxc_var_clean(&rv);
    return retval;
}

int uspa_handle_add(uspl_rx_t* usp_rx,
                    uspl_tx_t** usp_tx,
                    const char* acl_file) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type Add");
    int retval = -1;
    amxc_var_t request;
    amxc_llist_t resp_list;
    amxc_var_t* requests = NULL;
    const char* from_id = NULL;
    uint32_t access = AMXB_PUBLIC;

    amxc_var_init(&request);
    amxc_llist_init(&resp_list);

    when_null(usp_rx, exit);
    when_null(usp_tx, exit);

    retval = uspl_add_extract(usp_rx, &request);
    if(retval != 0) {
        retval = uspa_msghandler_build_error(usp_rx, usp_tx, retval);
        goto exit;
    }

    from_id = uspl_msghandler_from_id(usp_rx);
    access = uspa_controller_get_access(from_id);
    requests = amxc_var_get_key(&request, "requests", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_for_each(item, requests) {
        const char* object_path = GET_CHAR(item, "object_path");
        amxb_bus_ctx_t* ctx = uspa_discovery_get_ctx(object_path);
        retval = uspa_invoke_add(ctx, item, &resp_list, object_path, from_id, acl_file, access);
        if(retval != 0) {
            break;
        }
    }
    if(retval == 0) {
        retval = uspa_msghandler_build_reply(usp_rx, &resp_list, uspl_add_resp_new, usp_tx);
    } else {
        retval = uspa_msghandler_build_error(usp_rx, usp_tx, retval);
    }

exit:
    amxc_var_clean(&request);
    amxc_llist_clean(&resp_list, variant_list_it_free);

    return retval;
}
