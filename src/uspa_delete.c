/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <usp/uspl.h>

#include "uspa_msghandler.h"
#include "uspa_prefix_check.h"
#include "uspa_role.h"
#include "uspa_controller.h"

static int uspa_validate_del(amxb_bus_ctx_t* ctx,
                             const char* obj_path,
                             const char* acl_file) {
    amxd_path_t path;
    amxc_var_t* acls = NULL;
    amxc_var_t resolved;
    char* fixed_part = NULL;
    int retval = -1;
    bool allowed = false;
    amxc_string_t resolved_path;

    amxc_string_init(&resolved_path, 0);
    amxc_var_init(&resolved);
    amxd_path_init(&path, obj_path);

    acls = amxa_parse_files(acl_file);
    when_null(acls, exit);
    fixed_part = amxd_path_get_fixed_part(&path, false);
    amxa_resolve_search_paths(ctx, acls, fixed_part);

    if(amxd_path_is_search_path(&path)) {
        amxb_resolve(ctx, &path, &resolved);
        amxc_var_for_each(var, &resolved) {
            amxc_string_setf(&resolved_path, "%s", amxc_var_constcast(cstring_t, var));
            allowed = amxa_is_del_allowed(ctx, acls, amxc_string_get(&resolved_path, 0));
            when_false_do(allowed, exit, SAH_TRACEZ_NOTICE(ME, "USP delete not allowed for acl_file '%s'", acl_file));
        }
    } else {
        allowed = amxa_is_del_allowed(ctx, acls, obj_path);
        when_false_do(allowed, exit, SAH_TRACEZ_NOTICE(ME, "USP delete not allowed for acl_file '%s'", acl_file));
    }

    retval = 0;
exit:
    free(fixed_part);
    amxd_path_clean(&path);
    amxc_var_delete(&acls);
    amxc_var_clean(&resolved);
    amxc_string_clean(&resolved_path);

    return retval;
}

static int uspa_invoke_delete(amxb_bus_ctx_t* ctx,
                              amxc_llist_t* resp_list,
                              const char* path,
                              const char* acl_file,
                              uint32_t access) {
    SAH_TRACEZ_INFO(ME, "Invoke delete on path: [%s]", path);
    int retval = -1;
    amxc_var_t* affected = NULL;
    amxc_var_t* result = NULL;
    amxc_var_t* reply = NULL;
    amxc_string_t path_string;

    amxc_string_init(&path_string, 0);
    amxc_string_setf(&path_string, "%s", path);
    amxc_var_new(&reply);
    amxc_var_set_type(reply, AMXC_VAR_ID_HTABLE);
    affected = amxc_var_add_key(amxc_llist_t, reply, "affected_paths", NULL);
    amxc_var_add_key(amxc_llist_t, reply, "unaffected_paths", NULL);
    result = amxc_var_add_key(amxc_htable_t, reply, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", path);

    amxb_set_access(ctx, access);
    retval = uspa_validate_del(ctx, amxc_string_get(&path_string, 0), acl_file);
    when_failed(retval, exit);

    retval = amxb_del(ctx, amxc_string_get(&path_string, 0), 0, NULL, affected, 5);

    if(retval == 0) {
        amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_OK);
    } else {
        SAH_TRACEZ_WARNING(ME, "amxb_delete failed with status: [%d] for path: [%s]",
                           retval, path);
        amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_GENERAL_FAILURE);
        amxc_var_add_key(cstring_t, result, "err_msg", "delete failed");
    }

exit:
    amxb_set_access(ctx, AMXB_PROTECTED);
    amxc_llist_append(resp_list, &reply->lit);
    amxc_string_clean(&path_string);
    return retval;
}

int uspa_handle_delete(uspl_rx_t* usp_rx,
                       uspl_tx_t** usp_tx,
                       const char* acl_file) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type Delete");
    int retval = -1;
    amxc_var_t result;
    const amxc_llist_t* req_list = NULL;
    amxc_llist_t resp_list;
    amxc_var_t* requests = NULL;
    uint32_t access = AMXB_PUBLIC;

    amxc_var_init(&result);
    amxc_llist_init(&resp_list);

    when_null(usp_rx, exit);
    when_null(usp_tx, exit);

    when_false(uspl_delete_extract(usp_rx, &result) == 0, exit);

    access = uspa_controller_get_access(uspl_msghandler_from_id(usp_rx));
    requests = amxc_var_get_key(&result, "requests", AMXC_VAR_FLAG_DEFAULT);
    req_list = amxc_var_constcast(amxc_llist_t, requests);
    amxc_llist_iterate(it, req_list) {
        const char* path = amxc_var_constcast(cstring_t, amxc_var_from_llist_it(it));
        amxb_bus_ctx_t* ctx = uspa_discovery_get_ctx(path);
        retval = uspa_invoke_delete(ctx, &resp_list, path, acl_file, access);
        if(retval != 0) {
            retval = uspa_msghandler_build_error(usp_rx, usp_tx, retval);
            goto exit;
        }
    }
    retval = uspa_msghandler_build_reply(usp_rx, &resp_list, uspl_delete_resp_new, usp_tx);

exit:
    amxc_var_clean(&result);
    amxc_llist_clean(&resp_list, variant_list_it_free);

    return retval;
}
