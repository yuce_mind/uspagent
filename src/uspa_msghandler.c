/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <usp/uspl.h>
#include <uspi/uspi_connection.h>

#include "uspa.h"
#include "uspa_agent.h"
#include "uspa_msghandler.h"
#include "uspa_imtp.h"
#include "uspa_role.h"
#include "uspa_controller_mtp_mqtt.h"

typedef int (* uspa_handle_fn_t) (uspl_rx_t* usp_rx,
                                  uspl_tx_t** usp_tx,
                                  const char* acl_file);

typedef struct _uspa_msg_t {
    uint32_t id;
    uspa_handle_fn_t handler;
} uspa_msg_t;

static uspa_msg_t uspa_fn[] = {
    { USP__HEADER__MSG_TYPE__ERROR, NULL },
    { USP__HEADER__MSG_TYPE__GET, uspa_handle_get },
    { USP__HEADER__MSG_TYPE__GET_RESP, NULL },
    { USP__HEADER__MSG_TYPE__NOTIFY, uspa_handle_notify },
    { USP__HEADER__MSG_TYPE__SET, uspa_handle_set },
    { USP__HEADER__MSG_TYPE__SET_RESP, NULL },
    { USP__HEADER__MSG_TYPE__OPERATE, uspa_handle_operate },
    { USP__HEADER__MSG_TYPE__OPERATE_RESP, NULL },
    { USP__HEADER__MSG_TYPE__ADD, uspa_handle_add },
    { USP__HEADER__MSG_TYPE__ADD_RESP, NULL },
    { USP__HEADER__MSG_TYPE__DELETE, uspa_handle_delete },
    { USP__HEADER__MSG_TYPE__DELETE_RESP, NULL },
    { USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM, uspa_handle_get_supported_dm },
    { USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM_RESP, NULL },
    { USP__HEADER__MSG_TYPE__GET_INSTANCES, uspa_handle_get_instances },
    { USP__HEADER__MSG_TYPE__GET_INSTANCES_RESP, NULL },
    { USP__HEADER__MSG_TYPE__NOTIFY_RESP, uspa_handle_notify_resp },
    { USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO, uspa_handle_get_supported_proto },
    { USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO_RESP, NULL },
    { USP__HEADER__MSG_TYPE__REGISTER, uspa_handle_register },
    { USP__HEADER__MSG_TYPE__REGISTER_RESP, NULL },
};

void uspa_msghandler_send_usp_record(uspi_con_t* con,
                                     uspl_tx_t* usp_tx,
                                     amxc_var_t* mtp_info) {
    const char* protocol = GET_CHAR(mtp_info, "protocol");

    when_str_empty(protocol, exit);
    if(strcmp(protocol, "MQTT") == 0) {
        const char* topic = GET_CHAR(mtp_info, "topic");
        if((topic == 0) || (*topic == 0)) {
            SAH_TRACEZ_INFO(ME, "Fetch default controller MQTT topic");
            char* default_topic = uspa_controller_mtp_mqtt_get_topic(usp_tx->rendpoint_id);
            amxc_var_add_key(cstring_t, mtp_info, "topic", default_topic);
            free(default_topic);
        }
        uspa_imtp_send(con, usp_tx, mtp_info);
    } else if(strcmp(protocol, "IMTP") == 0) {
        uspa_imtp_send(con, usp_tx, mtp_info);
    }
exit:
    return;
}

void uspa_msghandler_process_binary_record(unsigned char* pbuf,
                                           int pbuf_len,
                                           uspi_con_t* con,
                                           amxc_var_t* mtp_info) {
    uspl_rx_t* usp_rx = NULL;
    uspl_tx_t* usp_tx = NULL;
    int retval = -1;
    uint32_t msg_type = 0;
    char* from_id = NULL;
    char* to_id = NULL;
    amxc_string_t* acl_file = NULL;

    when_null(pbuf, exit);

    usp_rx = uspl_msghandler_unpack_protobuf(pbuf, pbuf_len);
    when_null(usp_rx, exit);

    to_id = uspl_msghandler_to_id(usp_rx);
    if(!uspa_agent_check_eid(to_id)) {
        SAH_TRACEZ_WARNING(ME, "Received USP message with invalid to_id '%s'; ignoring", to_id);
        goto exit;
    }

    from_id = uspl_msghandler_from_id(usp_rx);
    // TODO validate whether the provided EndpointID is actually from the contacting controller
    // because we use this for checking ACLs (controller could be lying)
    acl_file = uspa_role_get_acl_file(from_id);
    if(acl_file == NULL) {
        SAH_TRACEZ_WARNING(ME, "No acls found for controller: %s", from_id);
        uspa_msghandler_build_error(usp_rx, &usp_tx, USP_ERR_PERMISSION_DENIED);
        goto exit;
    }

    msg_type = uspl_msghandler_msg_type(usp_rx);

    if(msg_type < (sizeof(uspa_fn) / sizeof(uspa_fn[0]))) {
        if(uspa_fn[msg_type].handler != NULL) {
            retval = uspa_fn[msg_type].handler(usp_rx, &usp_tx, amxc_string_get(acl_file, 0));
        } else {
            SAH_TRACEZ_WARNING(ME, "Unsupported USP record type[%d]", msg_type);
            retval = uspa_msghandler_build_error(usp_rx, &usp_tx, USP_ERR_INVALID_ARGUMENTS);
        }
    }

    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Handling of USP record type[%d] failed", msg_type);
        goto exit;
    }

exit:
    amxc_string_delete(&acl_file);
    if(usp_tx != NULL) {
        uspa_msghandler_send_usp_record(con, usp_tx, mtp_info);
        uspl_tx_delete(&usp_tx);
    }
    uspl_rx_delete(&usp_rx);
}

int uspa_msghandler_build_reply_basic(uspl_rx_t* usp_rx,
                                      uspl_tx_t** usp_tx) {
    int retval = -1;
    char* from_id = NULL;
    char* to_id = NULL;

    when_null(usp_rx, exit);
    when_null(usp_tx, exit);

    from_id = uspl_msghandler_from_id(usp_rx);
    when_null(from_id, exit);
    to_id = uspl_msghandler_to_id(usp_rx);
    when_null(to_id, exit);
    retval = uspl_tx_new(usp_tx, to_id, from_id);
    when_failed(retval, exit);

    retval = 0;
exit:
    return retval;
}

int uspa_msghandler_build_reply(uspl_rx_t* usp_rx,
                                amxc_llist_t* resp_list,
                                fn_reply_new fn,
                                uspl_tx_t** usp_tx) {
    int retval = -1;
    char* msg_id = NULL;

    when_null(resp_list, exit);

    msg_id = uspl_msghandler_msg_id(usp_rx);
    when_null(msg_id, exit);
    retval = uspa_msghandler_build_reply_basic(usp_rx, usp_tx);
    when_failed(retval, exit);
    retval = fn(*usp_tx, resp_list, msg_id);
    when_failed(retval, exit);
    retval = 0;

exit:
    return retval;
}

// Needs review; can maybe be replaced with code from uspa_error.c
int uspa_msghandler_build_error(uspl_rx_t* usp_rx,
                                uspl_tx_t** usp_tx,
                                int err_code) {
    int retval = -1;
    char* msg_id = NULL;
    char* from_id = NULL;
    char* to_id = NULL;
    amxc_var_t error;

    // TODO provide parameter specific error information in USP Error message (when relevant)
    amxc_var_init(&error);
    amxc_var_set_type(&error, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &error, "err_code", err_code);
    amxc_var_add_key(cstring_t, &error, "err_msg", uspl_error_code_to_str(err_code));

    when_null(usp_rx, exit);
    when_null(usp_tx, exit);

    msg_id = uspl_msghandler_msg_id(usp_rx);
    when_null(msg_id, exit);
    from_id = uspl_msghandler_from_id(usp_rx);
    when_null(from_id, exit);
    to_id = uspl_msghandler_to_id(usp_rx);
    when_null(to_id, exit);

    retval = uspl_tx_new(usp_tx, to_id, from_id);
    when_failed(retval, exit);

    retval = uspl_error_resp_new(*usp_tx, &error, msg_id);
    when_failed(retval, exit);

    retval = 0;
exit:
    amxc_var_clean(&error);
    return retval;
}
