/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "uspa_discovery.h"
#include "uspa_prefix_check.h"
#include "uspa_mtp_imtp.h"

static char* uspa_discovery_get_reg_inst(amxd_path_t* obj_path) {
    amxb_bus_ctx_t* bus_ctx = uspa_get_busctxt();
    amxc_string_t discover_path;
    amxc_var_t ret;
    amxc_var_t* result = NULL;
    char* instance = 0;
    char* part = amxd_path_get_first(obj_path, true);
    const char* param = NULL;

    amxc_var_init(&ret);
    amxc_string_init(&discover_path, 0);
    amxc_string_setf(&discover_path, "Discovery.Service.*.Registration.[Path == '%s", part);

    if(uspa_prefix_check_device(part)) {
        free(part);
        part = amxd_path_get_first(obj_path, true);
        if(part != NULL) {
            amxc_string_appendf(&discover_path, "%s", part);
            free(part);
            part = NULL;
        } else {
            param = amxd_path_get_param(obj_path);
            if(param != NULL) {
                amxc_string_appendf(&discover_path, "%s", param);
            }
        }
    }

    amxc_string_append(&discover_path, "'].", 3);
    amxb_get(bus_ctx, amxc_string_get(&discover_path, 0), 0, &ret, 5);

    result = GETP_ARG(&ret, "0.0");
    if(result != NULL) {
        instance = strdup(amxc_var_key(result));
    }

    free(part);
    amxc_var_clean(&ret);
    amxc_string_clean(&discover_path);

    return instance;
}

static char* uspa_discovery_get_eid_from_reg_inst(const char* reg_inst) {
    amxb_bus_ctx_t* bus_ctx = uspa_get_busctxt();
    amxd_path_t path;
    char* last = NULL;
    char* eid_str = NULL;
    amxc_var_t* eid = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxd_path_init(&path, reg_inst);
    last = amxd_path_get_last(&path, true);
    free(last);
    last = amxd_path_get_last(&path, true);
    free(last);

    amxb_get(bus_ctx, amxd_path_get(&path, AMXD_OBJECT_TERMINATE), 0, &ret, 5);
    eid = GETP_ARG(&ret, "0.0.EndpointID");
    if(eid != NULL) {
        eid_str = amxc_var_dyncast(cstring_t, eid);
    }

    amxc_var_clean(&ret);
    amxd_path_clean(&path);

    return eid_str;
}

amxb_bus_ctx_t* uspa_discovery_ctx_from_imtp(const char* path) {
    amxb_bus_ctx_t* be_ctx = NULL;
    char* registered_instance = NULL;
    char* eid = NULL;
    uspi_con_t* con = NULL;
    amxd_path_t obj_path;

    amxd_path_init(&obj_path, path);
    registered_instance = uspa_discovery_get_reg_inst(&obj_path);

    when_str_empty(registered_instance, exit);

    eid = uspa_discovery_get_eid_from_reg_inst(registered_instance);
    when_str_empty(eid, exit);

    con = uspa_mtp_imtp_get_con_from_eid(eid);
    when_null(con, exit);

    be_ctx = con->imtp_ctx;

exit:
    amxd_path_clean(&obj_path);
    free(eid);
    free(registered_instance);
    return be_ctx;
}

bool uspa_discovery_object_is_local(const char* path) {
    amxb_bus_ctx_t* bus_ctx = uspa_get_busctxt();
    amxd_path_t obj_path;
    int retval = -1;
    amxc_var_t exists;
    char* fixed_part = NULL;
    bool local = true;

    amxc_var_init(&exists);
    amxd_path_init(&obj_path, path);
    amxd_path_setf(&obj_path, false, "%s", path);

    fixed_part = amxd_path_get_fixed_part(&obj_path, false);
    retval = amxb_describe(bus_ctx, fixed_part, AMXB_FLAG_EXISTS, &exists, 5);
    if((retval != 0) || !amxc_var_dyncast(bool, &exists)) {
        local = false;
    }

    free(fixed_part);
    amxc_var_clean(&exists);
    amxd_path_clean(&obj_path);
    return local;
}

amxb_bus_ctx_t* uspa_discovery_get_ctx(const char* path) {
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxd_path_t obj_path;

    amxd_path_init(&obj_path, path);
    amxd_path_setf(&obj_path, false, "%s", path);

    bus_ctx = uspa_discovery_ctx_from_imtp(amxd_path_get(&obj_path, AMXD_OBJECT_TERMINATE));
    if(bus_ctx == NULL) {
        bus_ctx = uspa_get_busctxt();
    }

    amxd_path_clean(&obj_path);
    return bus_ctx;
}
