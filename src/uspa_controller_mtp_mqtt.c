/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>

#include "uspa_controller.h"
#include "uspa_controller_mtp_mqtt.h"
#include "uspa_mtp_mqtt.h"
#include "uspa_imtp.h"

char* uspa_controller_mtp_mqtt_get_enabled_mtp(const char* reference) {
    bool enabled = false;
    char* contr_mtp = NULL;
    int retval = -1;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    amxc_string_t path;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "LocalAgent.Controller.*.MTP.[MQTT.Reference == '%s'].", reference);

    retval = amxb_get(ctx, amxc_string_get(&path, 0), 0, &ret, 5);
    when_failed(retval, exit);

    enabled = GETP_BOOL(&ret, "0.0.Enable");
    when_false(enabled, exit);

    contr_mtp = strdup(amxc_var_key(GETP_ARG(&ret, "0.0")));

exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&path);
    return contr_mtp;
}

char* uspa_controller_mtp_mqtt_get_topic(const char* eid) {
    char* topic = NULL;
    int retval = -1;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    amxc_var_t* result = NULL;
    amxc_var_t ret;
    amxc_string_t path;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "LocalAgent.Controller.[EndpointID == '%s'].MTP.*.MQTT.", eid);

    retval = amxb_get(ctx, amxc_string_get(&path, 0), 0, &ret, 5);

    result = GETP_ARG(&ret, "0.0.Topic");
    if((retval != 0) || (result == NULL)) {
        SAH_TRACEZ_WARNING(ME, "Could not find controller '%s' MQTT topic", eid);
        goto exit;
    }

    topic = amxc_var_dyncast(cstring_t, result);

exit:
    amxc_string_clean(&path);
    amxc_var_clean(&ret);
    return topic;
}

int uspa_controller_mtp_mqtt_init(const char* contr_inst, const char* contr_mtp_inst) {
    int retval = -1;
    char* reference = NULL;
    uspi_con_t* con = NULL;

    reference = uspa_mtp_mqtt_get_reference(contr_mtp_inst);
    if((reference == NULL) || (*reference == 0)) {
        SAH_TRACEZ_WARNING(ME, "No MQTT reference client found for '%s'. Not connecting", contr_mtp_inst);
        goto exit;
    }

    con = uspa_mtp_mqtt_get_connection(reference);
    if(con != NULL) {
        uspa_controller_con_add(contr_inst, contr_mtp_inst, con);
    }

exit:
    free(reference);
    return retval;
}
