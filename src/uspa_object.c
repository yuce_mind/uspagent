/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "uspa_object.h"

static amxc_var_t* uspa_object_get_param_value(const char* obj_path, const char* param) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    amxc_var_t ret;
    amxc_string_t param_path;
    amxc_var_t* param_value = NULL;
    amxc_var_t* param_ret = NULL;

    amxc_var_new(&param_ret);
    amxc_var_init(&ret);
    amxc_string_init(&param_path, 0);
    amxc_string_setf(&param_path, "0.0.%s", param);

    when_str_empty(obj_path, exit);

    retval = amxb_get(ctx, obj_path, 0, &ret, 5);
    when_failed(retval, exit);

    param_value = GETP_ARG(&ret, amxc_string_get(&param_path, 0));
    when_null(param_value, exit);

    amxc_var_move(param_ret, param_value);

exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&param_path);
    return param_ret;
}

bool uspa_object_get_bool(const char* obj_path, const char* param) {
    bool value = false;
    amxc_var_t* param_value = NULL;

    param_value = uspa_object_get_param_value(obj_path, param);
    when_null(param_value, exit);

    value = amxc_var_dyncast(bool, param_value);

exit:
    amxc_var_delete(&param_value);
    return value;
}

uint32_t uspa_object_get_uint32_t(const char* obj_path, const char* param) {
    uint32_t value = false;
    amxc_var_t* param_value = NULL;

    param_value = uspa_object_get_param_value(obj_path, param);
    when_null(param_value, exit);

    value = amxc_var_dyncast(uint32_t, param_value);

exit:
    amxc_var_delete(&param_value);
    return value;
}

char* uspa_object_get_cstring_t(const char* obj_path, const char* param) {
    char* value = NULL;
    amxc_var_t* param_value = NULL;

    param_value = uspa_object_get_param_value(obj_path, param);
    when_null(param_value, exit);

    value = amxc_var_dyncast(cstring_t, param_value);

exit:
    amxc_var_delete(&param_value);
    return value;
}

int uspa_object_update_status(const char* obj_path, const char* status) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = uspa_get_busctxt();
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty(obj_path, exit);
    when_str_empty(status, exit);

    SAH_TRACEZ_INFO(ME, "Update Status of %s to %s", obj_path, status);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "status", status);
    retval = amxb_call(ctx, obj_path, "UpdateStatus", &args, &ret, 5);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    return retval;
}

bool uspa_object_is_instantiated(amxb_bus_ctx_t* ctx, const char* object) {
    bool retval = false;
    amxc_var_t ret;

    amxc_var_init(&ret);

    when_null(ctx, exit);
    when_str_empty(object, exit);

    amxb_get(ctx, object, 0, &ret, 5);
    when_null(GETP_ARG(&ret, "0.0"), exit);

    retval = true;
exit:
    amxc_var_clean(&ret);
    return retval;
}
