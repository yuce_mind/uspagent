/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <imtp/imtp_message.h>
#include <imtp/imtp_connection.h>

#include <usp/uspl.h>

#include "uspa_imtp.h"
#include "uspa_imtp_mqtt.h"
#include "uspa_object.h"
#include "uspa_controller_mtp.h"
#include "uspa_prefix_check.h"
#include "uspa_utils.h"
#include "uspa_mtp.h"
#include "uspa_mtp_imtp.h"
#include "uspa_mtp_mqtt.h"
#include "uspa_controller.h"

static amxc_string_t* uspa_imtp_get_uri(const char* reference, uint32_t mtp_id) {
    amxc_string_t* uri = NULL;

    if(mtp_id == USPA_MTP_ID_MQTT) {
        amxc_string_new(&uri, 0);
        amxc_string_setf(uri, "usp:/var/run/mqtt/%s", reference); // TODO fetch this from config odl
    } else {
        SAH_TRACEZ_WARNING(ME, "Unsupported MTP ID [%d] for reference '%s'", mtp_id, reference);
    }

    return uri;
}

static uspi_con_t* uspa_imtp_con_from_ref(const char* reference, uint32_t mtp_id) {
    uspi_con_t* con = NULL;

    if(mtp_id == USPA_MTP_ID_MQTT) {
        con = uspa_mtp_mqtt_get_connection(reference);
    } else {
        SAH_TRACEZ_WARNING(ME, "Unsupported MTP ID [%d] for reference '%s'", mtp_id, reference);
    }

    return con;
}

static int uspa_imtp_invoke_and_connect(const char* reference,
                                        const char* mtp_uri,
                                        uspi_con_t** con) {
    int retval = -1;
    amxb_invoke_t* invoke_ctx = NULL;
    amxb_bus_ctx_t* bus_ctx = uspa_get_busctxt();
    const char* method = "CreateListenSocket";
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t* stripped_ref = uspa_utils_add_dot(reference);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    if(uspa_prefix_check_device(reference)) {
        uspa_prefix_device_strip(stripped_ref);
    }

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "uri", mtp_uri);
    amxc_var_add_key(cstring_t, &args, "receiver_types", "USP");

    SAH_TRACEZ_INFO(ME, "Calling %sCreateListenSocket()", amxc_string_get(stripped_ref, 0));
    retval = amxb_call(bus_ctx, amxc_string_get(stripped_ref, 0), method, &args, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Could not call CreateListenSocket, retval = [%d]", retval);
        goto exit;
    }

    retval = uspi_con_connect(con, mtp_uri);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Could not connect to [%s], retval = [%d]", mtp_uri, retval);
        goto exit;
    }
    // Make sure the socket is not used automatically for amxb operations like an amxb_resolve
    (*con)->imtp_ctx->socket_type = USPA_IMTP_SOCK;

    SAH_TRACEZ_INFO(ME, "Connected to fd: [%d]", uspi_con_get_fd(*con));

exit:
    amxc_string_delete(&stripped_ref);
    amxb_free_invoke(&invoke_ctx);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return retval;
}

uspi_con_t* uspa_imtp_con_from_la_mtp(const char* la_mtp) {
    uspi_con_t* res = NULL;
    amxc_htable_t* mtp_instances = uspa_mtp_get_instances();

    when_str_empty(la_mtp, exit);

    amxc_htable_for_each(it, mtp_instances) {
        uspa_mtp_instance_t* inst = amxc_container_of(it, uspa_mtp_instance_t, hit);
        uspi_con_t* con = inst->con;
        uspa_mtp_t* uspa_mtp = NULL;
        if(con == NULL) {
            continue;
        }

        uspa_mtp = (uspa_mtp_t*) con->priv;
        if((strcmp(uspa_mtp->la_mtp, la_mtp) == 0)) {
            res = con;
            break;
        }
    }

exit:
    return res;
}

uspi_con_t* uspa_imtp_con_from_contr(const char* contr_inst) {
    uspi_con_t* res = NULL;
    amxc_var_t ret;
    const char* contr_eid = NULL;

    amxc_var_init(&ret);

    when_str_empty(contr_inst, exit);

    // Check original MTPs
    res = uspa_controller_con_get(contr_inst);
    when_not_null(res, exit);

    // If no connection found in regular MTPs, try to find con to IMTP
    amxb_get(uspa_get_busctxt(), contr_inst, 0, &ret, 5);
    contr_eid = GETP_CHAR(&ret, "0.0.EndpointID");
    res = uspa_mtp_imtp_get_con_from_eid(contr_eid);

exit:
    amxc_var_clean(&ret);
    return res;
}

static void uspa_imtp_read(UNUSED int fd, void* priv) {
    int retval = -1;
    uspi_con_t* con = (uspi_con_t*) priv;
    imtp_tlv_t* tlv = NULL;
    uspa_mtp_t* mtp = (uspa_mtp_t*) con->priv;

    when_null(mtp, exit);

    retval = uspi_con_read(con, &tlv);
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error reading data from IMTP connection");
        uspa_imtp_disconnect(con);
        goto exit;
    }

    if(mtp->type_id == USPA_MTP_ID_MQTT) {
        uspa_imtp_mqtt_handle_tlv(con, tlv);
    }

exit:
    imtp_tlv_delete(&tlv);
}

int uspa_imtp_connect(const char* reference,
                      uint32_t mtp_id,
                      const char* la_mtp) {
    int retval = -1;
    amxc_string_t* mtp_uri = NULL;
    uspi_con_t* con = NULL;

    SAH_TRACEZ_INFO(ME, "Create IMTP connection to %s", reference);

    when_str_empty(reference, exit);

    con = uspa_imtp_con_from_ref(reference, mtp_id);
    if(con != NULL) {
        SAH_TRACEZ_INFO(ME, "IMTP connection to %s already exists", reference);
        retval = 0;
        goto exit;
    }

    mtp_uri = uspa_imtp_get_uri(reference, mtp_id);
    when_null(mtp_uri, exit);

    retval = uspa_imtp_invoke_and_connect(reference, amxc_string_get(mtp_uri, 0), &con);
    when_failed(retval, exit);

    retval = uspa_mtp_instance_con_add(la_mtp, con);
    when_failed(retval, exit);

    if(mtp_id == USPA_MTP_ID_MQTT) {
        uspa_mtp_mqtt_new(con, reference, la_mtp);
    } else {
        SAH_TRACEZ_WARNING(ME, "Unsupported MTP ID [%d] for reference '%s'", mtp_id, reference);
        uspa_mtp_mqtt_delete(con);
        goto exit;
    }

    amxo_connection_add(uspa_get_parser(), uspi_con_get_fd(con),
                        uspa_imtp_read, NULL, AMXO_CUSTOM, con);

    uspa_controller_con_link(con, "MQTT", reference);

exit:
    amxc_string_delete(&mtp_uri);
    return retval;
}

int uspa_imtp_disconnect(uspi_con_t* con) {
    int retval = -1;
    uspa_mtp_t* mtp = NULL;

    when_null(con, exit);
    when_null(con->priv, exit);

    mtp = (uspa_mtp_t*) con->priv;

    amxo_connection_remove(uspa_get_parser(), uspi_con_get_fd(con));
    uspa_mtp_instance_con_rm(mtp->la_mtp);

    SAH_TRACEZ_NOTICE(ME, "Destroy Unix Domain socket on fd [%d]", uspi_con_get_fd(con));
    if(mtp->type_id == USPA_MTP_ID_MQTT) {
        uspa_mtp_mqtt_delete(con);
    }
    uspi_con_disconnect(&con);

    retval = 0;
exit:
    return retval;
}

int uspa_imtp_send(uspi_con_t* con, uspl_tx_t* usp_tx, amxc_var_t* mtp_info) {
    int retval = 0;
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_protobuf = NULL;
    void* bytes = NULL;
    uint8_t* payload = NULL;
    int fd = 0;
    const char* protocol = GET_CHAR(mtp_info, "protocol");

    when_null(con, exit);
    when_null_status(usp_tx, exit, retval = 0);
    when_null(mtp_info, exit);
    when_str_empty(protocol, exit);
    when_null_trace(usp_tx->pbuf, exit, ERROR, "Protobuf is NULL");
    SAH_TRACEZ_INFO(ME, "Create and send a reponse TLV to socket");

    fd = uspi_con_get_fd(con);
    when_true(fd <= 0, exit);

    payload = (uint8_t*) usp_tx->pbuf;
    imtp_tlv_new(&tlv_head, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE);
    imtp_tlv_new(&tlv_protobuf, imtp_tlv_type_protobuf_bytes,
                 usp_tx->pbuf_len, payload, 0, IMTP_TLV_COPY);
    imtp_tlv_add(tlv_head, tlv_protobuf);

    if(strcmp(protocol, "MQTT") == 0) {
        uspa_mtp_t* mtp = (uspa_mtp_t*) con->priv;
        uspa_mtp_mqtt_t* mqtt_data = NULL;

        when_null(mtp, exit);
        mqtt_data = mtp->mtp_data.mqtt;
        when_null(mqtt_data, exit);
        uspa_imtp_mqtt_add_tlv(tlv_head, mqtt_data, usp_tx, mtp_info);
    }

    retval = imtp_message_to_bytes(tlv_head, &bytes);
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error creating binary message from TLV chain");
    }

    SAH_TRACEZ_INFO(ME, "Writing %d bytes on fd [%d]", tlv_head->length + 8, fd);
    retval = write(fd, bytes, tlv_head->length + 8);
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error sending TLV over unix domain socket with fd: %d", fd);
    }

exit:
    free(bytes);
    imtp_tlv_delete(&tlv_head);
    return retval;
}
